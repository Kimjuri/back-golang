package mailer

import (
	"back/test"
	"strings"
	"testing"
)

var receiver []string
var htmlBody string

func init() {
	receiver = []string{"charles.paulet@epitech.eu", "hoepfully_not_a_valid_email@gmail.com"}
	htmlBody = `
	<!DOCTYPE HTML PULBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
		<head>
			<meta http-equiv="content-type" content="text/html">
		</head>
		<body>
			This is the body<br />
			<br>
			Regards<br />
			<i>Groomshop</i><br />
		</body>
	</html>
	`
}

func TestWritePlainEmail(t *testing.T) {
	subject := "Testing plain text email subject"
	message := "Testing plain text email body"
	body := WritePlainEmail(receiver, subject, message)
	test.MyAssert(t, "plain text header field", true, strings.Contains(body, "text/plain"))
	test.Reject(t, "email isnt empty", "", body)
}

func TestWriteHTMLEmail(t *testing.T) {
	subject := "Testing html email subject"
	body := WriteHTMLEmail(receiver, subject, htmlBody)
	test.MyAssert(t, "html text header field", true, strings.Contains(body, "text/html"))
	test.Reject(t, "email isnt empty", "", body)
}

func TestSendMail(t *testing.T) {
	subject := "Testing email subject"
	body := WriteHTMLEmail(receiver, subject, htmlBody)
	err := SendMail(receiver, subject, body)
	test.MyAssert(t, "send mail", true, err == nil)
}
