package mailer

/*

En utilisant le smtp de google, il faut bien régler les paramètres
de sécurité dans gmail (whitelist l'app groomshop et accepter la connexion
par des applications tierces).
https://myaccount.google.com/lesssecureapps

*/

import (
	"back/configuration"
	"back/model"
	"bytes"
	"fmt"
	"io/ioutil"
	"mime/quotedprintable"
	"net/smtp"
	"strings"
)

var sender model.Smtp

func init() {
	sender = model.Smtp{
		configuration.Configuration.SMTP_user,
		configuration.Configuration.SMTP_password,
		configuration.Configuration.SMTP_host,
		configuration.Configuration.SMTP_port,
	}
}

func SendMail(Dest []string, Subject, bodyMessage string) error {
	msg := "From: " + sender.User + "\n" +
		"To: " + strings.Join(Dest, ",") + "\n" +
		"Subject: " + Subject + "\n" + bodyMessage

	server := sender.Host + sender.Port
	src := smtp.PlainAuth("", sender.User, sender.Password, sender.Host)
	if err := smtp.SendMail(server, src, sender.User, Dest, []byte(msg)); err != nil {
		fmt.Printf("smtp error: %s", err)
		return err
	}
	fmt.Println("Mail sent")
	return nil
}

func writeEmail(dest []string, contentType, subject, bodyMessage string) string {

	header := make(map[string]string)
	header["From"] = sender.User

	receipient := ""

	for _, user := range dest {
		receipient = receipient + user
	}

	header["To"] = receipient
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", contentType)
	header["Content-Transfer-Encoding"] = "quoted-printable"
	header["Content-Disposition"] = "inline"

	message := ""

	for key, value := range header {
		message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	var encodedMessage bytes.Buffer

	finalMessage := quotedprintable.NewWriter(&encodedMessage)
	finalMessage.Write([]byte(bodyMessage))
	finalMessage.Close()

	message += "\r\n" + encodedMessage.String()

	return message
}

func WriteHTMLEmail(dest []string, subject, bodyMessage string) string {
	return writeEmail(dest, "text/html", subject, bodyMessage)
}

func WritePlainEmail(dest []string, subject, bodyMessage string) string {
	return writeEmail(dest, "text/plain", subject, bodyMessage)
}

func SendForgotPasswordEmail(token string, email string, userType string) error {
	subject := "Mot de passe oublié Groomshop"
	link := "https://groomshop.fr/updatePassword/" + userType + "/" + token
	m, err := ioutil.ReadFile("../mailer/templates/password")
	if err != nil {
		fmt.Errorf("SendForgotPasswordEmail : Couldn't open template file")
		return err
	} else {
		message := strings.Replace(string(m), "LINKTOKEN", link, 1)

		body := WriteHTMLEmail([]string{email}, subject, message)
		err := SendMail([]string{email}, subject, body)
		if err != nil {
			return err
		}
	}
	return nil
}

func SendVerificationEmail(token string, email string, userType string) error {
	subject := "Validation de compte Groomshop"
	link := "https://groomshop.fr/account/validate/" + userType + "/" + token
	m, err := ioutil.ReadFile("../mailer/templates/verification")
	if err != nil {
		fmt.Print("SendVerificationEmail : Couldn't open template file\n")
		return err
	} else {
		message := strings.Replace(string(m), "LINKTOKEN", link, 1)
		body := WriteHTMLEmail([]string{email}, subject, message)
		err := SendMail([]string{email}, subject, body)
		if err != nil {
			return err
		}
	}
	return nil
}
