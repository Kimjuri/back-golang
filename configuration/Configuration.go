package configuration

import (
	"back/model"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// Default configuration
var Configuration = model.Configuration{
	API_Address:      "0.0.0.0",
	API_Port:         8080,
	DB_Address:       "db",
	DB_Port:          3306,
	DB_Type:          "mysql",
	DB_User:          "root",
	DB_Pass:          "DB_PWD",
	DB_Name:          "groomshop",
	JWT_Secret:       "foobar",
	JWT_Method:       "HS256",
	JWT_Lifetime:     24,
	Version:          "Unknow",
	LastUpdate:       "Unknow",
	SMTP_host:        "smtp.gmail.com",
	SMTP_port:        ":587",
	SMTP_user:        "contact.groomshop@gmail.com",
	SMTP_password:    "groomshop-best-eip-dans-le-game",
	HOST:             "api.groomshop.fr",
	EMAIL_validation: true,
	Images_PATH:      "/home",
	Images_URL:       "https://api.groomshop.fr/receipts/",
}

var path = "../configuration/conf.json"

func init() {
	/* readlink -f conf.json */
	if raw, err := ioutil.ReadFile(path); err != nil {
		fmt.Println(err.Error())
	} else {
		json.Unmarshal(raw, &Configuration)
	}
	Configuration.DB_Pass = os.Getenv(Configuration.DB_Pass)
}
