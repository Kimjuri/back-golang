package main

import (
	"back/api"
	"back/configuration"
	"fmt"
)

func main() {
	address := fmt.Sprintf("%s:%d", configuration.Configuration.API_Address, configuration.Configuration.API_Port)
	a := api.API{Address: address, Timeout: 15, Router: nil}
	a.Run()
}
