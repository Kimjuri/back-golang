FROM golang:1.10.0-alpine3.7 AS build
MAINTAINER Charles PAULET <charles.paulet@epitech.eu>

RUN apk update

RUN apk add --no-cache git g++ make \
    && go get github.com/golang/dep/cmd/dep

COPY Gopkg.lock Gopkg.toml /go/src/back/

WORKDIR /go/src/back/

RUN dep ensure -vendor-only

COPY . /go/src/back/

RUN go build -o /back

FROM alpine:3.7

RUN apk update
RUN apk add ca-certificates

COPY --from=build /back /back
# Local certificates still required ?
COPY --from=build /go/src/back/auth/server.rsa.crt /auth/server.crt
COPY --from=build /go/src/back/auth/server.rsa.key /auth/server.key
COPY --from=build /go/src/back/configuration/conf.json /configuration/conf.json
COPY --from=build /go/src/back/mailer/templates/verification /mailer/templates/verification
COPY --from=build /go/src/back/mailer/templates/password /mailer/templates/password
COPY --from=build /go/src/back/scripts/wait-for-db.bash /start.sh
COPY --from=build /go/src/back/ressources/watermark.png /ressources/watermark.png
ENTRYPOINT ["/start.sh"]
