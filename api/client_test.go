package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

/* client creation */
// func TestClientCreation(t *testing.T) {
// 	test.ReloadDB(t)
// 	payload := []byte(`{
// 		"firstname": "Firstname",
// 		"lastname": "Lastname",
// 		"street": "test_street",
// 		"zipcode": "00000",
// 		"city": "test_city",
// 		"additionaladdress": "test_additionaladdress",
// 		"email": "test@mail.fr",
// 		"phonenumber": "0625487596",
// 		"password": "test_pw",
// 		"deadline": "04:13:37"
// 	}`)
// 	req, _ := http.NewRequest("POST", "/clients", bytes.NewBuffer(payload))
// 	rr := httptest.NewRecorder()
// 	handler := http.HandlerFunc(createHandler(createClient, &model.Client{}, clientValidKeys, "any"))
// 	handler.ServeHTTP(rr, req)
//
// 	test.CheckResponseCode(t, http.StatusCreated, rr.Code)
// }

func TestClientFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/clients/5", nil) /* Test route */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "5", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(5, "client"); err != nil { /* authenticate request */
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getClientHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims))) /* Simulate http service with request context */

	test.CheckResponseCode(t, http.StatusOK, rr.Code) /* Test response status */

	var client model.Client
	if err := json.Unmarshal([]byte(rr.Body.String()), &client); err != nil { /* Retrieve json data from response */
		t.Fatal(err)
	}
	test.MyAssert(t, "client id", 5, client.ID)
	test.MyAssert(t, "client firstname", "TestClientFetch", client.FirstName)
	test.MyAssert(t, "client lastname", "cln5", client.LastName)
	test.MyAssert(t, "client email", "ce5@client.fr", client.Email)
	test.MyAssert(t, "client phonenumber", "0606060606", client.PhoneNumber)
	test.MyAssert(t, "client deadline", "04:13:54", client.Deadline)
}

func TestClientFetchAsAdmin(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/clients/5", nil) /* Test route */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "5", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil { /* authenticate request */
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getClientHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims))) /* Simulate http service with request context */

	test.CheckResponseCode(t, http.StatusOK, rr.Code) /* Test response status */

	var client model.Client
	if err := json.Unmarshal([]byte(rr.Body.String()), &client); err != nil { /* Retrieve json data from response */
		t.Fatal(err)
	}
	test.MyAssert(t, "client id", 5, client.ID)
	test.MyAssert(t, "client firstname", "TestClientFetch", client.FirstName)
	test.MyAssert(t, "client lastname", "cln5", client.LastName)
	test.MyAssert(t, "client email", "ce5@client.fr", client.Email)
	test.MyAssert(t, "client phonenumber", "0606060606", client.PhoneNumber)
	test.MyAssert(t, "client deadline", "04:13:54", client.Deadline)
}

func ClientTestPatch(t *testing.T, payload []byte, expecting int) {
	test.ReloadDB(t)
	req, err := http.NewRequest("PATCH", "/clients/1", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(patchHandler(patchClient, &model.Client{}, clientValidKeys, AuthRequired))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, expecting, rr.Code)
}

func TestClientPatch(t *testing.T) {
	payload := []byte(`{
		"firstname": "firstname",
		"lastname": "lastname",
		"email": "f.lastname@test.fr",
		"phonenumber": "0606060606",
		"password": "pwd_test",
		"deadline": "00:00:00"
	}`)
	ClientTestPatch(t, payload, http.StatusAccepted)
}

func TestClientPatchFirstname(t *testing.T) {
	payload := []byte(`{
		"firstname": "firstname2"
	}`)
	ClientTestPatch(t, payload, http.StatusAccepted)
}

func TestClientPatchWrongFirstname(t *testing.T) {
	payload := []byte(`{
		"firstname": "@firstname2"
	}`)
	ClientTestPatch(t, payload, http.StatusInternalServerError)
}

func TestClientPatchLastname(t *testing.T) {
	payload := []byte(`{
		"lastname": "lastname2"
	}`)
	ClientTestPatch(t, payload, http.StatusAccepted)
}

func TestClientPatchWrongLastname(t *testing.T) {
	payload := []byte(`{
		"lastname": "@lastname2"
	}`)
	ClientTestPatch(t, payload, http.StatusInternalServerError)
}

func TestClientPatchEmail(t *testing.T) {
	payload := []byte(`{
		"email": "thisis.atest@test.com"
	}`)
	ClientTestPatch(t, payload, http.StatusAccepted)
}

func TestClientPatchWrongEmail(t *testing.T) {
	payload := []byte(`{
		"email": "wrong@email@gmail.com"
	}`)
	ClientTestPatch(t, payload, http.StatusInternalServerError)
}

func TestClientPatchPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "+33601020304"
	}`)
	ClientTestPatch(t, payload, http.StatusAccepted)
}

func TestClientPatchWrongPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "not a phone number"
	}`)
	ClientTestPatch(t, payload, http.StatusInternalServerError)
}
