package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func TestDeliveryCreation(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idPurchases": [1],
		"idRider": 1
	}`)
	req, _ := http.NewRequest("POST", "/deliveries", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "deliveries_manager"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(createHandler(createDelivery, &model.Delivery{}, deliveriesValidKeys, "deliveries_manager"))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusCreated, rr.Code)
}

func TestDeliveryCreationAsAnotherApp(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idPurchase": [1],
		"idRider": 1
	}`)
	req, _ := http.NewRequest("POST", "/deliveries", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "random_app"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(createHandler(createDelivery, &model.Delivery{}, deliveriesValidKeys, "deliveries_manager"))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestAllDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 3, len(ds))
}

func TestAllDeliveriesOfAClientFetchFromClient(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?c=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 3, len(ds))
}

func TestPlacedDeliveriesOfAClientFetchFromClient(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/placed?c=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "delivery_placed", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 2, len(ds))
}

func TestAllDeliveriesOfAClientFetchFromPartner(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?c=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "partner"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestAllDeliveriesOfAClientFetchFromRider(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?c=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 3, len(ds))
}

func TestAllDeliveriesOfAPartnerFetchFromPartner(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?p=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "partner"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 3, len(ds))
}

func TestPlacedDeliveriesOfAPartnerFetchFromPartner(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/placed?p=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "partner"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "delivery_placed", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 2, len(ds))
}

func TestAllDeliveriesOfAPartnerFetchFromClient(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?p=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestAllDeliveriesOfAPartnerFetchFromRider(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?p=1", nil) /* Get all deliveries of client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries amount", 3, len(ds))
}

func TestAllForPurchaseDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?pu=1", nil) /* Get all deliveries with purchase 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries for purchase 1 amount", 1, len(ds))
}

func TestAllForRiderDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries?r=1", nil) /* Get all deliveries for rider 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All deliveries for rider 1 amount", 2, len(ds))
}

func TestPlacedDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/placed", nil) /* Get all placed deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "delivery_placed", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Placed deliveries amount", 2, len(ds))
}

func TestPendingForPurchaseDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/pending?pu=2", nil) /* Get all pending deliveries with purchase 2 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "delivery_pending", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Pending deliveries with purchase 2 amount", 1, len(ds))
}

func TestResolvedForRiderDeliveriesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/resolved?r=1", nil) /* Get all deliveries resolved by rider 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryHandler(getDelivery, "delivery_resolved", validDeliveryQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.Delivery
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Resolved deliveries with rider 1 amount", 0, len(ds))
}

func TestFetchDeliveryByID(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/1", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getDeliveryByIDHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var p map[string]interface{}
	if err := json.Unmarshal([]byte(rr.Body.String()), &p); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All purchases amount", 1, len(p))
}

func TestDeliveryPatch(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idRider": 1
	}`)
	req, err := http.NewRequest("PATCH", "/deliveries/1", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(2, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(patchHandler(patchDelivery, &model.Delivery{}, deliveriesValidKeys, NoAuth))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusAccepted, rr.Code)
}

func TestAddressesDelivery(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/deliveries/addresses/1", nil) /* Get all deliveries resolved by rider 1 */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getAddressesOfDelivery)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var a []model.Address
	if err := json.Unmarshal([]byte(rr.Body.String()), &a); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All addresses amount", 2, len(a))
	test.MyAssert(t, "street client", "16 rue d'Orléans", a[0].Street)
	test.MyAssert(t, "street partner", "1 rue de Budapest", a[1].Street)
}
