package api

import (
	"back/configuration"
	"back/db"
	"back/model"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"time"

	"github.com/disintegration/imaging"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

func createDelivery(T interface{}) (int64, error) {
	d := (reflect.ValueOf(T)).Interface().(*model.Delivery)
	/* Prendre l'heure du client à l'avenir*/
	t := time.Now()
	tf := t.Format("2006-01-02 15:04:05")
	return db.CreateDelivery(d.IDPurchases, d.IDRider, 1, tf)
}

func generateImageFileName(extension string) (string, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}
	return id.String() + extension, nil
}

func getMin(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func applyWatermarkAndSave(imagePath string, outPath string) error {
	// Open Image
	reader, err := os.Open(imagePath)
	if err != nil {
		return err
	}
	defer reader.Close()
	// Read Image properties
	imageConfig, _, err := image.DecodeConfig(reader)
	if err != nil {
		return err
	}
	reader.Seek(0, 0)
	// Read Image data
	imageBuffer, _, err := image.Decode(reader)
	if err != nil {
		return err
	}
	// Open Watermark
	readerWatermark, err := os.Open("./ressources/watermark.png")
	if err != nil {
		return err
	}
	defer readerWatermark.Close()
	imageWatermarkBuffer, _, err := image.Decode(readerWatermark)
	if err != nil {
		return err
	}
	// Rotate Watermark data
	imageWatermarkBuffer = imaging.Rotate(imageWatermarkBuffer, 45, color.NRGBA{0, 0, 0, 0})
	size := getMin(imageConfig.Width, imageConfig.Height)
	if size+20 <= imageWatermarkBuffer.Bounds().Max.X || size+20 <= imageWatermarkBuffer.Bounds().Max.Y {
		// Resize watermark image
		imageWatermarkBuffer = imaging.Resize(imageWatermarkBuffer, size-20, size-20, imaging.Lanczos)
	}
	// Apply Watermark on Image
	offsetFirst := image.Pt(50-(imageWatermarkBuffer.Bounds().Max.X/2), 0-(imageWatermarkBuffer.Bounds().Max.Y/2))
	offsetSecond := image.Pt((imageConfig.Width/2)-(imageWatermarkBuffer.Bounds().Max.X/2), (imageConfig.Height/2)-(imageWatermarkBuffer.Bounds().Max.Y/2))
	offsetThird := image.Pt(imageConfig.Width-(imageWatermarkBuffer.Bounds().Max.X/2)-50, imageConfig.Height-(imageWatermarkBuffer.Bounds().Max.Y/2))
	bounds := imageBuffer.Bounds()
	mask := image.NewRGBA(bounds)
	draw.Draw(mask, bounds, imageBuffer, image.ZP, draw.Src)
	draw.Draw(mask, imageWatermarkBuffer.Bounds().Add(offsetFirst), imageWatermarkBuffer, image.ZP, draw.Over)
	draw.Draw(mask, imageWatermarkBuffer.Bounds().Add(offsetSecond), imageWatermarkBuffer, image.ZP, draw.Over)
	draw.Draw(mask, imageWatermarkBuffer.Bounds().Add(offsetThird), imageWatermarkBuffer, image.ZP, draw.Over)
	// Create Output file
	newImage, err := os.Create(outPath)
	if err != nil {
		return err
	}
	defer newImage.Close()
	// Save result on Output file
	jpeg.Encode(newImage, mask, &jpeg.Options{jpeg.DefaultQuality})
	return nil
}

func getDeliveryHandler(f func(string, string, string) ([]*model.Delivery, error), status string, validKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "rider" && ctx.Role != "partner" && ctx.Role != "client" && ctx.Role != "admin" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			q := r.URL.Query()
			lq := len(q)
			var d []*model.Delivery
			var err error
			if lq > 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Too much parameters"})
			} else if lq == 0 {
				if ctx.Role != "rider" && ctx.Role != "admin" {
					respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
					return
				} else {
					d, err = db.GetAllSpecifiedDeliveries(status)
				}
			} else {
				found := false
				for _, k := range validKeys {
					if q[k] != nil {
						switch k {
						case "p":
							if ctx.Role != "rider" && ctx.Role != "partner" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								if ctx.Role == "partner" && strconv.Itoa(ctx.ID) != q[k][0] {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									return
								} else {
									d, err = f(status, k, q[k][0])
								}
							}
						case "c":
							if ctx.Role != "rider" && ctx.Role != "client" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								if ctx.Role == "client" && strconv.Itoa(ctx.ID) != q[k][0] {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									return
								} else {
									d, err = f(status, k, q[k][0])
								}
							}
						case "r":
							if ctx.Role != "rider" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								if ctx.Role == "rider" && strconv.Itoa(ctx.ID) != q[k][0] {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									return
								} else {
									d, err = f(status, k, q[k][0])
								}
							}
						case "pu":
							if ctx.Role != "rider" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								d, err = f(status, k, q[k][0])

							}
						}

						found = true
					}
				}
				if found != true {
					respond(w, http.StatusBadRequest, model.Exception{Message: "Unknow parameter(s)"})
				}
			}
			if err != nil {
				respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
			} else {
				if err := json.NewEncoder(w).Encode(d); err != nil {
					fmt.Println(err.Error())
					respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				}
			}
		}
	})
}

func getDelivery(status string, key string, id string) ([]*model.Delivery, error) {
	var d []*model.Delivery
	var err error
	switch key {
	case "p":
		d, err = db.GetPartnerDeliveries(status, id)
	case "r":
		d, err = db.GetRiderDeliveries(status, id)
	case "c":
		d, err = db.GetClientDeliveries(status, id)
	case "pu":
		d, err = db.GetPurchaseDeliveries(status, id)
	}
	return d, err
}

func getDeliveryByIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "client" && ctx.Role != "partner" && ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if p, err := db.GetPurchases("SELECT * FROM purchases WHERE fk_delivery=?;", mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "partner" {
			if p[0].IDPartner != ctx.ID {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		} else if ctx.Role == "client" {
			if p[0].IDClient != ctx.ID {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		} else if ctx.Role == "rider" {
			if d, err := db.GetDeliveries("SELECT * FROM deliveries WHERE id=?", mux.Vars(r)["id"]); err == nil {
				if d[0].IDRider != ctx.ID {
					respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
				}
			}
		}

		for _, purchase := range p {
			id := purchase.ID
			if receipt, err := db.GetReceiptByPurchase(strconv.Itoa(id)); err == nil {
				if ctx.Role == "client" || ctx.Role == "admin" {
					purchase.Receipt = configuration.Configuration.Images_URL + receipt.Receipt
				}
				if ctx.Role == "rider" {
					purchase.Receipt = configuration.Configuration.Images_URL + receipt.ReceiptWatermarked
				}
			}
		}

		enc := json.NewEncoder(w)
		enc.SetEscapeHTML(false)
		data := make(map[string]interface{})
		data["purchases"] = p
		if err := enc.Encode(data); err != nil {
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getAddressesOfDelivery(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		if a, err := db.GetDeliveryAddresses(mux.Vars(r)["id"]); err != nil {
			respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
		} else {
			if err := json.NewEncoder(w).Encode(a); err != nil {
				fmt.Println(err.Error())
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		}
	}
}

func validateDelivery(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	}
	d, err := db.GetDeliveries("SELECT * FROM deliveries WHERE id = ?;", mux.Vars(r)["id"])
	if err == nil {
		if d[0].Status != 3 {
			d[0].Status = 3
			if err := db.CreateBillForDelivery(d[0]); err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			} else {
				t := time.Now()
				tf := t.Format("2006-01-02 15:04:05")
				db.UpdateDelivery(d[0].IDRider, d[0].Status, tf, mux.Vars(r)["id"])
				description := fmt.Sprintf("delivery was validated by %s:%d", ctx.Role, ctx.ID)
				db.CreateDeliveryLog(d[0].ID, tf, description)
			}
		} else {
			respond(w, http.StatusConflict, model.Exception{Message: http.StatusText(http.StatusConflict)})
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func patchDelivery(b []byte, id string, role string, user_id int) int {
	if role != "rider" && role != "partner" && role != "admin" {
		return 403
	}
	d, err := db.GetDeliveries("SELECT * FROM deliveries WHERE id = ?;", id)
	if err != nil {
		return 404
	}
	deliveriesMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(b), &deliveriesMap)
	if err != nil {
		return 500
	}
	for key, value := range deliveriesMap {
		switch key {
		case "idRider":
			d[0].IDRider = int(value.(float64))
		case "status":
			d[0].Status = int(value.(float64))
		case "date":
			d[0].Date = string(value.(string))
		}
	}
	db.UpdateDelivery(d[0].IDRider, d[0].Status, d[0].Date, id)
	t := time.Now()
	tf := t.Format("2006-01-02 15:04:05")
	description := fmt.Sprintf("delivery was patched by %s:%d", role, user_id)
	db.CreateDeliveryLog(id, tf, description)
	return 202
}
