package api

import (
	"back/model"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
)

func respond(w http.ResponseWriter, s int, e interface{}) {
	if j, err := json.Marshal(e); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
	} else {
		w.WriteHeader(s)
		w.Write(j)
	}
}

/* Check if haystack contains needle */
func in_array(needle interface{}, haystack interface{}) (ok bool, i int) {
	val := reflect.Indirect(reflect.ValueOf(haystack))
	switch val.Kind() {
	case reflect.Slice, reflect.Array:
		for ; i < val.Len(); i++ {
			if ok = needle == val.Index(i).Interface(); ok {
				return
			}
		}
	}
	return
}

/* Check body validity */
func parseBody(T interface{}, w http.ResponseWriter, r *http.Request, requiredKeys []string) (interface{}, []byte, error) {
	newItem := reflect.ValueOf(T).Interface() // Get underlying type as run-time under a value and transform it as an interface
	/* Check body size */
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		return nil, nil, err
	}
	if err := r.Body.Close(); err != nil {
		return nil, nil, err
	}
	/* check body invalid keys */
	serializedItem := make(map[string]interface{})
	err = json.Unmarshal([]byte(body), &serializedItem)
	if err != nil {
		return nil, nil, err
	}
	validKey := false
	for key, _ := range serializedItem {
		validKey = false
		for _, k := range requiredKeys {
			if k == key {
				validKey = true
			}
		}
		if validKey == false {
			return nil, nil, errors.New("parseBody: bad request")
		}
	}
	/* fill T with body */
	if err := json.Unmarshal(body, &newItem); err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			return nil, nil, err
		}
	}
	return newItem, body, nil
}

/* Patch existing item */
func patchHandler(f func(b []byte, id string, role string, user_id int) int, T interface{}, requiredKeys []string, checkAuth bool) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if checkAuth && mux.Vars(r)["id"] != strconv.Itoa(ctx.ID) && ctx.Role != "admin" {
			if ok, _ := in_array(possibleRoles, ctx.Role); !ok {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		} else {
			_, b, err := parseBody(T, w, r, requiredKeys)
			if err != nil {
				respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
			} else {
				code := f(b, mux.Vars(r)["id"], ctx.Role, ctx.ID)
				switch code {
				case 202:
					respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
				case 403:
					respond(w, http.StatusForbidden, model.Exception{Message: http.StatusText(http.StatusForbidden)})
				case 404:
					respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
				case 500:
					respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				default:
					respond(w, http.StatusUnprocessableEntity, model.Exception{Message: http.StatusText(http.StatusUnprocessableEntity)})
				}
			}
		}
	})
}

func validAccountHandler(f func(ct string, token string) int, ct string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		code := f(ct, mux.Vars(r)["token"])
		switch code {
		case 202:
			respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
		case 409:
			respond(w, http.StatusConflict, model.Exception{Message: http.StatusText(http.StatusConflict)})
		case 500:
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	})
}

/* Create new item */
func createHandler(f func(T interface{}) (int64, error), T interface{}, requiredKeys []string, requiredAccountType string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if requiredAccountType != "any" {
			ctx := getAuthCtx(r)
			if ctx.Role != requiredAccountType && ctx.Role != "admin" {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		}
		newItem, _, err := parseBody(T, w, r, requiredKeys)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)
		} else {
			if id, err := f(newItem); err != nil {
				w.WriteHeader(http.StatusConflict)
				fmt.Println(err)
			} else {
				w.WriteHeader(http.StatusCreated)
				if err := json.NewEncoder(w).Encode(id); err != nil {
					fmt.Println(err)
				}
			}
		}
	})
}

/* Password forgot */
func forgotPasswordHandler(f func(T interface{}) int, T interface{}, requiredKeys []string, requiredAccountType string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if requiredAccountType != "any" {
			ctx := getAuthCtx(r)
			if ctx.Role != requiredAccountType {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		}
		newItem, _, err := parseBody(T, w, r, requiredKeys)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)
		} else {
			code := f(newItem)
			switch code {
			case 202:
				respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
			case 404:
				respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
			case 500:
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			default:
				respond(w, http.StatusUnprocessableEntity, model.Exception{Message: http.StatusText(http.StatusUnprocessableEntity)})
			}
		}
	})
}

func updatePasswordHandler(f func(T interface{}, token string) int, T interface{}, requiredKeys []string, requiredAccountType string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if requiredAccountType != "any" {
			ctx := getAuthCtx(r)
			if ctx.Role != requiredAccountType {
				respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
			}
		}
		newItem, _, err := parseBody(T, w, r, requiredKeys)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)
		} else {
			code := f(newItem, mux.Vars(r)["token"])
			switch code {
			case 202:
				respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
			case 404:
				respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
			case 500:
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			default:
				respond(w, http.StatusUnprocessableEntity, model.Exception{Message: http.StatusText(http.StatusUnprocessableEntity)})
			}
		}
	})
}

/* Take care of generic identification */
func createAuthHandler(f func(T interface{}) (interface{}, error), T interface{}, requiredKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		newItem, _, err := parseBody(T, w, r, requiredKeys)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println(err)
		} else {
			if user, err := f(newItem); err != nil {
				w.WriteHeader(http.StatusUnauthorized)
				fmt.Println(err)
			} else {
				if err := json.NewEncoder(w).Encode(user); err != nil {
					fmt.Println(err.Error())
					respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				}
			}
		}
	})
}

/* Check for request headers */
func checkHeaders(inner http.Handler, route model.Route) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if route.AuthRequired {
			if claims, err := parseJwt(w, r); err != nil {
				respond(w, http.StatusUnauthorized, model.Exception{Message: err.Error()})
				return
			} else {
				inner.ServeHTTP(w, r.WithContext(generateAuthCtx(r, claims)))
			}
		} else {
			inner.ServeHTTP(w, r)
		}
	})
}

/* Add headers on response */
func addDefaultHeaders(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
		w.Header().Add("Content-Type", "application/json; charset=UTF-8")
		inner.ServeHTTP(w, r)
	})
}
