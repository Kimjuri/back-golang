package api

import (
	"back/configuration"
	"back/db"
	"back/mailer"
	"back/model"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"regexp"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/lhecker/argon2"
)

func getRiderHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "rider" && ctx.Role != "partner" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if r, err := db.GetRider(mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "rider" && r.ID != ctx.ID {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			r.Password = ""
			if err := enc.Encode(r); err != nil {
				w.WriteHeader(http.StatusNotFound)
			}
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getFreeRiders(w http.ResponseWriter, r *http.Request) {
	if r, err := db.GetFreeRiders(); err == nil {
		enc := json.NewEncoder(w)
		enc.SetEscapeHTML(false)
		if err := enc.Encode(r); err != nil {
			w.WriteHeader(http.StatusNotFound)
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getRiderBill(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if ctx.Role != "admin" && ctx.Role != "rider" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if ctx.Role == "rider" && ctx.ID != id {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		var total, average int
		var er, e error
		var history []*model.History
		q := r.URL.Query()
		lq := len(q)
		if lq == 0 {
			total, err = db.GetAllCurrentRiderPayment(id)
			history, e = db.GetHistoryCurrentRider(id)
			average, er = db.GetAverageCurrentRider(id)
			if err != nil || e != nil || er != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		} else {
			start, ok := r.URL.Query()["start"]
			if !ok || len(start[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			end, ok := r.URL.Query()["end"]
			if !ok || len(end[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
			if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			total, err = db.GetAllRiderPayment(id, start[0], end[0])
			history, e = db.GetHistoryRider(id, start[0], end[0])
			average, er = db.GetAverageRider(id, start[0], end[0])
			if err != nil || e != nil || er != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		}
		data := make(map[string]interface{})
		data["total"] = total
		data["average"] = average
		data["history"] = history
		if err := json.NewEncoder(w).Encode(data); err != nil {
			fmt.Println(err.Error())
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	}
}

func getRiderStatsAdmin(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		q := r.URL.Query()
		lq := len(q)
		var total, time int
		var average float32
		var err error
		if lq == 0 {
			total, time, average, err = db.GetCurrentRiderStatAdmin()
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		} else {
			start, ok := r.URL.Query()["start"]
			if !ok || len(start[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			end, ok := r.URL.Query()["end"]
			if !ok || len(end[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
			if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			total, time, average, err = db.GetRiderStatAdmin(start[0], end[0])
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		}
		data := make(map[string]interface{})
		data["total"] = total
		data["time"] = time
		data["average"] = average
		if err := json.NewEncoder(w).Encode(data); err != nil {
			fmt.Println(err.Error())
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	}
}

func adminValidRider(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id := mux.Vars(r)["id"]
	if ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if err := db.ValidRider(id); err != nil {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	} else {
		respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
	}
}

func patchRider(b []byte, id string, role string, user_id int) int {
	if role != "rider" && role != "admin" {
		return 403
	}
	r, err := db.GetRider(id)
	if err != nil {
		return 404
	}
	riderMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(b), &riderMap)
	if err != nil {
		return 500
	}
	for key, value := range riderMap {
		switch key {
		case "firstname":
			if IsName(value.(string)) == false {
				fmt.Errorf("patchRider : firstname not valid")
				return 500
			}
			r.FirstName = value.(string)
		case "lastname":
			if IsName(value.(string)) == false {
				fmt.Errorf("patchRider : lastname not valid")
				return 500
			}
			r.LastName = value.(string)
		case "email":
			if IsEmail(value.(string)) == false {
				fmt.Errorf("patchRider : email not valid")
				return 500
			}
			r.Email = value.(string)
		case "phonenumber":
			if IsPhoneNumber(value.(string)) == false {
				fmt.Errorf("patchRider : phone number not valid")
				return 500
			}
			r.PhoneNumber = value.(string)
		case "siret":
			if IsSiret(value.(string)) == false {
				fmt.Errorf("patchRider : siret not valid")
				return 500
			}
			r.Siret = value.(string)
		case "rating":
			r.Rating = value.(int)
		case "password":
			r.Password = value.(string)
			cfg := argon2.DefaultConfig()
			raw, err := cfg.Hash([]byte(r.Password), nil)
			if err != nil {
				fmt.Printf("Error during hashing: %s\n", err.Error())
				return 500
			}
			r.Password = string(raw.Encode())
		case "iban":
			r.Iban = value.(string)
		case "longitude":
			r.Longitude = value.(float64)
		case "latitude":
			r.Latitude = value.(float64)
		case "free":
			r.Free = value.(bool)
		}
	}
	db.UpdateRider(r.FirstName, r.LastName, r.Email, r.PhoneNumber, r.Rating, r.Password, r.Iban, r.Longitude, r.Latitude, r.Free, r.Siret, id)
	return 202
}

func deleteRiderHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	ctx := getAuthCtx(r)
	if (ctx.Role != "rider" && ctx.Role != "admin") || (ctx.Role == "rider" && id != strconv.Itoa(ctx.ID)) {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if db.DeleteRider(id) /* err */ != nil {
		w.WriteHeader(http.StatusNotFound)
	}
}

func forgotPasswordRider(T interface{}) int {
	fp := (reflect.ValueOf(T)).Interface().(*model.ForgotPassword)
	if exist, err := db.CheckRiderExistingAccount(fp.Email); err != nil {
		fmt.Errorf("forgotPasswordRider : %s\n", err.Error())
		return 500
	} else {
		if exist == false {
			fmt.Errorf("ForgotPasswordRider : Account doesn't exists")
			return 404
		}
	}
	token, err := generateForgotPasswordToken(fp)
	if err != nil {
		fmt.Errorf("ForgotPasswordRider : Error creating token")
		return 500
	}
	if err := mailer.SendForgotPasswordEmail(token, fp.Email, "riders"); err != nil {
		fmt.Errorf("ForgotPasswordRider : Couldn't send email")
		return 500
	}
	return 202
}

func updatePasswordRider(T interface{}, tokenString string) int {
	up := (reflect.ValueOf(T)).Interface().(*model.UpdatePassword)
	token, err := jwt.ParseWithClaims(tokenString, &model.ForgotPasswordClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		fmt.Errorf(err.Error())
		return 500
	}
	if claims, ok := token.Claims.(*model.ForgotPasswordClaims); ok && token.Valid {
		if e, err := db.CheckRiderExistingAccount(claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		} else if e != true {
			fmt.Errorf("updatePasswordRider : Account does not exist")
			return 404
		}
		cfg := argon2.DefaultConfig()
		raw, err := cfg.Hash([]byte(up.NewPassword), nil)
		if err != nil {
			fmt.Errorf("updatePasswordRider : could not hash password : %s", err.Error())
			return 500
		}
		if err := db.UpdatePasswordRider(string(raw.Encode()), claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		}
	} else {
		fmt.Errorf(err.Error())
		return 500
	}
	return 202
}

func createRider(T interface{}) (int64, error) {
	c := (reflect.ValueOf(T)).Interface().(*model.Rider)
	cfg := argon2.DefaultConfig()
	raw, err := cfg.Hash([]byte(c.Password), nil)
	if err != nil {
		return 0, fmt.Errorf("createRider : could not hash password : %s", err.Error())
	}
	if IsName(c.FirstName) && IsName(c.LastName) && IsEmail(c.Email) && IsPhoneNumber(c.PhoneNumber) && IsSiret(c.Siret) {
		if configuration.Configuration.EMAIL_validation == true {
			/*******CODE FOR VALIDATION ACCOUNT***********/
			token, err := generateCreateToken(c, string(raw.Encode()))
			if err != nil {
				return 0, fmt.Errorf("createRider : Error creating token")
			}
			if err := mailer.SendVerificationEmail(token, c.Email, "riders"); err != nil {
				return 0, fmt.Errorf("createRider : Couldn't send email")
			}
			return 0, nil
		}
		/*******CODE FOR NO VALIDATION ACCOUNT***********/
		fmt.Errorf("Create Without Validation")
		return db.CreateRider(c.FirstName, c.LastName, c.Email, c.PhoneNumber, 0, string(raw.Encode()), c.Siret)
	}
	return 0, fmt.Errorf("createRider : wrong syntax")
}
