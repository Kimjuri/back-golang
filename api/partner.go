package api

import (
	"back/configuration"
	"back/db"
	"back/mailer"
	"back/model"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"regexp"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/lhecker/argon2"
)

/* Get partner purchases stats */
func getPartnerStatsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id := mux.Vars(r)["id"]
	start, ok := r.URL.Query()["start"]
	if !ok || len(start[0]) < 1 {
		respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
		return
	}
	end, ok := r.URL.Query()["end"]
	if !ok || len(end[0]) < 1 {
		respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
		return
	}
	reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
	if err != nil {
		respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		return
	}
	if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
		respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
		return
	}
	if ctx.Role != "partner" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if ctx.Role == "partner" && strconv.Itoa(ctx.ID) != id {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if stats, err := db.GetPartnerPurchasesStats(id, start[0], end[0]); err == nil {
		enc := json.NewEncoder(w)
		if err := enc.Encode(stats); err != nil {
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	} else {
		respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
	}
}

func getPartnerStatsClient(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if ctx.Role != "admin" && ctx.Role != "partner" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if ctx.Role == "partner" && ctx.ID != id {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		var clientsHabit []*model.ClientStat
		q := r.URL.Query()
		lq := len(q)
		if lq == 0 {
			clientsHabit, err = db.GetCurrentClientsHabits(id)
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		} else {
			start, ok := r.URL.Query()["start"]
			if !ok || len(start[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			end, ok := r.URL.Query()["end"]
			if !ok || len(end[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
			if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			clientsHabit, err = db.GetClientsHabits(id, start[0], end[0])
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		}
		if err := json.NewEncoder(w).Encode(clientsHabit); err != nil {
			fmt.Println(err.Error())
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	}
}

func getPartnerStatsClientAdmin(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		var clientsHabit []*model.ClientStat
		var err error
		q := r.URL.Query()
		lq := len(q)
		if lq == 0 {
			clientsHabit, err = db.GetCurrentClientsHabitsAdmin()
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		} else {
			start, ok := r.URL.Query()["start"]
			if !ok || len(start[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			end, ok := r.URL.Query()["end"]
			if !ok || len(end[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
			if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			clientsHabit, err = db.GetClientsHabitsAdmin(start[0], end[0])
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		}
		if err := json.NewEncoder(w).Encode(clientsHabit); err != nil {
			fmt.Println(err.Error())
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	}
}

/* Get partner informations */
func getPartnerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "partner" && ctx.Role != "client" && ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if p, err := db.GetPartner(mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "partner" && p.ID != ctx.ID {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			p.Password = ""
			if err := enc.Encode(p); err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

/* Delete a partner */
func deletePartnerHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	ctx := getAuthCtx(r)
	if (ctx.Role != "partner" && ctx.Role != "admin") || (ctx.Role == "partner" && id != strconv.Itoa(ctx.ID)) {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if db.DeletePartner(id) /* err */ != nil {
		w.WriteHeader(http.StatusNotFound)
	}
}

func adminValidPartner(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id := mux.Vars(r)["id"]
	if ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if err := db.ValidPartner(id); err != nil {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	} else {
		respond(w, http.StatusAccepted, model.Exception{Message: http.StatusText(http.StatusAccepted)})
	}
}

/* Partially update partner informations */
func patchPartner(b []byte, id string, role string, user_id int) int {
	if role != "partner" && role != "admin" {
		return 403
	}
	p, err := db.GetPartner(id)
	if err != nil {
		return 404
	}
	partnerMap := make(map[string]interface{})
	if err = json.Unmarshal([]byte(b), &partnerMap); err != nil {
		return 500
	}
	for key, value := range partnerMap {
		switch key {
		case "name":
			if IsName(value.(string)) == false {
				fmt.Errorf("patchPartner : name not valid")
				return 500
			}
			p.Name = value.(string)
		case "street":
			p.Street = value.(string)
		case "zipcode":
			p.ZipCode = value.(string)
		case "city":
			p.City = value.(string)
		case "additionaladdress":
			p.AdditionalAddress = value.(string)
		case "email":
			if IsEmail(value.(string)) == false {
				fmt.Errorf("patchPartner : email not valid")
				return 500
			}
			p.Email = value.(string)
		case "logo":
			p.Logo = value.(string)
		case "phonenumber":
			if IsPhoneNumber(value.(string)) == false {
				fmt.Errorf("patchPartner : phone number not valid")
				return 500
			}
			p.PhoneNumber = value.(string)
		case "password":
			p.Password = value.(string)
			cfg := argon2.DefaultConfig()
			raw, err := cfg.Hash([]byte(p.Password), nil)
			if err != nil {
				fmt.Printf("Error during hashing: %s\n", err.Error())
				return 500
			}
			p.Password = string(raw.Encode())
		case "siret":
			if IsSiret(value.(string)) == false {
				fmt.Errorf("patchPartner : siret not valid")
				return 500
			}
			p.Siret = value.(string)
		}
	}
	db.UpdateAddressPartner(p.Street, p.ZipCode, p.City, p.AdditionalAddress, id)
	db.UpdatePartner(p.Name, p.Email, p.PhoneNumber, p.Password, p.Logo, p.Siret, id)
	return 202
}

func forgotPasswordPartner(T interface{}) int {
	fp := (reflect.ValueOf(T)).Interface().(*model.ForgotPassword)
	if exist, err := db.CheckPartnerExistingAccount(fp.Email); err != nil {
		fmt.Errorf("forgotPasswordPartner : %s\n", err.Error())
		return 500
	} else {
		if exist == false {
			fmt.Errorf("ForgotPasswordPartner : Account doesn't exists")
			return 404
		}
	}
	token, err := generateForgotPasswordToken(fp)
	if err != nil {
		fmt.Errorf("ForgotPasswordPartner : Error creating token")
		return 500
	}
	if err := mailer.SendForgotPasswordEmail(token, fp.Email, "partners"); err != nil {
		fmt.Errorf("ForgotPasswordPartner : Couldn't send email")
		return 500
	}
	return 202
}

func updatePasswordPartner(T interface{}, tokenString string) int {
	up := (reflect.ValueOf(T)).Interface().(*model.UpdatePassword)
	token, err := jwt.ParseWithClaims(tokenString, &model.ForgotPasswordClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		fmt.Errorf(err.Error())
		return 500
	}
	if claims, ok := token.Claims.(*model.ForgotPasswordClaims); ok && token.Valid {
		if e, err := db.CheckPartnerExistingAccount(claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		} else if e != true {
			fmt.Errorf("updatePasswordPartner : Account does not exist")
			return 404
		}
		cfg := argon2.DefaultConfig()
		raw, err := cfg.Hash([]byte(up.NewPassword), nil)
		if err != nil {
			fmt.Errorf("updatePasswordPartner : could not hash password : %s", err.Error())
			return 500
		}
		if err := db.UpdatePasswordPartner(string(raw.Encode()), claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		}
	} else {
		fmt.Errorf(err.Error())
		return 500
	}
	return 202
}

func createPartner(T interface{}) (int64, error) {
	c := (reflect.ValueOf(T)).Interface().(*model.Partner)
	cfg := argon2.DefaultConfig()
	raw, err := cfg.Hash([]byte(c.Password), nil)
	if err != nil {
		return 0, fmt.Errorf("createPartner : could not hash password : %s", err.Error())
	}
	if IsName(c.Name) && IsEmail(c.Email) && IsPhoneNumber(c.PhoneNumber) && IsSiret(c.Siret) {
		/*******CODE FOR VALIDATION ACCOUNT***********/
		if configuration.Configuration.EMAIL_validation == true {
			token, err := generateCreateToken(c, string(raw.Encode()))
			if err != nil {
				return 0, fmt.Errorf("createPartner : Error creating token")
			}
			if err := mailer.SendVerificationEmail(token, c.Email, "partners"); err != nil {
				return 0, fmt.Errorf("createPartner : Couldn't send email")
			}
			return 0, nil
		}
		/*******CODE FOR NO VALIDATION ACCOUNT***********/
		fmt.Errorf("Create Without Validation")
		id, err := db.CreateAddress(c.Street, c.ZipCode, c.City, c.AdditionalAddress)
		if err != nil {
			return 0, err
		}
		return db.CreatePartner(c.Name, id, c.Email, c.PhoneNumber, string(raw.Encode()), c.Logo, c.Siret)
	}
	return 0, fmt.Errorf("createPartner : wrong syntax")
}
