package api

import "regexp"

func IsPhoneNumber(s string) bool {
	exp := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
	return exp.MatchString(s)
}

func IsEmail(s string) bool {
	exp := regexp.MustCompile(`^[\w\d._%+-]+@([\w\d.-]+\.[\w]{2,}|\d+\.\d+\.\d+\.\d+)$`)
	return exp.MatchString(s)
}

func IsName(s string) bool {
	exp := regexp.MustCompile(`^[\w\sýþÿÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçËÌÜÝÞÍèéêëìíîïðñòóôõöøùúûüýëîïðËÌÍ]+$`)
	return exp.MatchString(s)
}

func IsSiret(s string) bool {
	exp := regexp.MustCompile(`^[0-9]{14}$`)
	return exp.MatchString(s)
}
