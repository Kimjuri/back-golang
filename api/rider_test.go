package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

/*func TestRiderCreation(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"firstname": "Firstname",
		"lastname": "Lastname",
		"email": "test@test.com",
		"phonenumber": "0601020304",
		"password": "test_pw"
	}`)
	req, _ := http.NewRequest("POST", "/riders", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createHandler(createRider, &model.Rider{}, riderValidKeys, "any"))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusCreated, rr.Code)
}*/

func RiderTestPatch(t *testing.T, payload []byte, expecting int) {
	test.ReloadDB(t)
	req, err := http.NewRequest("PATCH", "/riders/1", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(patchHandler(patchRider, &model.Rider{}, riderValidKeys, AuthRequired))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, expecting, rr.Code)
}

func TestRiderFetchAsAdmin(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/riders/1", nil) /* Test route */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil { /* authenticate request */
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getRiderHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims))) /* Simulate http service with request context */

	test.CheckResponseCode(t, http.StatusOK, rr.Code) /* Test response status */

	var rider model.Rider
	if err := json.Unmarshal([]byte(rr.Body.String()), &rider); err != nil { /* Retrieve json data from response */
		t.Fatal(err)
	}
	test.MyAssert(t, "rider id", 1, rider.ID)
	test.MyAssert(t, "rider email", "re1@rider.fr", rider.Email)
}

func TestRiderPatchFirstname(t *testing.T) {
	payload := []byte(`{
		"firstname": "Firstnameupdated"
	}`)
	RiderTestPatch(t, payload, http.StatusAccepted)
}

func TestRiderPatchWrongFirstname(t *testing.T) {
	payload := []byte(`{
		"firstname": "@Firstname"
	}`)
	RiderTestPatch(t, payload, http.StatusInternalServerError)
}

func TestRiderPatchLastname(t *testing.T) {
	payload := []byte(`{
		"lastname": "Lastnameupdated"
	}`)
	RiderTestPatch(t, payload, http.StatusAccepted)
}

func TestRiderPatchWrongLastname(t *testing.T) {
	payload := []byte(`{
		"lastname": "@Lastname"
	}`)
	RiderTestPatch(t, payload, http.StatusInternalServerError)
}

func TestRiderPatchEmail(t *testing.T) {
	payload := []byte(`{
		"email": "thisis.atest@test.com"
	}`)
	RiderTestPatch(t, payload, http.StatusAccepted)
}

func TestRiderPatchWrongEmail(t *testing.T) {
	payload := []byte(`{
		"email": "wrong@email@gmail.com"
	}`)
	RiderTestPatch(t, payload, http.StatusInternalServerError)
}

func TestRiderPatchPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "+33601020304"
	}`)
	RiderTestPatch(t, payload, http.StatusAccepted)
}

func TestRiderPatchWrongPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "not a phone number"
	}`)
	RiderTestPatch(t, payload, http.StatusInternalServerError)
}
