package api

import (
	"back/configuration"
	"back/db"
	"back/model"
	"context"
	"errors"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"net/http"
	"reflect"
	"strings"
	"time"
)

/* Memory isolation of the secret ? */
var jwtSecret = []byte(configuration.Configuration.JWT_Secret)

var possibleRoles = []string{"client", "partner", "rider", "deliveries_manager", "admin"}

/* Generate a jwt token with a custom model.Claims claims */
func generateToken(accountID int, accountType string) (string, error) {
	token := jwt.NewWithClaims(jwt.GetSigningMethod(configuration.Configuration.JWT_Method), &model.MyClaims{
		jwt.StandardClaims{
			//Id:        sessionID,
			ExpiresAt: time.Now().Add(time.Hour * configuration.Configuration.JWT_Lifetime).Unix(),
			Issuer:    "GroomShop",
		},
		accountID,
		accountType,
	})
	if tokenstring, err := token.SignedString(jwtSecret); err != nil {
		return "", err
	} else {
		return tokenstring, nil
	}
}

/* Check wether the Authentication header is set or not and may return its value */
func isAuthenticationHeaderSet(r *http.Request) (string, error) {
	authorizationHeader := r.Header.Get("authorization")
	if authorizationHeader != "" {
		bearerToken := strings.Split(authorizationHeader, " ")
		if len(bearerToken) == 2 {
			return bearerToken[1], nil
		} else {
			return "", errors.New("Syntax error on authorization token")
		}
	} else {
		return "", errors.New("An authorization token is required")
	}
}

func generateAuthCtx(r *http.Request, claims jwt.Claims) context.Context {
	myClaims, _ := claims.(*model.MyClaims)
	authCtx := &model.AuthContext{myClaims.ID, myClaims.Role}
	return context.WithValue(r.Context(), model.JwtKey, authCtx)
}

func getAuthCtx(r *http.Request) *model.AuthContext {
	return r.Context().Value(model.JwtKey).(*model.AuthContext)
}

/* Parse jwt */
func parseJwt(w http.ResponseWriter, r *http.Request) (jwt.Claims, error) {
	if bearerToken, err := isAuthenticationHeaderSet(r); err != nil {
		return nil, err
	} else {
		if token, err := jwt.ParseWithClaims(bearerToken, &model.MyClaims{}, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("There was an error")
			}
			return jwtSecret, nil
		}); err != nil || token.Valid != true {
			fmt.Println("parseJwt: ", err.Error())
			return nil, errors.New("Invalid token")
		} else {
			return token.Claims, nil
		}
	}
}

func authClient(T interface{}) (interface{}, error) {
	c := (reflect.ValueOf(T)).Interface().(*model.Auth)
	if c, err := db.AuthClient(c.Email, c.Password); err != nil {
		return nil, err
	} else {
		if token, err := generateToken(c.ID, "client"); err != nil {
			return nil, err
		} else {
			c.Token = token
			c.Password = ""
			return c, nil
		}
	}
}

func authPartner(T interface{}) (interface{}, error) {
	p := (reflect.ValueOf(T)).Interface().(*model.Auth)
	if p, err := db.AuthPartner(p.Email, p.Password); err != nil {
		return nil, err
	} else {
		if token, err := generateToken(p.ID, "partner"); err != nil {
			return nil, err
		} else {
			p.Token = token
			p.Password = ""
			return p, nil
		}
	}
}

func authRider(T interface{}) (interface{}, error) {
	r := (reflect.ValueOf(T)).Interface().(*model.Auth)
	if r, err := db.AuthRider(r.Email, r.Password); err != nil {
		return nil, err
	} else {
		if token, err := generateToken(r.ID, "rider"); err != nil {
			return nil, err
		} else {
			r.Token = token
			r.Password = ""
			return r, nil
		}
	}
}

func authAdmin(T interface{}) (interface{}, error) {
	c := (reflect.ValueOf(T)).Interface().(*model.Auth)
	if c, err := db.AuthAdmin(c.Email, c.Password); err != nil {
		return nil, err
	} else {
		if token, err := generateToken(c.ID, "admin"); err != nil {
			return nil, err
		} else {
			c.Token = token
			return c, nil
		}
	}
}

func authApplication(T interface{}) (interface{}, error) {
	r := (reflect.ValueOf(T)).Interface().(*model.AuthApplication)
	if r, err := db.AuthApplication(r.IDApplication, r.Password); err != nil {
		return nil, err
	} else {
		if token, err := generateToken(r.ID, r.Role); err != nil {
			return nil, err
		} else {
			r.Token = token
			return r, nil
		}
	}
}
