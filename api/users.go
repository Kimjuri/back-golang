package api

import (
	"back/db"
	"back/model"
	"encoding/json"
	"fmt"
	"net/http"
)

func getUsersHandler(validKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "admin" && ctx.Role != "client" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			var err error
			q := r.URL.Query()
			lq := len(q)
			if lq > 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Too much parameters"})
			} else {
				if lq == 0 && ctx.Role == "admin" {
					if u, error := db.GetUsers(); error != nil {
						err = error
					} else {
						if err2 := json.NewEncoder(w).Encode(u); err2 != nil {
							fmt.Println(err.Error())
							respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
						}
					}
				} else {
					for _, k := range validKeys {
						if q[k] != nil && k == "t" {
							switch q[k][0] {
							case "clients":
								if ctx.Role != "admin" {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									err = nil
								} else {
									if c, error := db.GetAllClients(); error != nil {
										err = error
									} else {
										if err2 := json.NewEncoder(w).Encode(c); err2 != nil {
											fmt.Println(err.Error())
											respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
										}
									}
								}
							case "riders":
								if ctx.Role != "admin" {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									err = nil
								} else {
									if r, error := db.GetAllRiders(); error != nil {
										err = error
									} else {
										if err2 := json.NewEncoder(w).Encode(r); err2 != nil {
											fmt.Println(err.Error())
											respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
										}
									}
								}
							case "partners":
								if p, error := db.GetAllPartners(); error != nil {
									err = error
								} else {
									if err2 := json.NewEncoder(w).Encode(p); err2 != nil {
										fmt.Println(err.Error())
										respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
									}
								}
							}
						}
					}
				}
				if err != nil {
					respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
				}
			}

		}
	})
}

func getValidAccount(validKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "admin" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			var err error
			q := r.URL.Query()
			lq := len(q)
			if lq > 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Too much parameters"})
			} else {
				if lq == 0 {
					if u, error := db.GetValidAccounts(); error != nil {
						err = error
					} else {
						if err2 := json.NewEncoder(w).Encode(u); err2 != nil {
							fmt.Println(err.Error())
							respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
						}
					}
				} else {
					for _, k := range validKeys {
						if q[k] != nil && k == "t" {
							switch q[k][0] {
							case "riders":
								if r, error := db.GetAllValidRiders(); error != nil {
									err = error
								} else {
									if err2 := json.NewEncoder(w).Encode(r); err2 != nil {
										fmt.Println(err.Error())
										respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
									}
								}
							case "partners":
								if p, error := db.GetAllValidPartners(); error != nil {
									err = error
								} else {
									if err2 := json.NewEncoder(w).Encode(p); err2 != nil {
										fmt.Println(err.Error())
										respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
									}
								}
							}
						}
					}
				}
				if err != nil {
					respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
				}
			}

		}
	})
}

func getNonValidAccount(validKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "admin" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			var err error
			q := r.URL.Query()
			lq := len(q)
			if lq > 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Too much parameters"})
			} else {
				if lq == 0 {
					if u, error := db.GetNonValidAccounts(); error != nil {
						err = error
					} else {
						if err2 := json.NewEncoder(w).Encode(u); err2 != nil {
							fmt.Println(err.Error())
							respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
						}
					}
				} else {
					for _, k := range validKeys {
						if q[k] != nil && k == "t" {
							switch q[k][0] {
							case "riders":
								if r, error := db.GetAllNonValidRiders(); error != nil {
									err = error
								} else {
									if err2 := json.NewEncoder(w).Encode(r); err2 != nil {
										fmt.Println(err.Error())
										respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
									}
								}
							case "partners":
								if p, error := db.GetAllNonValidPartners(); error != nil {
									err = error
								} else {
									if err2 := json.NewEncoder(w).Encode(p); err2 != nil {
										fmt.Println(err.Error())
										respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
									}
								}
							}
						}
					}
				}
				if err != nil {
					respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
				}
			}

		}
	})
}
