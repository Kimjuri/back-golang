package api

import (
	"back/configuration"
	"back/db"
	"back/model"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"reflect"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func createPurchase(T interface{}) (int64, error) {
	p := (reflect.ValueOf(T)).Interface().(*model.Purchase)
	t := time.Now()
	tf := t.Format("2006-01-02 15:04:05")
	return db.CreatePurchase(0, p.IDClient, p.IDPartner, 4, tf)
}

func setPurchaseReceiptHandler(w http.ResponseWriter, r *http.Request) {
	imageDirectory := configuration.Configuration.Images_PATH
	ctx := getAuthCtx(r)
	if ctx.Role != "partner" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		return
	} else if d, err := db.GetPurchases("SELECT * FROM purchases WHERE id = ?;", mux.Vars(r)["id"]); err == nil {
		if flag, err := db.CheckReceiptPartnerId(strconv.Itoa(d[0].ID), strconv.Itoa(ctx.ID)); err == nil && flag == true {
			if file, header, err := r.FormFile("receipt"); err == nil {
				defer file.Close()
				ext := path.Ext(header.Filename)
				if name, err := generateImageFileName(ext); err == nil {
					if nameWatermarked, err := generateImageFileName(".jpeg"); err == nil {
						if receipt, err := db.GetReceiptByPurchase(mux.Vars(r)["id"]); err == nil {
							os.Remove(path.Join(imageDirectory, receipt.Receipt))
							os.Remove(path.Join(imageDirectory, receipt.ReceiptWatermarked))
							db.DeleteReceiptByDelivery(mux.Vars(r)["id"])
						}
						if db.CreateReceipt(name, nameWatermarked, d[0].ID); err == nil {
							if copyfile, err := os.OpenFile(path.Join(imageDirectory, name), os.O_WRONLY|os.O_CREATE, 0666); err == nil {
								if _, err := io.Copy(copyfile, file); err == nil {
									copyfile.Close()
									if err := applyWatermarkAndSave(path.Join(imageDirectory, name), path.Join(imageDirectory, nameWatermarked)); err == nil {
										io.WriteString(w, fmt.Sprintln(`{ "status": "receipt saved" }`))
										return
									}
								}
							}
						}
					}
				}
			}
		}
	}
	respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
}

func getPurchaseInfoHandler() func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "client" && ctx.Role != "admin" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			if purchase_id, err := strconv.Atoi(mux.Vars(r)["id"]); err != nil {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Wrong ID"})
			} else {
				if purchases, err := db.GetPurchases("SELECT * FROM purchases WHERE id = ?;", purchase_id); err != nil {
					respond(w, http.StatusBadRequest, model.Exception{Message: "Wrong ID"})
				} else {
					if len(purchases) == 0 {
						respond(w, http.StatusBadRequest, model.Exception{Message: "Wrong ID"})
						return
					}
					purchase := purchases[0]
					data := make(map[string]interface{})
					if client, err := db.GetClient(strconv.Itoa(purchase.IDClient)); err != nil {
						if ctx.Role == "client" {
							respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
							return
						} else {
							data["client"] = struct{}{}
						}
					} else {
						if ctx.Role == "client" && ctx.ID != client.ID {
							respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
							return
						} else {
							client.Password = ""
							data["client"] = client
						}
					}
					if partner, err := db.GetPartner(strconv.Itoa(purchase.IDPartner)); err != nil {
						data["partner"] = struct{}{}
					} else {
						partner.Token = ""
						partner.Password = ""
						data["partner"] = partner
					}
					enc := json.NewEncoder(w)
					enc.SetEscapeHTML(false)
					if err := enc.Encode(data); err != nil {
						respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
					}
				}
			}
		}
	})
}

func getPurchaseByIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "client" && ctx.Role != "partner" && ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if p, err := db.GetPurchases("SELECT * FROM purchases WHERE id=?;", mux.Vars(r)["id"]); err == nil {
		enc := json.NewEncoder(w)
		enc.SetEscapeHTML(false)
		if receipt, err := db.GetReceiptByPurchase(mux.Vars(r)["id"]); err == nil {
			if ctx.Role == "client" || ctx.Role == "admin" {
				p[0].Receipt = configuration.Configuration.Images_URL + receipt.Receipt
			}
			if ctx.Role == "rider" {
				p[0].Receipt = configuration.Configuration.Images_URL + receipt.ReceiptWatermarked
			}
		}
		if err := enc.Encode(p[0]); err != nil {
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getPurchaseHandler(f func(string, string, string, string) ([]*model.Purchase, error), status string, validKeys []string) func(w http.ResponseWriter, r *http.Request) {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := getAuthCtx(r)
		if ctx.Role != "rider" && ctx.Role != "client" && ctx.Role != "partner" && ctx.Role != "admin" {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			q := r.URL.Query()
			lq := len(q)
			var d []*model.Purchase
			var err error
			if lq > 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: "Too much parameters"})
			} else if lq == 0 {
				if ctx.Role != "rider" && ctx.Role != "admin" {
					respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
					return
				} else {
					d, err = db.GetAllSpecifiedPurchases(status)
				}
			} else {
				found := false
				for _, k := range validKeys {
					if q[k] != nil {
						switch k {
						case "p":
							if ctx.Role != "rider" && ctx.Role != "partner" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								if ctx.Role == "partner" && strconv.Itoa(ctx.ID) != q[k][0] {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									return
								} else {
									d, err = f(ctx.Role, status, k, q[k][0])
								}
							}
						case "c":
							if ctx.Role != "rider" && ctx.Role != "client" && ctx.Role != "admin" {
								respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
								return
							} else {
								if ctx.Role == "client" && strconv.Itoa(ctx.ID) != q[k][0] {
									respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
									return
								} else {
									d, err = f(ctx.Role, status, k, q[k][0])
								}
							}
						}
						found = true
					}
				}
				if found != true {
					respond(w, http.StatusBadRequest, model.Exception{Message: "Unknow parameter(s)"})
				}
			}
			if err != nil {
				fmt.Println(err)
				respond(w, http.StatusBadRequest, model.Exception{Message: err.Error()})
			} else {
				if err := json.NewEncoder(w).Encode(d); err != nil {
					fmt.Println(err.Error())
					respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				}
			}
		}
	})
}

func getPurchase(as string, status string, key string, id string) ([]*model.Purchase, error) {
	var p []*model.Purchase
	var err error
	switch key {
	case "p":
		p, err = db.GetPartnerPurchases(status, id)
	case "c":
		p, err = db.GetClientPurchases(status, id)
	}
	for _, purchase := range p {
		id := purchase.ID
		if receipt, err := db.GetReceiptByPurchase(strconv.Itoa(id)); err == nil {
			if as == "client" || as == "admin" {
				purchase.Receipt = configuration.Configuration.Images_URL + receipt.Receipt
			}
			if as == "rider" {
				purchase.Receipt = configuration.Configuration.Images_URL + receipt.ReceiptWatermarked
			}
		}
	}
	return p, err
}

func patchPurchase(b []byte, id string, role string, user_id int) int {
	if role != "rider" && role != "admin" {
		return 403
	}
	p, err := db.GetPurchases("SELECT * FROM purchases WHERE id = ?;", id)
	if err != nil {
		return 404
	}
	purchasesMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(b), &purchasesMap)
	if err != nil {
		return 500
	}
	for _, value := range purchasesMap {
		if v, err := strconv.ParseInt(fmt.Sprintf("%v", value), 10, 64); err == nil {
			p[0].Status = v
		}
	}
	db.UpdatePurchase(p[0].IDDelivery, p[0].IDClient, p[0].IDPartner, p[0].Status, p[0].Date, id)
	return 202
}
