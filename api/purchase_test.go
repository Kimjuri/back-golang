package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPurchaseCreation(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idClient": 1,
		"idPartner": 2
	}`)
	req, _ := http.NewRequest("POST", "/purchases", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	if token, err := generateToken(5, "partner"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(createHandler(createPurchase, &model.Purchase{}, purchaseValidKeys, "partner"))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusCreated, rr.Code)
}

func TestPurchaseCreationAsAClient(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idClient": 1,
		"idPartner": 2
	}`)
	req, _ := http.NewRequest("POST", "/purchases", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	if token, err := generateToken(9, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(createHandler(createPurchase, &model.Purchase{}, purchaseValidKeys, "partner"))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestAllPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases", nil) /* Get all purchases */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(4, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "all", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All purchases number", 5, len(ps))
	test.MyAssert(t, "purchases[0] id", 1, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(4), ps[0].Status)
}

func TestPlacedPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/placed", nil) /* Get all placed purchases */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(5, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "purchase_placed", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Placed purchases number", 3, len(ps))
	test.MyAssert(t, "purchases[0] id", 1, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(4), ps[0].Status)
}

func TestPendingPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/pending", nil) /* Get all pending purchases */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(6, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "purchase_pending", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Pending purchases number", 1, len(ps))
	test.MyAssert(t, "purchases[0] id", 2, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(5), ps[0].Status)
}

func TestResolvedPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/resolved", nil) /* Get all resolved purchases */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(7, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "purchase_resolved", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Pending purchases number", 1, len(ps))
	test.MyAssert(t, "purchases[0] id", 3, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(6), ps[0].Status)
}

func TestResolvedForPartnerPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/resolved?p=1", nil) /* Get all resolved purchases for partner 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(8, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "purchase_resolved", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Pending purchases number", 1, len(ps))
	test.MyAssert(t, "purchases[0] id", 3, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(6), ps[0].Status)
}

func TestPendingForClientPurchasesFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/resolved?c=1", nil) /* get all resolved purchases for client 1 */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(9, "rider"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "purchase_resolved", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusOK, rr.Code)

	var ps []model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &ps); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "Pending purchases number", 1, len(ps))
	test.MyAssert(t, "purchases[0] id", 3, ps[0].ID)
	test.MyAssert(t, "purchases[0] id client", 1, ps[0].IDClient)
	test.MyAssert(t, "purchases[0] id partner", 1, ps[0].IDPartner)
	test.MyAssert(t, "purchases[0] status", int64(6), ps[0].Status)
}

func TestAllPurchasesFetchAsAClient(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases", nil) /* Get all purchases */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(6, "client"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseHandler(getPurchase, "all", validPurchaseQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))

	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestPurchaseByID(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/purchases/1", nil) /* Test route */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(5, "rider"); err != nil { /* authenticate request */
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPurchaseByIDHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims))) /* Simulate http service with request context */

	test.CheckResponseCode(t, http.StatusOK, rr.Code) /* Test response status */

	var purchase model.Purchase
	if err := json.Unmarshal([]byte(rr.Body.String()), &purchase); err != nil { /* Retrieve json data from response */
		t.Fatal(err)
	}
	test.MyAssert(t, "purchase id", 1, purchase.ID)
	test.MyAssert(t, "purchase idDelivery", 1, purchase.IDDelivery)
	test.MyAssert(t, "purchase idClient", 1, purchase.IDClient)
	test.MyAssert(t, "purchase idPartner", 1, purchase.IDPartner)
	test.MyAssert(t, "purchase status", int64(4), purchase.Status)
}
