package api

import (
	"back/configuration"
	"back/db"
	"net/http"
)

type Ping struct {
	APIVersion string `json:"api_version"`
	DBVersion  string `json:"db_version"`
	LastUpdate string `json:"last_update"`
}

func pingHandler(w http.ResponseWriter, r *http.Request) {
	respond(w, http.StatusOK, Ping{
		APIVersion: configuration.Configuration.Version,
		DBVersion:  db.GetVersion(),
		LastUpdate: configuration.Configuration.LastUpdate,
	})
}
