package api

import (
	"back/configuration"
	"back/db"
	"back/mailer"
	"back/model"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/lhecker/argon2"
	"io"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

func getClientHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "client" && ctx.Role != "partner" && ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if c, err := db.GetClient(mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "client" && c.ID != ctx.ID {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			enc := json.NewEncoder(w)
			enc.SetEscapeHTML(false)
			c.Password = ""
			if err := enc.Encode(c); err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
			}
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getClientDeliveryStats(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		return
	}
	if ctx.Role != "admin" && ctx.Role != "client" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if ctx.Role == "client" && ctx.ID != id {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else {
		q := r.URL.Query()
		lq := len(q)
		var total int
		var clientDeliveryStat []*model.ClientDeliveryStat
		var err error
		if lq == 0 {
			total, clientDeliveryStat, err = db.GetCurrentClientDeliveryStat(id)
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		} else {
			start, ok := r.URL.Query()["start"]
			if !ok || len(start[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			end, ok := r.URL.Query()["end"]
			if !ok || len(end[0]) < 1 {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			reg, err := regexp.Compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$")
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
			if reg.MatchString(start[0]) == false || reg.MatchString(end[0]) == false {
				respond(w, http.StatusBadRequest, model.Exception{Message: http.StatusText(http.StatusBadRequest)})
				return
			}
			total, clientDeliveryStat, err = db.GetClientDeliveryStat(id, start[0], end[0])
			if err != nil {
				respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
				return
			}
		}
		data := make(map[string]interface{})
		data["total"] = total
		data["deliveries"] = clientDeliveryStat
		if err := json.NewEncoder(w).Encode(data); err != nil {
			fmt.Println(err.Error())
			respond(w, http.StatusInternalServerError, model.Exception{Message: http.StatusText(http.StatusInternalServerError)})
		}
	}
}

func setClientNewPictureHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "client" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if c, err := db.GetClient(mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "client" && c.ID != ctx.ID {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			file, header, err := r.FormFile("file")
			if err != nil {
				respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
			} else {
				defer file.Close()
				name := strings.Split(header.Filename, ".")
				if len(name) <= 1 {
					respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
				} else {
					imageUUID, err := uuid.NewUUID()
					if err != nil {
						respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
					} else {
						imagename := imageUUID.String() + "." + name[len(name)-1]
						oldimagename := c.Picture
						db.UpdateClientPicture(imagename, mux.Vars(r)["id"])
						path := "/home/"
						os.Remove(path + oldimagename)
						copyfile, err := os.OpenFile(path+imagename, os.O_WRONLY|os.O_CREATE, 0666)
						if err != nil {
							respond(w, http.StatusNotFound, model.Exception{Message: "Error"})
						} else {
							io.Copy(copyfile, file)
							defer copyfile.Close()
							io.WriteString(w, fmt.Sprintf(`{"url": "https://%s/images/%s"}`, configuration.Configuration.HOST, imagename))
						}
					}
				}
			}
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func getClientPictureHandler(w http.ResponseWriter, r *http.Request) {
	ctx := getAuthCtx(r)
	if ctx.Role != "client" && ctx.Role != "partner" && ctx.Role != "rider" && ctx.Role != "admin" {
		respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
	} else if c, err := db.GetClient(mux.Vars(r)["id"]); err == nil {
		if ctx.Role == "client" && c.ID != ctx.ID {
			respond(w, http.StatusUnauthorized, model.Exception{Message: http.StatusText(http.StatusUnauthorized)})
		} else {
			client_picture := c.Picture
			if client_picture == "" {
				client_picture = "default.jpg"
			}
			io.WriteString(w, fmt.Sprintf(`{"url": "https://%s/images/%s"}`, configuration.Configuration.HOST, client_picture))
		}
	} else {
		respond(w, http.StatusNotFound, model.Exception{Message: http.StatusText(http.StatusNotFound)})
	}
}

func deleteClientHandler(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	ctx := getAuthCtx(r)
	if (ctx.Role != "client" && ctx.Role != "admin") || (ctx.Role == "client" && id != strconv.Itoa(ctx.ID)) {
		respond(w, http.StatusUnauthorized, model.Exception{Message: "You are not authorized to access this ressource"})
	} else if db.DeleteClient(id) /* err */ != nil {
		w.WriteHeader(http.StatusNotFound)
	}
}

func patchClient(b []byte, id string, role string, user_id int) int {
	if role != "client" && role != "admin" {
		return 403
	}
	c, err := db.GetClient(id)
	if err != nil {
		return 404
	}
	clientMap := make(map[string]interface{})
	if err := json.Unmarshal([]byte(b), &clientMap); err != nil {
		return 500
	}
	for key, value := range clientMap {
		switch key {
		case "firstname":
			if IsName(value.(string)) == false {
				fmt.Errorf("patchClient : firstname not valid")
				return 500
			}
			c.FirstName = value.(string)
		case "lastname":
			if IsName(value.(string)) == false {
				fmt.Errorf("patchClient : lastname not valid")
				return 500
			}
			c.LastName = value.(string)
		case "street":
			c.Street = value.(string)
		case "zipcode":
			c.ZipCode = value.(string)
		case "city":
			c.City = value.(string)
		case "additionaladdress":
			c.AdditionalAddress = value.(string)
		case "email":
			if IsEmail(value.(string)) == false {
				fmt.Errorf("patchClient : email not valid")
				return 500
			}
			c.Email = value.(string)
		case "phonenumber":
			if IsPhoneNumber(value.(string)) == false {
				fmt.Errorf("patchClient : phone number not valid")
				return 500
			}
			c.PhoneNumber = value.(string)
		case "password":
			c.Password = value.(string)
			cfg := argon2.DefaultConfig()
			raw, err := cfg.Hash([]byte(c.Password), nil)
			if err != nil {
				fmt.Printf("Error during hashing: %s\n", err.Error())
				return 500
			}
			c.Password = string(raw.Encode())
		case "deadline":
			c.Deadline = value.(string)
		}
	}
	db.UpdateAddressClient(c.Street, c.ZipCode, c.City, c.AdditionalAddress, id)
	db.UpdateClient(c.FirstName, c.LastName, c.Email, c.PhoneNumber, c.Password, c.Deadline, id)
	return 202
}

func forgotPasswordClient(T interface{}) int {
	fp := (reflect.ValueOf(T)).Interface().(*model.ForgotPassword)
	if exist, err := db.CheckClientExistingAccount(fp.Email); err != nil {
		fmt.Errorf("ForgotPasswordClient : %s\n", err.Error())
		return 500
	} else {
		if exist == false {
			fmt.Errorf("ForgotPasswordClient : Account doesn't exists")
			return 404
		}
	}
	token, err := generateForgotPasswordToken(fp)
	if err != nil {
		fmt.Errorf("ForgotPasswordClient : Error creating token")
		return 500
	}
	if err := mailer.SendForgotPasswordEmail(token, fp.Email, "clients"); err != nil {
		fmt.Errorf("ForgotPasswordClient : Couldn't send email")
		return 500
	}
	return 202
}

func updatePasswordClient(T interface{}, tokenString string) int {
	up := (reflect.ValueOf(T)).Interface().(*model.UpdatePassword)
	token, err := jwt.ParseWithClaims(tokenString, &model.ForgotPasswordClaims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})
	if err != nil {
		fmt.Errorf(err.Error())
		return 500
	}
	if claims, ok := token.Claims.(*model.ForgotPasswordClaims); ok && token.Valid {
		if e, err := db.CheckClientExistingAccount(claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		} else if e != true {
			fmt.Errorf("updatePasswordClient : Account does not exist")
			return 404
		}
		cfg := argon2.DefaultConfig()
		raw, err := cfg.Hash([]byte(up.NewPassword), nil)
		if err != nil {
			fmt.Errorf("updatePasswordClient : could not hash password : %s", err.Error())
			return 500
		}
		if err := db.UpdatePasswordClient(string(raw.Encode()), claims.Email); err != nil {
			fmt.Errorf(err.Error())
			return 500
		}
	} else {
		fmt.Errorf(err.Error())
		return 500
	}
	return 202
}

func createClient(T interface{}) (int64, error) {
	c := (reflect.ValueOf(T)).Interface().(*model.Client)
	cfg := argon2.DefaultConfig()
	raw, err := cfg.Hash([]byte(c.Password), nil)
	if err != nil {
		return 0, fmt.Errorf("createClient : could not hash password : %s", err.Error())
	}
	if IsName(c.FirstName) && IsName(c.LastName) && IsEmail(c.Email) && IsPhoneNumber(c.PhoneNumber) {

		/*******CODE FOR VALIDATION ACCOUNT***********/
		if configuration.Configuration.EMAIL_validation == true {
			token, err := generateCreateToken(c, string(raw.Encode()))
			if err != nil {
				return 0, fmt.Errorf("createClient : Error creating token")
			}
			if err := mailer.SendVerificationEmail(token, c.Email, "clients"); err != nil {
				return 0, fmt.Errorf("createClient : Couldn't send email")
			}
			return 0, nil
		}

		/*******CODE FOR NO VALIDATION ACCOUNT***********/
		fmt.Errorf("Create Without Validation")
		id, err := db.CreateAddress(c.Street, c.ZipCode, c.City, c.AdditionalAddress)
		if err != nil {
			return 0, nil
		}
		return db.CreateClient(c.FirstName, c.LastName, id, c.Email, c.PhoneNumber, string(raw.Encode()), c.Deadline)
	}
	return 0, fmt.Errorf("createClient : wrong syntax")
}
