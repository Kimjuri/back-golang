package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestDeliveriesManagerAuth(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idApplication": 1,
		"password": "cp1"
	}`) /* /!\ idApplication is an int */
	req, _ := http.NewRequest("POST", "/auth/application", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authApplication, &model.AuthApplication{}, validAuthApplicationKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var app model.Account
	if err := json.Unmarshal([]byte(rr.Body.String()), &app); err != nil {
		fmt.Println(rr.Body.String())
	}
	test.MyAssert(t, "account id", 1, app.ID)
	test.MyAssert(t, "application id", 1, app.IDApplication)
	test.MyAssert(t, "application role", "application_role", app.Role)
	test.MyAssert(t, "account argon hash", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I", app.Password)
}

func TestDeliveriesManagerAuthWithBadPassword(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"idApplication": 1,
		"password": "bad_password"
	}`) /* /!\ idApplication is an int */
	req, _ := http.NewRequest("POST", "/auth/application", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authApplication, &model.AuthApplication{}, validAuthApplicationKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusUnauthorized, rr.Code)
}

func TestClientAuth(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"email": "test_auth_client@client.fr",
		"password": "cp1"
	}`)
	req, _ := http.NewRequest("POST", "/auth/client", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authClient, &model.Auth{}, validAuthKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var client model.Client
	if err := json.Unmarshal([]byte(rr.Body.String()), &client); err != nil {
		fmt.Println(rr.Body.String())
	}
	test.MyAssert(t, "client id", 2, client.ID)
	test.MyAssert(t, "client email", "test_auth_client@client.fr", client.Email)
}

func TestPartnerAuth(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"email": "test_auth_partner@partner.fr",
		"password": "pp1"
	}`)
	req, _ := http.NewRequest("POST", "/auth/partner", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authPartner, &model.Auth{}, validAuthKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var partner model.Partner
	if err := json.Unmarshal([]byte(rr.Body.String()), &partner); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "partner id", 2, partner.ID)
	test.MyAssert(t, "partner email", "test_auth_partner@partner.fr", partner.Email)
}

func TestRiderAuth(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"email": "test_auth_rider@rider.fr",
		"password": "rp1"
	}`)
	req, _ := http.NewRequest("POST", "/auth/rider", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authRider, &model.Auth{}, validAuthKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var rider model.Rider
	if err := json.Unmarshal([]byte(rr.Body.String()), &rider); err != nil {
		fmt.Println(rr.Body.String())
	}
	test.MyAssert(t, "rider id", 2, rider.ID)
	test.MyAssert(t, "rider email", "test_auth_rider@rider.fr", rider.Email)
}

func TestAdminAuth(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"email": "contact.groomshop@gmail.com",
		"password": "admin"
	}`)
	req, _ := http.NewRequest("POST", "/auth/admin", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createAuthHandler(authAdmin, &model.Auth{}, validAuthKeys))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var admin model.Admin
	if err := json.Unmarshal([]byte(rr.Body.String()), &admin); err != nil {
		fmt.Println(rr.Body.String())
	}
	test.MyAssert(t, "admin id", 1, admin.ID)
	test.MyAssert(t, "admint email", "contact.groomshop@gmail.com", admin.Email)
}
