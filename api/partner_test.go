package api

import (
	"back/model"
	"back/test"
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

/*func TestPartner(t *testing.T) {
	test.ReloadDB(t)
	payload := []byte(`{
		"name": "Name",
		"street": "42 rue du test",
		"zipcode": "77777",
		"city": "test_city",
		"additionaladdress": "test_additionaladdress",
		"email": "test@test.com",
		"phonenumber": "0601020304",
		"password": "test_pw"
	}`)
	req, _ := http.NewRequest("POST", "/partners", bytes.NewBuffer(payload))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(createHandler(createPartner, &model.Partner{}, partnerValidKeys, "any"))
	handler.ServeHTTP(rr, req)
	test.CheckResponseCode(t, http.StatusCreated, rr.Code)
}*/

func PartnerTestPatch(t *testing.T, payload []byte, expecting int) {
	test.ReloadDB(t)
	req, err := http.NewRequest("PATCH", "/partners/1", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1",
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "partner"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(patchHandler(patchPartner, &model.Partner{}, partnerValidKeys, AuthRequired))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, expecting, rr.Code)
}

func TestPartnerFetchAsAdmin(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/partners/1", nil) /* Test route */
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{
		"id": "1", /* Use mux vars to satisfy handler */
	})
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil { /* authenticate request */
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getPartnerHandler)
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims))) /* Simulate http service with request context */

	test.CheckResponseCode(t, http.StatusOK, rr.Code) /* Test response status */

	var partner model.Partner
	if err := json.Unmarshal([]byte(rr.Body.String()), &partner); err != nil { /* Retrieve json data from response */
		t.Fatal(err)
	}
	test.MyAssert(t, "partner id", 1, partner.ID)
	test.MyAssert(t, "partner name", "pn1", partner.Name)
}

func TestPartnerPatch(t *testing.T) {
	payload := []byte(`{
		"name": "name2",
		"email": "email@test.fr",
		"phonenumber": "+33606060606",
		"password": "pwd_test"
	}`)
	PartnerTestPatch(t, payload, http.StatusAccepted)
}

func TestPartnerPatchName(t *testing.T) {
	payload := []byte(`{
		"name": "name2"
	}`)
	PartnerTestPatch(t, payload, http.StatusAccepted)
}

func TestPartnerPatchWrongName(t *testing.T) {
	payload := []byte(`{
		"name": "@name2"
	}`)
	PartnerTestPatch(t, payload, http.StatusInternalServerError)
}

func TestPartnerPatchEmail(t *testing.T) {
	payload := []byte(`{
		"email": "thisis.atest@test.com"
	}`)
	PartnerTestPatch(t, payload, http.StatusAccepted)
}

func TestPartnerPatchWrongEmail(t *testing.T) {
	payload := []byte(`{
		"email": "wrong@email@gmail.com"
	}`)
	PartnerTestPatch(t, payload, http.StatusInternalServerError)
}

func TestPartnerPatchPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "+33601020304"
	}`)
	PartnerTestPatch(t, payload, http.StatusAccepted)
}

func TestPartnerPatchWrongPhoneNumber(t *testing.T) {
	payload := []byte(`{
		"phonenumber": "not a phone number"
	}`)
	PartnerTestPatch(t, payload, http.StatusInternalServerError)
}
