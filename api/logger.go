package api

import (
	"log"
	"net/http"
	"time"
)

// Logger prints http requests for logging/debugging purposes.
func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		log.Printf("%s\t%s\t%s\t%s", r.Method, r.RequestURI, name, time.Since(start))
		inner.ServeHTTP(w, r)
	})
}
