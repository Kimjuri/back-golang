package api

import (
	"back/model"
)

// Valid keys on json body payloads
var clientValidKeys = []string{"firstname", "lastname", "street", "zipcode", "city", "additionaladdress", "email", "phonenumber", "password", "deadline"}
var partnerValidKeys = []string{"name", "street", "zipcode", "city", "additionaladdress", "email", "phonenumber", "password", "logo", "siret"}
var riderValidKeys = []string{"firstname", "lastname", "email", "phonenumber", "rating", "password", "latitude", "longitude", "free", "siret", "iban"}
var validAuthKeys = []string{"email", "password"}
var validAuthApplicationKeys = []string{"idApplication", "password"}
var validDeliveryQueriesKeys = []string{"r", "p", "c", "pu"}
var purchaseValidKeys = []string{"idClient", "idPartner", "date"}
var editPurchasesValidKeys = []string{"status"}
var deliveriesValidKeys = []string{"idPurchases", "idRider", "status", "date"}
var validPurchaseQueriesKeys = []string{"p", "c"}
var validUsersQueriesKeys = []string{"t"}
var forgotPasswordKeys = []string{"email"}
var updatePasswordKeys = []string{"newpassword"}
var adminValidKeys = []string{"t"}

const AuthRequired = true
const NoAuth = !AuthRequired

var routes = []model.Route{
	{"Ping route", "GET", "/ping", NoAuth, pingHandler},
	{"Get users", "GET", "/users", AuthRequired, getUsersHandler(validUsersQueriesKeys)},
	{"Get client infos", "GET", "/clients/{id:[0-9]+}", AuthRequired, getClientHandler},
	{"Get client images", "GET", "/clients/{id:[0-9]+}/picture", AuthRequired, getClientPictureHandler},
	{"Get valid account", "GET", "/adminvalid", AuthRequired, getValidAccount(adminValidKeys)},
	{"Get non valid account", "GET", "/nonadminvalid", AuthRequired, getNonValidAccount(adminValidKeys)},
	{"Set client new images", "POST", "/clients/{id:[0-9]+}/picture", AuthRequired, setClientNewPictureHandler},
	{"Delete client", "DELETE", "/clients/{id:[0-9]+}", AuthRequired, deleteClientHandler},
	{"Create rider", "POST", "/riders", NoAuth, createHandler(createRider, &model.Rider{}, riderValidKeys, "any")},
	{"Create partner", "POST", "/partners", NoAuth, createHandler(createPartner, &model.Partner{}, partnerValidKeys, "any")},
	{"Create client", "POST", "/clients", NoAuth, createHandler(createClient, &model.Client{}, clientValidKeys, "any")},
	{"Valid client", "POST", "/valid/client/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, validAccountHandler(validCreateToken, "client")},
	{"Valid partner", "POST", "/valid/partner/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, validAccountHandler(validCreateToken, "partner")},
	{"Valid rider", "POST", "/valid/rider/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, validAccountHandler(validCreateToken, "rider")},
	{"Valid partner admin", "POST", "/adminvalid/partners/{id:[0-9]+}", AuthRequired, adminValidPartner},
	{"Valid rider admin", "POST", "/adminvalid/riders/{id:[0-9]+}", AuthRequired, adminValidRider},
	{"Update client", "PATCH", "/clients/{id:[0-9]+}", AuthRequired, patchHandler(patchClient, &model.Client{}, clientValidKeys, AuthRequired)},
	{"Update partner", "PATCH", "/partners/{id:[0-9]+}", AuthRequired, patchHandler(patchPartner, &model.Partner{}, partnerValidKeys, AuthRequired)},
	{"Get partner stats", "GET", "/partners/{id:[0-9]+}/stats", AuthRequired, getPartnerStatsHandler},
	{"Get partner stats about clients", "GET", "/partners/{id:[0-9]+}/statsclient", AuthRequired, getPartnerStatsClient},
	{"Get partner stats about clients admin", "GET", "/admin/partners/stats", AuthRequired, getPartnerStatsClientAdmin},
	{"Get rider stats admin", "GET", "/admin/riders/stats", AuthRequired, getRiderStatsAdmin},
	{"Get client delivery stats", "GET", "/clients/{id:[0-9]+}/stats", AuthRequired, getClientDeliveryStats},
	{"Update rider", "PATCH", "/riders/{id:[0-9]+}", AuthRequired, patchHandler(patchRider, &model.Rider{}, riderValidKeys, AuthRequired)},
	{"Password forgot client", "POST", "/clients/password", NoAuth, forgotPasswordHandler(forgotPasswordClient, &model.ForgotPassword{}, forgotPasswordKeys, "any")},
	{"Password update client", "POST", "/clients/password/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, updatePasswordHandler(updatePasswordClient, &model.UpdatePassword{}, updatePasswordKeys, "any")},
	{"Password forgot partner", "POST", "/partners/password", NoAuth, forgotPasswordHandler(forgotPasswordPartner, &model.ForgotPassword{}, forgotPasswordKeys, "any")},
	{"Password update partner", "POST", "/partners/password/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, updatePasswordHandler(updatePasswordPartner, &model.UpdatePassword{}, updatePasswordKeys, "any")},
	{"Password forgot rider", "POST", "/riders/password", NoAuth, forgotPasswordHandler(forgotPasswordRider, &model.ForgotPassword{}, forgotPasswordKeys, "any")},
	{"Password update rider", "POST", "/riders/password/{token:[A-Za-z0-9\\-\\._~\\+\\/]+}", NoAuth, updatePasswordHandler(updatePasswordRider, &model.UpdatePassword{}, updatePasswordKeys, "any")},
	{"Client authentification", "POST", "/auth/client", NoAuth, createAuthHandler(authClient, &model.Auth{}, validAuthKeys)},
	{"Partner authentification", "POST", "/auth/partner", NoAuth, createAuthHandler(authPartner, &model.Auth{}, validAuthKeys)},
	{"Rider authentification", "POST", "/auth/rider", NoAuth, createAuthHandler(authRider, &model.Auth{}, validAuthKeys)},
	{"Admin authentification", "POST", "/auth/admin", NoAuth, createAuthHandler(authAdmin, &model.Auth{}, validAuthKeys)},
	{"Application authentification", "POST", "/auth/application", NoAuth, createAuthHandler(authApplication, &model.Auth{}, validAuthKeys)},
	{"Get free riders", "GET", "/riders/free", NoAuth, getFreeRiders},
	{"Get rider bills", "GET", "/riders/bills/{id:[0-9]+}", AuthRequired, getRiderBill},
	{"Get partner infos", "GET", "/partners/{id:[0-9]+}", AuthRequired, getPartnerHandler},
	{"Delete partner", "DELETE", "/partners/{id:[0-9]+}", AuthRequired, deletePartnerHandler},
	{"Get rider infos", "GET", "/riders/{id:[0-9]+}", AuthRequired, getRiderHandler},
	{"Delete rider", "DELETE", "/riders/{id:[0-9]+}", AuthRequired, deleteRiderHandler},
	{"Create delivery", "POST", "/deliveries", AuthRequired, createHandler(createDelivery, &model.Delivery{}, deliveriesValidKeys, "deliveries_manager")},
	{"Update delivery", "PATCH", "/deliveries/{id:[0-9]+}", AuthRequired, patchHandler(patchDelivery, &model.Delivery{}, deliveriesValidKeys, NoAuth)},
	{"Validate delivery", "PATCH", "/deliveries/validate/{id:[0-9]+}", AuthRequired, validateDelivery},
	{"Get addresses of delivery", "GET", "/deliveries/addresses/{id:[0-9]+}", AuthRequired, getAddressesOfDelivery},
	{"Get delivery by id", "GET", "/deliveries/{id:[0-9]+}", AuthRequired, getDeliveryByIDHandler},
	{"Get all delivery(es)", "GET", "/deliveries", AuthRequired, getDeliveryHandler(getDelivery, "all", validDeliveryQueriesKeys)},
	{"Get placed delivery(es)", "GET", "/deliveries/placed", AuthRequired, getDeliveryHandler(getDelivery, "delivery_placed", validDeliveryQueriesKeys)},
	{"Get pending delivery(es)", "GET", "/deliveries/pending", AuthRequired, getDeliveryHandler(getDelivery, "delivery_pending", validDeliveryQueriesKeys)},
	{"Get resolved delivery(es)", "GET", "/deliveries/resolved", AuthRequired, getDeliveryHandler(getDelivery, "delivery_resolved", validDeliveryQueriesKeys)},
	{"Create Purchase", "POST", "/purchases", AuthRequired, createHandler(createPurchase, &model.Purchase{}, purchaseValidKeys, "partner")},
	{"Upload and link receipt to a purchase", "POST", "/purchases/{id:[0-9]+}/receipt", AuthRequired, setPurchaseReceiptHandler},
	{"Update purchase", "PATCH", "/purchases/{id:[0-9]+}", AuthRequired, patchHandler(patchPurchase, &model.Purchase{}, editPurchasesValidKeys, NoAuth)},
	{"Get all purchases", "GET", "/purchases", AuthRequired, getPurchaseHandler(getPurchase, "all", validPurchaseQueriesKeys)},
	{"Get all info purchases", "GET", "/purchases/info/{id:[0-9]+}", AuthRequired, getPurchaseInfoHandler()},
	{"Get purchase by id", "GET", "/purchases/{id:[0-9]+}", AuthRequired, getPurchaseByIDHandler},
	{"Get placed purchase(s)", "GET", "/purchases/placed", AuthRequired, getPurchaseHandler(getPurchase, "purchase_placed", validPurchaseQueriesKeys)},
	{"Get pending purchase(s)", "GET", "/purchases/pending", AuthRequired, getPurchaseHandler(getPurchase, "purchase_pending", validPurchaseQueriesKeys)},
	{"Get resolved purchase(s)", "GET", "/purchases/resolved", AuthRequired, getPurchaseHandler(getPurchase, "purchase_resolved", validPurchaseQueriesKeys)},
}
