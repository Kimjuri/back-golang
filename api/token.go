package api

import (
	"back/configuration"
	"back/db"
	"back/model"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"reflect"
	"time"
)

func generateCreateToken(T interface{}, password string) (string, error) {
	var token *jwt.Token
	switch T := T.(type) {
	case *model.Client:
		c := (reflect.ValueOf(T)).Interface().(*model.Client)
		token = jwt.NewWithClaims(jwt.GetSigningMethod(configuration.Configuration.JWT_Method), &model.ClientClaims{
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * configuration.Configuration.JWT_Lifetime).Unix(),
				Issuer:    "GroomShop",
			},
			c.FirstName,
			c.LastName,
			c.Street,
			c.ZipCode,
			c.City,
			c.AdditionalAddress,
			c.Email,
			c.PhoneNumber,
			password,
			c.Deadline,
		})
	case *model.Partner:
		p := (reflect.ValueOf(T)).Interface().(*model.Partner)
		token = jwt.NewWithClaims(jwt.GetSigningMethod(configuration.Configuration.JWT_Method), &model.PartnerClaims{
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * configuration.Configuration.JWT_Lifetime).Unix(),
				Issuer:    "GroomShop",
			},
			p.Name,
			p.Street,
			p.ZipCode,
			p.City,
			p.AdditionalAddress,
			p.Email,
			p.PhoneNumber,
			password,
			p.Siret,
		})
	case *model.Rider:
		r := (reflect.ValueOf(T)).Interface().(*model.Rider)
		token = jwt.NewWithClaims(jwt.GetSigningMethod(configuration.Configuration.JWT_Method), &model.RiderClaims{
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * configuration.Configuration.JWT_Lifetime).Unix(),
				Issuer:    "GroomShop",
			},
			r.FirstName,
			r.LastName,
			r.Email,
			r.PhoneNumber,
			password,
			r.Iban,
			r.Siret,
		})
	}
	tokenstring, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}
	return tokenstring, nil
}

func createNewAccount(T interface{}) int {
	switch T := T.(type) {
	case *model.ClientClaims:
		a := (reflect.ValueOf(T)).Interface().(*model.ClientClaims)
		if e, err := db.CheckClientExistingAccount(a.Email); err != nil {
			fmt.Println(err.Error())
			return 500
		} else {
			if e != false {
				fmt.Println("validClientHandler: Account already exists")
				return 409
			} else {
				id, err := db.CreateAddress(a.Street, a.ZipCode, a.City, a.AdditionalAddress)
				if err != nil {
					return 500
				} else {
					if _, err := db.CreateClient(a.FirstName, a.LastName, id, a.Email, a.PhoneNumber, a.Password, a.Deadline); err != nil {
						return 500
					}
				}
			}
		}
	case *model.PartnerClaims:
		a := (reflect.ValueOf(T)).Interface().(*model.PartnerClaims)
		if e, err := db.CheckPartnerExistingAccount(a.Email); err != nil {
			fmt.Println(err.Error())
			return 500
		} else {
			if e != false {
				fmt.Println("validPartnerHandler: Account already exists")
				return 409
			} else {
				id, err := db.CreateAddress(a.Street, a.ZipCode, a.City, a.AdditionalAddress)
				if err != nil {
					return 500
				} else {
					if _, err := db.CreatePartner(a.Name, id, a.Email, a.PhoneNumber, a.Password, "https://groomshop.fr/img/groomshop-alpha-inverted.png", a.Siret); err != nil {
						return 500
					}
				}
			}
		}
	case *model.RiderClaims:
		a := (reflect.ValueOf(T)).Interface().(*model.RiderClaims)
		if e, err := db.CheckRiderExistingAccount(a.Email); err != nil {
			fmt.Println(err.Error())
			return 500
		} else {
			if e != false {
				fmt.Println("validRiderHandler: Account already exists")
				return 409
			} else {
				if _, err := db.CreateRider(a.FirstName, a.LastName, a.Email, a.PhoneNumber, 0, a.Password, a.Iban, a.Siret); err != nil {
					return 500
				}
			}
		}
	}
	return 201
}

func validCreateToken(ct string, tokenString string) int {
	var token *jwt.Token
	var claims interface{}
	var err error
	var ok bool
	switch ct {
	case "client":
		token, err = jwt.ParseWithClaims(tokenString, &model.ClientClaims{}, func(token *jwt.Token) (interface{}, error) {
			return jwtSecret, nil
		})
		claims, ok = token.Claims.(*model.ClientClaims)
	case "partner":
		token, err = jwt.ParseWithClaims(tokenString, &model.PartnerClaims{}, func(token *jwt.Token) (interface{}, error) {
			return jwtSecret, nil
		})
		claims, ok = token.Claims.(*model.PartnerClaims)
	case "rider":
		token, err = jwt.ParseWithClaims(tokenString, &model.RiderClaims{}, func(token *jwt.Token) (interface{}, error) {
			return jwtSecret, nil
		})
		claims, ok = token.Claims.(*model.RiderClaims)
	}
	if err != nil {
		fmt.Errorf("validCreateToken : Can't parse token")
		return 500
	} else if !ok || !token.Valid {
		fmt.Errorf("validCreateToken : Wrong token")
		return 500
	}
	return (createNewAccount(claims))
}

func generateForgotPasswordToken(fp *model.ForgotPassword) (string, error) {
	token := jwt.NewWithClaims(jwt.GetSigningMethod(configuration.Configuration.JWT_Method), &model.ForgotPasswordClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * configuration.Configuration.JWT_Lifetime).Unix(),
			Issuer:    "GroomShop",
		},
		fp.Email,
	})
	tokenstring, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}
	fmt.Println(tokenstring)
	return tokenstring, nil
}
