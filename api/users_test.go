package api

import (
	"back/model"
	"back/test"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAllUsersFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/users", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getUsersHandler(validUsersQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 20, len(ds))
}

func TestAllValidAccountFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/adminvalid", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 12, len(ds))
}

func TestAllValidAccountRiderFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/adminvalid?t=riders", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 8, len(ds))
}

func TestAllValidAccountPartnerFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/adminvalid?t=partners", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 4, len(ds))
}

func TestAllNonValidAccountFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/nonadminvalid", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getNonValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 2, len(ds))
}

func TestAllNonValidAccountRiderFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/nonadminvalid?t=riders", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getNonValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 1, len(ds))
}

func TestAllNonValidAccountPartnerFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/nonadminvalid?t=partners", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getNonValidAccount(adminValidKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 1, len(ds))
}

func TestAllUsersClientsFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/users?t=clients", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getUsersHandler(validUsersQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 6, len(ds))
}

func TestAllUsersPartnersFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/users?t=partners", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getUsersHandler(validUsersQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 5, len(ds))
}

func TestAllUsersRidersFetch(t *testing.T) {
	test.ReloadDB(t)
	req, err := http.NewRequest("GET", "/users?t=riders", nil) /* Get all deliveries */
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	if token, err := generateToken(1, "admin"); err != nil {
		t.Fatal(err)
	} else {
		req.Header.Set("Authorization", "Bearer "+token)
	}
	handler := http.HandlerFunc(getUsersHandler(validUsersQueriesKeys))
	claims, _ := parseJwt(nil, req)
	handler.ServeHTTP(rr, req.WithContext(generateAuthCtx(req, claims)))
	test.CheckResponseCode(t, http.StatusOK, rr.Code)
	var ds []model.User
	if err := json.Unmarshal([]byte(rr.Body.String()), &ds); err != nil {
		t.Fatal(err)
	}
	test.MyAssert(t, "All users amount", 9, len(ds))
}
