# Backend Groomshop

Rest API in Golang

## Features
* SSLv2/TLS
* gorilla mux http router
* mariadb integration
* curl-like tests
* mariadb/certificates tools

## Getting started

You must install golang and mariadb at least ofc. You also must
generate server certificate and key using generate.sh (take care of common name.
CN=localhost for example). Retrieve also certificate (server.crt) to
run curl tests (--cacert option or use -k). You might be able to retrieve where
it belong on auth/server.crt or remotely by using :
```
$ openssl s_client -connect HOST:443 -showcerts
```

### Dockerize

Start docker deamon and use the compose file.
```
$ docker-compose up
```

### Local

You can start the service without docker.
To do so, add `api` and `db` as local hosts via `/etc/hosts`. Then, start
the `local.sh` script. It will init GroomShop api and start logging in files.
```
# local.sh && tail -f *.dump
```
Be sure to stop background process when needed:
```
# pkill back && pkill mariadb
```
