DROP DATABASE IF EXISTS groomshop;

CREATE DATABASE groomshop DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE groomshop;

CREATE TABLE address
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  street VARCHAR(38) NOT NULL,
  zipcode VARCHAR(5) NOT NULL,
  city VARCHAR(32) NOT NULL,
  additionaladdress VARCHAR(38)
) ENGINE=InnoDB;

CREATE TABLE clients
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(40) NOT NULL,
  lastname VARCHAR(40) NOT NULL,
  fk_address INT NOT NULL,
  email VARCHAR(255) NOT NULL,
  phonenumber VARCHAR(15) NOT NULL,
  password VARCHAR(100) NOT NULL,
  deadline TIME NOT NULL,
  picture VARCHAR(50)
) ENGINE = InnoDB;

CREATE TABLE partners
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(40) NOT NULL,
  fk_address INT NOT NULL,
  email VARCHAR(255) NOT NULL,
  phonenumber VARCHAR(15) NOT NULL,
  password VARCHAR(100) NOT NULL,
  logo VARCHAR(100) NOT NULL,
  valid BOOLEAN DEFAULT 0,
  siret VARCHAR(14) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE riders
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(40) NOT NULL,
  lastname VARCHAR(40) NOT NULL,
  email VARCHAR(255) NOT NULL,
  phonenumber VARCHAR(15) NOT NULL,
  rating INT NOT NULL,
  password VARCHAR(100),
  iban VARCHAR(34),
  longitude FLOAT,
  latitude FLOAT,
  free    BOOLEAN DEFAULT 0,
  valid BOOLEAN DEFAULT 0,
  siret VARCHAR(14) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE statuses
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  description VARCHAR(255) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE purchases
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fk_delivery INT DEFAULT 0,
  fk_client INT,
  fk_partner INT,
  fk_status INT,
  date TIMESTAMP
) ENGINE = InnoDB;

CREATE TABLE deliveries
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fk_rider INT,
  fk_status INT,
  date TIMESTAMP
) ENGINE = InnoDB;

CREATE TABLE bills
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fk_rider INT,
  fk_client INT,
  fk_delivery INT,
  amount INT DEFAULT 5,
  date TIMESTAMP,
  time INT
) ENGINE = InnoDB;

CREATE TABLE receipts
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  receipt VARCHAR(255),
  receiptWatermarked VARCHAR(255),  
  fk_purchase INT
) ENGINE = InnoDB;

CREATE TABLE deliveries_logs
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  fk_delivery INT,
  date TIMESTAMP,
  description VARCHAR(255) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE accounts
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  application_id INT,
  role VARCHAR(255) NOT NULL,
  password VARCHAR(100) NOT NULL
) ENGINE = InnoDB;



CREATE TABLE admin
(
  id TINYINT PRIMARY KEY NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(100) NOT NULL
) ENGINE = InnoDB;

DELIMITER //
CREATE TRIGGER limitTable
BEFORE INSERT
ON admin
FOR EACH ROW
BEGIN
  SELECT COUNT(*) INTO @cnt FROM admin;
  IF @cnt >= 1 THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Admin account already set'; -- raise an error
  END IF;
END;//
DELIMITER ;
