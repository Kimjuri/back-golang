USE groomshop;

INSERT INTO address(id, street, zipcode, city) VALUES (1, "16 rue d'Orléans", "44000", "Nantes");
INSERT INTO address VALUES (2, "9 rue Linné", "44100", "Nantes", "Appartement 36");
INSERT INTO address VALUES (3, "6 rue d'Alger", "44000", "Nantes", null);
INSERT INTO address VALUES (4, "17 Boulevard Vincent Gâche", "44200", "Nantes", "Appartement 76");
INSERT INTO address VALUES (5, "24 rue Dr Brindeau", "44000", "Nantes", null);
INSERT INTO address VALUES (6, "1 rue de Budapest", "44000", "Nantes", null);
INSERT INTO address VALUES (7, "6 rue de Feltre", "44000", "Nantes", null);
INSERT INTO address VALUES (8, "3 rue du Calvaire", "44000", "Nantes", null);
INSERT INTO address VALUES (9, "10 rue Boileau", "44000", "Nantes", null);
INSERT INTO address VALUES (10, "11 rue Paré", "44000", "Nantes", null);

INSERT INTO clients VALUES (1, "Gaetan", "Laux", 1, "ce1@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","17:20:12", null);
INSERT INTO clients VALUES (2, "Bruce", "Frappier", 2, "test_auth_client@client.fr", "0606060607", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","18:21:54", null);
INSERT INTO clients VALUES (3, "Caroline", "Perrier", 3, "ce3@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","17:17:54", null);
INSERT INTO clients VALUES (4, "Noémie", "Souplet", 4, "ce4@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","20:13:54", null);
INSERT INTO clients VALUES (5, "Michel", "Marquis", 5, "ce5@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","22:01:54", null);

INSERT INTO riders VALUES (1, "Patrick", "Du Pruvost", "re1@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, true, true, '01234567890123');
INSERT INTO riders VALUES (2, "Océane", "Delorme", "test_auth_rider@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (3, "Alain", "Chevalier", "re3@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (4, "Juliette", "Nicolas", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (5, "Lucie", "Weiss", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true,'01234567890123');
INSERT INTO riders VALUES (6, "Laurent", "Mace", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (7, "Aurélie", "Dumas", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (8, "Thibaud", "Lemasson", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (9, "Alix", "Leger", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, false, '01234567890123');

INSERT INTO partners VALUES (1, "H&M", 6, "pe1@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/HM.png", true, '01234567890123');
INSERT INTO partners VALUES (2, "Undiz", 7, "test_auth_partner@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/undiz.png", true, '01234567890123');
INSERT INTO partners VALUES (3, "Naf Naf", 8, "pe3@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/nafnaf.png", true, '01234567890123');
INSERT INTO partners VALUES (4, "Jules", 9, "test_auth_partner@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/jules.png", true, '01234567890123');
INSERT INTO partners VALUES (5, "ROCK A GOGO", 10, "pe5@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/rag.png", false, '01234567890123');

INSERT INTO statuses VALUES (1, "delivery_placed");
INSERT INTO statuses VALUES (2, "delivery_pending");
INSERT INTO statuses VALUES (3, "delivery_resolved");
INSERT INTO statuses VALUES (4, "purchase_placed");
INSERT INTO statuses VALUES (5, "purchase_pending");
INSERT INTO statuses VALUES (6, "purchase_resolved");

INSERT INTO accounts VALUES (1, 1, "application_role", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I");

INSERT INTO admin VALUES (1, "contact.groomshop@gmail.com", "$argon2i$v=19$m=4096,t=3,p=1$FtpbcataDFCTY44drrTjaQ$3dSRP53OkCuLPJg0GvwUAJZ3V00eSmEmkGg5pbQTljg");

INSERT INTO bills VALUES (1,1,1,1,5,"2019-09-21 18:45:15",650);
INSERT INTO bills VALUES (2,1,1,3,5,"2019-10-21 17:45:15",1020);
INSERT INTO bills VALUES (3,2,1,2,5,"2019-09-22 19:30:15",780);