package db

import (
	"back/model"
	"back/test"
	"testing"
)

func TestCheckClientExistingAccount(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckClientExistingAccount("ce1@client.fr")
	if err != nil {
		t.Errorf("Check client existing account: %v", err)
	} else {
		if b == false {
			t.Errorf("Can create account client that already exists")
		}
	}
}

func TestCheckClientExistingAccount2(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckClientExistingAccount("ce12@client.fr")
	if err != nil {
		t.Errorf("Check client existing account: %v", err)
	} else {
		if b == true {
			t.Errorf("Cannot create account client that doesn't already exist")
		}
	}
}

func TestCheckPartnerExistingAccount(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckPartnerExistingAccount("pe1@partner.fr")
	if err != nil {
		t.Errorf("Check partner existing account: %v", err)
	} else {
		if b == false {
			t.Errorf("Can create account partner that already exists")
		}
	}
}

func TestCheckPartnerExistingAccount2(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckPartnerExistingAccount("pe12@partner.fr")
	if err != nil {
		t.Errorf("Check partner existing account: %v", err)
	} else {
		if b == true {
			t.Errorf("Cannot create account partner that doesn't already exist")
		}
	}
}

func TestCheckRiderExistingAccount(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckRiderExistingAccount("re1@rider.fr")
	if err != nil {
		t.Errorf("Check rider existing account: %v", err)
	} else {
		if b == false {
			t.Errorf("Can create account rider that already exist")
		}
	}
}

func TestCheckRiderExistingAccount2(t *testing.T) {
	test.ReloadDB(t)
	b, err := CheckRiderExistingAccount("re12@rider.fr")
	if err != nil {
		t.Errorf("Check rider existing account: %v", err)
	} else {
		if b == true {
			t.Errorf("Cannot create account rider that doesn't already exist")
		}
	}
}

func TestGetUsers(t *testing.T) {
	test.ReloadDB(t)
	u, err := GetUsers()
	if err != nil {
		t.Errorf("Cannot Get users: %v", err)
	}
	test.MyAssert(t, "All users amount", 20, len(u))
}

func TestGetClient(t *testing.T) {
	test.ReloadDB(t)
	if c, err := GetClient("3"); err != nil {
		t.Errorf("Cannot GetClient: %v", err)
	} else {
		expect := model.Client{ID: 3, FirstName: "TestGetClient", LastName: "cln3", Street: "6 rue d'Alger", ZipCode: "44000", City: "Nantes", AdditionalAddress: "", Email: "ce3@client.fr", PhoneNumber: "0606060606", Password: "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I"}
		test.ClientModel(t, expect, c)
	}
	if _, err := GetClient("-1"); err == nil {
		t.Errorf("Can get invalid GetClient: %v", err)
	}
	if _, err := GetClient("123456"); err == nil {
		t.Errorf("Can get invalid GetClient: %v", err)
	}
}

func TestGetAllClients(t *testing.T) {
	test.ReloadDB(t)
	c, err := GetAllClients()
	if err != nil {
		t.Errorf("Cannot GetAllClient: %v", err)
	}
	test.MyAssert(t, "All clients amount", 6, len(c))
}

func TestGetRider(t *testing.T) {
	test.ReloadDB(t)
	if got, err := GetRider("1"); err != nil {
		t.Errorf("Cannot GetRider: %v", err)
	} else {
		expect := model.Rider{ID: 1, FirstName: "rfn1", LastName: "rln1", Email: "re1@rider.fr", PhoneNumber: "0606060606", Rating: 0, Password: "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", Longitude: 0, Latitude: 0, Free: true}
		test.RiderModel(t, expect, got)
	}
	if _, err := GetRider("-1"); err == nil {
		t.Errorf("Can get invalid GetRider: %v", err)
	}
	if _, err := GetRider("123456"); err == nil {
		t.Errorf("Can get invalid GetRider: %v", err)
	}
}

func TestGetAllRiders(t *testing.T) {
	test.ReloadDB(t)
	r, err := GetAllRiders()
	if err != nil {
		t.Errorf("Cannot GetAllRider: %v", err)
	}
	test.MyAssert(t, "All rider amount", 9, len(r))
}

func TestGetFreeRiders(t *testing.T) {
	test.ReloadDB(t)
	r, err := GetFreeRiders()
	if err != nil {
		t.Errorf("Cannot GetFreeRider: %v", err)
	}
	test.MyAssert(t, "All free rider amount", 1, len(r))
}

func TestGetPartner(t *testing.T) {
	test.ReloadDB(t)
	if got, err := GetPartner("1"); err != nil {
		t.Errorf("Cannot GetPartner: %v", err)
	} else {
		expect := model.Partner{ID: 1, Name: "pn1", Street: "1 rue de Budapest", ZipCode: "44000", City: "Nantes", AdditionalAddress: "", Email: "pe1@partner.fr", PhoneNumber: "0606060606", Password: "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", Siret: "01234567890123"}
		test.PartnerModel(t, expect, got)
	}
	if _, err := GetPartner("-1"); err == nil {
		t.Errorf("Can get invalid GetPartner: %v", err)
	}
	if _, err := GetPartner("123456"); err == nil {
		t.Errorf("Can get invalid GetPartner: %v", err)
	}
}

func TestGetAllPartners(t *testing.T) {
	test.ReloadDB(t)
	r, err := GetAllPartners()
	if err != nil {
		t.Errorf("Cannot GetAllPartners: %v", err)
	}
	test.MyAssert(t, "All partner amount", 5, len(r))
}

func TestGetPurchases(t *testing.T) {
	test.ReloadDB(t)
	if ps, err := GetPurchases("SELECT * FROM purchases WHERE id = ?;", 1); err != nil {
		t.Errorf("Cannot GetPurchases: %v", err)
	} else {
		d := ps[0]
		expect := model.Purchase{ID: 1, IDDelivery: 1, IDClient: 1, IDPartner: 1, Status: 4, Date: "2019-01-07 10:20:47"}
		test.PurchaseModel(t, expect, d)
	}
	if _, err := GetPurchases("SELECT * FROM purchases WHERE email = ?;", -1); err == nil {
		t.Errorf("Can get invalid getPurchases: %v", err)
	}
}

func TestGetPurchasePartnerForClient(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetPurchasePartnerForClient(1, 1)
	if err != nil {
		t.Errorf("Cannot GetPurchasePartnerForClient : %v", err)
	} else {
		expect := model.Partner{ID: 1, Name: "pn1", Street: "1 rue de Budapest", ZipCode: "44000", City: "Nantes", AdditionalAddress: "", Email: "pe1@partner.fr", PhoneNumber: "0606060606", Password: "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", Siret: "01234567890123"}
		test.PartnerModel(t, expect, p)
	}
	if _, err := GetPurchasePartnerForClient(1, 154); err == nil {
		t.Errorf("Can get invalid GetPurchasePartner ForClient")
	}
}

func TestGetPurchasesOfDelivery(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetPurchasesOfDelivery(1)
	if err != nil {
		t.Errorf("Cannot GetPurchaseOfDelivery : %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 2, len(p))
	}
}

func TestGetDeliveries(t *testing.T) {
	test.ReloadDB(t)
	if ds, err := GetDeliveries("SELECT * FROM deliveries WHERE id = ?;", 1); err != nil {
		t.Errorf("Cannot GetDeliveries: %v", err)
	} else {
		d := ds[0]
		expect := model.Delivery{ID: 1, IDRider: 1, Status: 1, Date: "2019-01-07 13:20:47"}
		test.DeliveryModel(t, expect, d)
	}
}

func TestCreateAddress(t *testing.T) {
	test.ReloadDB(t)
	_, err := CreateAddress("9 rue test", 12345, "Nantes", "")
	if err != nil {
		t.Errorf("Cannot CreateAddress: %v", err)
	}
}
func TestCreateClient(t *testing.T) {
	test.ReloadDB(t)
	c := model.Client{ID: 1000, FirstName: "TestCreateClient", LastName: "cln2", Street: "12 avenue des poussins", ZipCode: "44200", City: "Nantes", AdditionalAddress: "Appartement 15", Email: "ce2@client.fr", PhoneNumber: "0606060607", Password: "cp2", Deadline: "13:42:37"}
	id, err := CreateAddress(c.Street, c.ZipCode, c.City, c.AdditionalAddress)
	if err != nil {
		t.Errorf("Cannot CreateClient: %v", err)
	}
	if _,err := CreateClient(c.FirstName, c.LastName, id, c.Email, c.PhoneNumber, c.Password, c.Deadline); err != nil {
		t.Errorf("Cannot CreateClient: %v", err)
	}
}

func TestCreateRider(t *testing.T) {
	test.ReloadDB(t)
	c := model.Rider{ID: 1000, FirstName: "TestCreateRider", LastName: "cln2", Email: "ce2@client.fr", PhoneNumber: "0606060607", Rating: 0, Password: "cp2", Iban: "FR7630006000011234567890189", Latitude: 0, Longitude: 0, Free: true, Siret: "01234567890123"}
	if _,err := CreateRider(c.FirstName, c.LastName, c.Email, c.PhoneNumber, c.Rating, c.Password, c.Iban, c.Siret); err != nil {
		t.Errorf("Cannot CreateRider: %v", err)
	}
}

func TestCreatePartner(t *testing.T) {
	test.ReloadDB(t)
	c := model.Partner{ID: 1000, Name: "TestCreatePartner", Street: "12 avenue des poussins", ZipCode: "44200", City: "Nantes", AdditionalAddress: "Appartement 15", Email: "ce2@client.fr", PhoneNumber: "0606060607", Password: "cp2", Logo: "https://api.groomshop.fr/images/default.jpg", Siret: "01234567890123"}
	id, err := CreateAddress(c.Street, c.ZipCode, c.City, c.AdditionalAddress)
	if err != nil {
		t.Errorf("Cannot CreatePartner: %v", err)
	}
	if _,err := CreatePartner(c.Name, id, c.Email, c.PhoneNumber, c.Password, c.Logo, c.Siret); err != nil {
		t.Errorf("Cannot CreateRider: %v", err)
	}
}

func TestCreateDelivery(t *testing.T) {
	test.ReloadDB(t)
	d := model.Delivery{ID: 1000, IDPurchases: []int{1}, IDRider: 1, Status: 1, Date: "2019-01-07 10:20:47"}
	if _, err := CreateDelivery(d.IDPurchases, d.IDRider, d.Status, d.Date); err != nil {
		t.Errorf("Cannot CreateDelivery: %v", err)
	}
}

func TestCreateDeliveryLog(t *testing.T) {
	test.ReloadDB(t)
	id, err := CreateDeliveryLog(1, "2019-01-07 10:20:47", "test")
	if err != nil {
		t.Errorf("Cannot CreateDeliveryLog: %v", err)
	}
	if id != 1 {
		t.Errorf("Cannot CreateDeliveryLog: %v", err)
	}
}
func TestCreatePurchase(t *testing.T) {
	test.ReloadDB(t)
	p := model.Purchase{ID: 1000, IDDelivery: 0, IDClient: 1, IDPartner: 1, Status: 1, Date: "2019-01-07 10:20:47"}
	if _, err := CreatePurchase(p.IDClient, p.IDDelivery, p.IDPartner, p.Status, p.Date); err != nil {
		t.Errorf("Cannot CreatePurchase: %v", err)
	}
}

func TestUpdateClient(t *testing.T) {
	test.ReloadDB(t)
	c := model.Client{ID: 123456, FirstName: "TestUpdateClient", LastName: "cln4", Street: "17 avenue des poussins", ZipCode: "44200", City: "Nantes", AdditionalAddress: "Appartement 17", Email: "ce4@client.fr", PhoneNumber: "0606060607", Password: "cp4", Deadline: "13:42:37"}
	if err := UpdateAddressClient(c.Street, c.ZipCode, c.City, c.AdditionalAddress, c.ID); err == nil {
		t.Errorf("Can update non existant client address: %v", err)
	}
	if err := UpdateClient(c.FirstName, c.LastName, c.Email, c.PhoneNumber, c.Password, c.Deadline, c.ID); err == nil {
		t.Errorf("Can update non existant client: %v", err)
	}
	if err := UpdateAddressClient(c.Street, c.ZipCode, c.City, c.AdditionalAddress, 4); err != nil {
		t.Errorf("Cannot UpdateClient Address: %v", err)
	}
	if err := UpdateClient(c.FirstName, c.LastName, c.Email, c.PhoneNumber, c.Password, c.Deadline, 4); err != nil {
		t.Errorf("Cannot UpdateClient: %v", err)
	}
	/* TODO test useless update (0 row affected) */
}

func TestUpdatePartner(t *testing.T) {
	test.ReloadDB(t)
	p := model.Partner{ID: 123456, Name: "pn2", Street: "42 boulevard de la vie", ZipCode: "44442", City: "Nantes", AdditionalAddress: "", Email: "pe1@partner.fr", PhoneNumber: "0606060606", Password: "pp1", Logo: "https://api.groomshop.fr/images/default.jpg", Siret: "99999999999999"}
	if err := UpdateAddressPartner(p.Street, p.ZipCode, p.City, p.AdditionalAddress, p.ID); err == nil {
		t.Errorf("Can update non existant partner address: %v", err)
	}
	if err := UpdatePartner(p.Name, p.Email, p.PhoneNumber, p.Password, p.Siret, 123456); err == nil {
		t.Errorf("Can update non existant partner: %v", err)
	}
	if err := UpdateAddressPartner(p.Street, p.ZipCode, p.City, p.AdditionalAddress, 3); err != nil {
		t.Errorf("Cannot UpdatePartner Address: %v", err)
	}
	if err := UpdatePartner(p.Name, p.Email, p.PhoneNumber, p.Password, p.Logo, p.Siret, 3); err != nil {
		t.Errorf("Cannot UpdatePartner: %v", err)
	}
	/* TODO test useless update (0 row affected) */
}

func TestUpdatePasswordClient(t *testing.T) {
	test.ReloadDB(t)
	if err := UpdatePasswordClient("toto", "ce1@client.fr"); err != nil {
		t.Errorf("Cannot UpdatePasswordClient: %v", err)
	}
	if err := UpdatePasswordClient("toto", "ce125@client.fr"); err == nil {
		t.Errorf("Can update invalid UpdatePasswordClient: %v", err)
	}
}

func TestUpdateClientPicture(t *testing.T) {
	test.ReloadDB(t)
	if err := UpdateClientPicture("test", 1); err != nil {
		t.Errorf("Cannot UpdateClientPicture: %v", err)
	}
	if err := UpdateClientPicture("test", -1); err == nil {
		t.Errorf("Can update invalid UpdateClientPicture: %v", err)
	}
}

func TestUpdatePasswordPartner(t *testing.T) {
	test.ReloadDB(t)
	if err := UpdatePasswordPartner("toto", "pe1@partner.fr"); err != nil {
		t.Errorf("Cannot UpdatePasswordPartner: %v", err)
	}
	if err := UpdatePasswordPartner("toto", "pe471@partner.fr"); err == nil {
		t.Errorf("Can update invalid UpdatePasswordPartner: %v", err)
	}
}

func TestUpdatePasswordRider(t *testing.T) {
	test.ReloadDB(t)
	if err := UpdatePasswordRider("toto", "re1@rider.fr"); err != nil {
		t.Errorf("Cannot UpdatePasswordRider: %v", err)
	}
	if err := UpdatePasswordRider("toto", "re114@rider.fr"); err == nil {
		t.Errorf("Can update invalid UpdatePasswordRider: %v", err)
	}
}

func TestUpdateRider(t *testing.T) {
	test.ReloadDB(t)
	r := model.Rider{ID: 123456, FirstName: "rfn2", LastName: "rln1", Email: "re1@rider.fr", PhoneNumber: "0606060606", Rating: 0, Password: "rp1", Iban: "FR7630006000011234567890189", Longitude: 12.0, Latitude: 12.0, Free: true, Siret: "99999999999999"}
	if err := UpdateRider(r.FirstName, r.LastName, r.Email, r.PhoneNumber, r.Rating, r.Password, r.Iban, r.Longitude, r.Latitude, r.Free, r.Siret, 123456); err == nil {
		t.Errorf("Can update non existant partner: %v", err)
	}
	if err := UpdateRider(r.FirstName, r.LastName, r.Email, r.PhoneNumber, r.Rating, r.Password, r.Iban, r.Longitude, r.Latitude, r.Free, r.Siret, 3); err != nil {
		t.Errorf("Cannot UpdateRider: %v", err)
	}
	/* TODO test useless update (0 row affected) */
}

func TestUpdateDelivery(t *testing.T) {
	test.ReloadDB(t)
	d := model.Delivery{ID: 1, IDRider: 1, Status: 2, Date: "2019-01-07 15:20:47"}
	if err := UpdateDelivery(d.IDRider, d.Status, d.Date, d.ID); err != nil {
		t.Errorf("Cannot UpdateDelivery: %v", err)
	}
	if err := UpdateDelivery(d.IDRider, d.Status, d.Date, 451); err == nil {
		t.Errorf("Can update invalid UpdateDelivery: %v", err)
	}
}

func TestUpdatePurchase(t *testing.T) {
	test.ReloadDB(t)
	if err := UpdatePurchase(1, 1, 1, 1, "2019-01-07 15:20:47", 1); err != nil {
		t.Errorf("Cannot UpdatePurchase: %v", err)
	}
	if err := UpdatePurchase(1, 1, 1, 1, "2019-01-07 15:20:47", 14548); err == nil {
		t.Errorf("Can update invalid UpdatePurchase: %v", err)
	}
}
func TestAuthClient(t *testing.T) {
	test.ReloadDB(t)
	if _, err := AuthClient("test_auth_client@client.fr", "cp1"); err != nil {
		t.Errorf("Cannot AuthClient: %v", err)
	}
	if _, err := AuthClient("non-existant", "cp1"); err == nil {
		t.Errorf("Can auth invalid client: %v", err)
	}
}

func TestAuthPartner(t *testing.T) {
	test.ReloadDB(t)
	if _, err := AuthPartner("test_auth_partner@partner.fr", "pp1"); err != nil {
		t.Errorf("Cannot AuthPartner: %v", err)
	}
	if _, err := AuthPartner("non-existant", "pp1"); err == nil {
		t.Errorf("Can auth invalid partner: %v", err)
	}
}

func TestAuthRider(t *testing.T) {
	test.ReloadDB(t)
	if _, err := AuthRider("test_auth_rider@rider.fr", "rp1"); err != nil {
		t.Errorf("Cannot AuthRider: %v", err)
	}
	if _, err := AuthRider("non-existant", "rp1"); err == nil {
		t.Errorf("Can auth invalid rider: %v", err)
	}
}

func TestAuthApplication(t *testing.T) {
	test.ReloadDB(t)
	if _, err := AuthApplication(1, "cp1"); err != nil {
		t.Errorf("Cannot AuthApplication: %v", err)
	}
	if _, err := AuthApplication(1001, "random"); err == nil {
		t.Errorf("Can auth invalid application: %v", err)
	}
}

func TestAuthAdmin(t *testing.T) {
	test.ReloadDB(t)
	if _, err := AuthAdmin("contact.groomshop@gmail.com", "admin"); err != nil {
		t.Errorf("Cannot AuthAdmin: %v", err)
	}
	if _, err := AuthAdmin("non-existant", "random"); err == nil {
		t.Errorf("Can auth invalid auth: %v", err)
	}
}

func TestGetRiderBillForSeptember(t *testing.T) {
	test.ReloadDB(t)
	total, err := GetAllRiderPayment(1, "2019-09-01", "2019-10-01")
	if err != nil {
		t.Errorf("Cannot GetAllRiderPayment: %v", err)
	} else {
		test.MyAssert(t, "Total", 5, total)
	}
	average, err := GetAverageRider(1, "2019-09-01", "2019-10-01")
	if err != nil {
		t.Errorf("Cannot GetAverageRider: %v", err)
	} else {
		test.MyAssert(t, "Average", 650, average)
	}
}

func TestGetDeliveryAddresses(t *testing.T) {
	test.ReloadDB(t)
	a, err := GetDeliveryAddresses("1")
	if err != nil {
		t.Errorf("Cannot GetDeliveryAddresses: %v", err)
	} else {
		test.MyAssert(t, "All addresses amount", 2, len(a))
	}
}

func TestGetAllSpecifiedDeliveries(t *testing.T) {
	test.ReloadDB(t)
	d, err := GetAllSpecifiedDeliveries("all")
	if err != nil {
		t.Errorf("Cannot GetAllSpecifiedDelivery: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 3, len(d))
	}
	d, err = GetAllSpecifiedDeliveries("delivery_placed")
	if err != nil {
		t.Errorf("Cannot GetAllSpecifiedDelivery: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 2, len(d))
	}
}

func TestGetPurchaseDeliveries(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetPurchaseDeliveries("all", "1")
	if err != nil {
		t.Errorf("Cannot GetPurchasedDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 1, len(p))
	}
	p, err = GetPurchaseDeliveries("delivery_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetPurchasedDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 1, len(p))
	}
}

func TestGetClientDeliveries(t *testing.T) {
	test.ReloadDB(t)
	d, err := GetClientDeliveries("all", "1")
	if err != nil {
		t.Errorf("Cannot GetClientDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 3, len(d))
	}
	d, err = GetClientDeliveries("delivery_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetClientDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 2, len(d))
	}
}

func TestGetPartnerDeliveries(t *testing.T) {
	test.ReloadDB(t)
	d, err := GetPartnerDeliveries("all", "1")
	if err != nil {
		t.Errorf("Cannot GetPartnerDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 3, len(d))
	}
	d, err = GetPartnerDeliveries("delivery_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetPartnerDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 2, len(d))
	}
}

func TestGetRiderDeliveries(t *testing.T) {
	test.ReloadDB(t)
	d, err := GetRiderDeliveries("all", "1")
	if err != nil {
		t.Errorf("Cannot GetRiderDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 2, len(d))
	}
	d, err = GetRiderDeliveries("delivery_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetRiderDeliveries: %v", err)
	} else {
		test.MyAssert(t, "All deliveries amount", 2, len(d))
	}
}

func TestGetAllSpecifiedPurchases(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetAllSpecifiedPurchases("all")
	if err != nil {
		t.Errorf("Cannot GetAllSpecifiedPurchases: %v", err)
	} else {
		test.MyAssert(t, "All Purchases amount", 5, len(p))
	}
	p, err = GetAllSpecifiedPurchases("purchase_placed")
	if err != nil {
		t.Errorf("Cannot GetAllSpecifiedPurchases: %v", err)
	} else {
		test.MyAssert(t, "All Purchases amount", 3, len(p))
	}
}

func TestGetPartnerPurchases(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetPartnerPurchases("all", "1")
	if err != nil {
		t.Errorf("Cannot GetPartnerPurchases: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 5, len(p))
	}
	p, err = GetPartnerPurchases("purchase_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetPartnerPurchases: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 3, len(p))
	}
}

func TestGetClientPurchases(t *testing.T) {
	test.ReloadDB(t)
	p, err := GetClientPurchases("all", "1")
	if err != nil {
		t.Errorf("Cannot GetClientPurchases: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 5, len(p))
	}
	p, err = GetClientPurchases("purchase_placed", "1")
	if err != nil {
		t.Errorf("Cannot GetClientPurchases: %v", err)
	} else {
		test.MyAssert(t, "All purchases amount", 3, len(p))
	}
}

func TestDeleteClient(t *testing.T) {
	test.ReloadDB(t)
	if err := DeleteClient(1); err != nil {
		t.Errorf("Can't delete client: %v", err)
	}
	if err := DeleteClient(1); err == nil {
		t.Errorf("Can delete invalid client: %v", err)
	}
}

func TestDeletePartner(t *testing.T) {
	test.ReloadDB(t)
	if err := DeletePartner(1); err != nil {
		t.Errorf("Can't delete partner: %v", err)
	}
	if err := DeletePartner(1); err == nil {
		t.Errorf("Can delete invalid partner: %v", err)
	}
}

func TestDeleteRider(t *testing.T) {
	test.ReloadDB(t)
	if err := DeleteRider(1); err != nil {
		t.Errorf("Can't delete rider: %v", err)
	}
	if err := DeleteRider(1); err == nil {
		t.Errorf("Can delete invalid rider: %v", err)
	}
}

/* TODO Get*Deliveries */
/* TODO Get*Purchases */
