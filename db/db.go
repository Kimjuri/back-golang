package db

import (
	"back/configuration"
	"back/model"
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/lhecker/argon2"
)

var db *sql.DB

func init() {
	var err error
	address := fmt.Sprintf("%s:%d", configuration.Configuration.DB_Address, configuration.Configuration.DB_Port)
	creds := fmt.Sprintf("%s:%s", configuration.Configuration.DB_User, configuration.Configuration.DB_Pass)
	infos := fmt.Sprintf("%s@tcp(%s)/%s?collation=utf8_general_ci", creds, address, configuration.Configuration.DB_Name)
	db, err = sql.Open(configuration.Configuration.DB_Type, infos)
	if err != nil {
		fmt.Println(err)
	} else {
		var version string
		db.QueryRow("SELECT VERSION()").Scan(&version)
		fmt.Printf("Database address = %s:%d\n", configuration.Configuration.DB_Address, configuration.Configuration.DB_Port)
		fmt.Println("Database version = [", version, "]")
	}
}

func GetVersion() string {
	var version string
	db.QueryRow("SELECT VERSION()").Scan(&version)
	return version
}

// rowScanner is implemented by sql.Row and sql.Rows
type rowScanner interface {
	Scan(dest ...interface{}) error
}

func scanStatGraph(r rowScanner) (*model.StatGraph, error) {
	var (
		x string
		y string
	)
	if err := r.Scan(&x, &y); err != nil {
		return nil, err
	}
	result := &model.StatGraph{
		X: x,
		Y: y,
	}
	return result, nil
}

func scanHistory(r rowScanner) (*model.History, error) {
	var (
		date     string
		duration int
		amount   int
	)
	if err := r.Scan(&date, &duration, &amount); err != nil {
		return nil, err
	}
	result := &model.History{
		Date:     date,
		Duration: duration,
		Amount:   amount,
	}
	return result, nil
}

func scanClientStat(r rowScanner) (*model.ClientStat, error) {
	var (
		idClient   int
		nbDelivery int
	)
	if err := r.Scan(&idClient, &nbDelivery); err != nil {
		return nil, err
	}
	result := &model.ClientStat{
		IDClient:   idClient,
		NbDelivery: nbDelivery,
	}
	return result, nil
}

func scanClientDeliveryStat(r rowScanner) (*model.ClientDeliveryStat, error) {
	var (
		date  string
		store string
	)
	if err := r.Scan(&date, &store); err != nil {
		return nil, err
	}
	result := &model.ClientDeliveryStat{
		Date:  date,
		Store: store,
	}
	return result, nil
}

func scanReceipt(r rowScanner) (*model.Receipt, error) {
	var (
		id                 int
		receipt            sql.NullString
		receiptWatermarked sql.NullString
		fkDelivery         int
	)
	if err := r.Scan(&id, &receipt, &receiptWatermarked, &fkDelivery); err != nil {
		return nil, err
	}
	result := &model.Receipt{
		ID:                 id,
		Receipt:            receipt.String,
		ReceiptWatermarked: receiptWatermarked.String,
		IDDelivery:         fkDelivery,
	}
	return result, nil
}

func scanClient(s rowScanner) (*model.Client, error) {
	var (
		id                int
		firstname         sql.NullString
		lastname          sql.NullString
		street            sql.NullString
		zipcode           sql.NullString
		city              sql.NullString
		additionaladdress sql.NullString
		email             sql.NullString
		phonenumber       sql.NullString
		password          sql.NullString
		deadline          sql.NullString
		picture           sql.NullString
	)
	if err := s.Scan(&id, &firstname, &lastname, &street, &zipcode, &city, &additionaladdress, &email, &phonenumber, &password, &deadline, &picture); err != nil {
		return nil, err
	}
	c := &model.Client{
		ID:                id,
		FirstName:         firstname.String,
		LastName:          lastname.String,
		Street:            street.String,
		ZipCode:           zipcode.String,
		City:              city.String,
		AdditionalAddress: additionaladdress.String,
		Email:             email.String,
		PhoneNumber:       phonenumber.String,
		Password:          password.String,
		Deadline:          deadline.String,
		Picture:           picture.String,
	}
	return c, nil
}

func scanRider(s rowScanner) (*model.Rider, error) {
	var (
		id          int
		firstname   sql.NullString
		lastname    sql.NullString
		email       sql.NullString
		phonenumber sql.NullString
		rating      int
		password    sql.NullString
		iban        sql.NullString
		longitude   float64
		latitude    float64
		free        bool
		valid       bool
		siret       sql.NullString
	)
	if err := s.Scan(&id, &firstname, &lastname, &email, &phonenumber, &rating, &password, &iban, &longitude, &latitude, &free, &valid, &siret); err != nil {
		return nil, err
	}
	p := &model.Rider{
		ID:          id,
		FirstName:   firstname.String,
		LastName:    lastname.String,
		Email:       email.String,
		PhoneNumber: phonenumber.String,
		Rating:      rating,
		Password:    password.String,
		Iban:        iban.String,
		Longitude:   longitude,
		Latitude:    latitude,
		Free:        free,
		Valid:       valid,
		Siret:       siret.String,
	}
	return p, nil
}

func scanPartner(s rowScanner) (*model.Partner, error) {
	var (
		id                int
		name              sql.NullString
		street            sql.NullString
		zipcode           sql.NullString
		city              sql.NullString
		additionaladdress sql.NullString
		email             sql.NullString
		phonenumber       sql.NullString
		password          sql.NullString
		logo              sql.NullString
		valid             bool
		siret             sql.NullString
	)
	if err := s.Scan(&id, &name, &street, &zipcode, &city, &additionaladdress, &email, &phonenumber, &password, &logo, &valid, &siret); err != nil {
		return nil, err
	}
	p := &model.Partner{
		ID:                id,
		Name:              name.String,
		Street:            street.String,
		ZipCode:           zipcode.String,
		City:              city.String,
		AdditionalAddress: additionaladdress.String,
		Email:             email.String,
		PhoneNumber:       phonenumber.String,
		Password:          password.String,
		Logo:              logo.String,
		Valid:             valid,
		Siret:             siret.String,
	}
	return p, nil
}

func scanDelivery(s rowScanner) (*model.Delivery, error) {
	var (
		id      int
		idRider int
		status  int
		date    string
	)
	if err := s.Scan(&id, &idRider, &status, &date); err != nil {
		return nil, err
	}
	purchases, err := GetPurchasesOfDelivery(id)
	if err != nil {
		return nil, err
	}
	p := &model.Delivery{
		ID:          id,
		IDPurchases: purchases,
		IDRider:     idRider,
		Status:      status,
		Date:        date,
	}
	return p, nil
}

func scanAddress(s rowScanner, t string) (*model.Address, error) {
	var (
		street            string
		zipcode           string
		city              string
		additionaladdress sql.NullString
	)
	if err := s.Scan(&street, &zipcode, &city, &additionaladdress); err != nil {
		return nil, err
	}
	a := &model.Address{
		Type:              t,
		Street:            street,
		ZipCode:           zipcode,
		City:              city,
		AdditionalAddress: additionaladdress.String,
	}
	return a, nil
}

func scanUser(s rowScanner, t string) (*model.User, error) {
	var (
		id int
	)
	if err := s.Scan(&id); err != nil {
		return nil, err
	}
	a := &model.User{
		Type: t,
		ID:   id,
	}
	return a, nil
}

func scanPurchases(s rowScanner) (*model.Purchase, error) {
	var (
		id         int
		idDelivery int
		idClient   int
		idPartner  int
		status     sql.NullInt64
		date       string
	)
	if err := s.Scan(&id, &idDelivery, &idClient, &idPartner, &status, &date); err != nil {
		return nil, err
	}
	p := &model.Purchase{
		ID:         id,
		IDDelivery: idDelivery,
		IDClient:   idClient,
		IDPartner:  idPartner,
		Status:     status.Int64,
		Date:       date,
		Receipt:    "",
	}
	return p, nil
}

func scanAccount(s rowScanner) (*model.Account, error) {
	var (
		id            int
		idApplication int
		role          sql.NullString
		password      sql.NullString
	)
	if err := s.Scan(&id, &idApplication, &role, &password); err != nil {
		return nil, err
	}
	a := &model.Account{
		ID:            id,
		IDApplication: idApplication,
		Role:          role.String,
		Password:      password.String,
	}
	return a, nil
}

func scanAdmin(s rowScanner) (*model.Admin, error) {
	var (
		id       int
		email    sql.NullString
		password sql.NullString
	)
	if err := s.Scan(&id, &email, &password); err != nil {
		return nil, err
	}
	a := &model.Admin{
		ID:       id,
		Email:    email.String,
		Password: password.String,
	}
	return a, nil
}

func scanIDPurchase(s rowScanner) (int, error) {
	var (
		id int
	)
	if err := s.Scan(&id); err != nil {
		return 0, err
	}
	return id, nil
}

func CheckClientExistingAccount(mail string) (bool, error) {
	if stmt, err := db.Prepare("SELECT * FROM clients WHERE email = ?;"); err != nil {
		return false, fmt.Errorf("CheckClientExistingAccount: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(mail); err != nil {
		return false, err
	} else {
		defer rows.Close()
		if rows.Next() == false {
			return false, nil
		}
	}
	return true, nil
}

func CheckPartnerExistingAccount(mail string) (bool, error) {
	if stmt, err := db.Prepare("SELECT * FROM partners WHERE email = ?;"); err != nil {
		return false, fmt.Errorf("CheckPartnerExistingAccount: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(mail); err != nil {
		return false, err
	} else {
		defer rows.Close()
		if rows.Next() == false {
			return false, nil
		}
	}
	return true, nil
}

func CheckRiderExistingAccount(mail string) (bool, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders WHERE email = ?;"); err != nil {
		return false, fmt.Errorf("CheckRiderExistingAccount: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(mail); err != nil {
		return false, err
	} else {
		defer rows.Close()
		if rows.Next() == false {
			return false, nil
		}
	}
	return true, nil
}

func CheckReceiptPartnerId(id_purchase string, id_partner string) (bool, error) {
	if stmt, err := db.Prepare("SELECT * FROM purchases WHERE id = ? AND fk_partner = ?;"); err != nil {
		return false, err
	} else if rows, err := stmt.Query(id_purchase, id_partner); err != nil {
		return false, err
	} else {
		defer rows.Close()
		if rows.Next() == false {
			return false, nil
		}
		return true, nil
	}
}

func GetValidAccounts() ([]*model.User, error) {
	var users []*model.User
	if stmt, err := db.Prepare("SELECT partners.id FROM partners WHERE valid=true;"); err != nil {
		return nil, fmt.Errorf("GetValidAccounts: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		for rows.Next() {
			if u, err := scanUser(rows, "partner"); err != nil {
				return nil, fmt.Errorf("GetValidAccounts: could not read row: %v", err)
			} else {
				users = append(users, u)
			}
		}
		if stmt, err := db.Prepare("SELECT riders.id FROM riders WHERE valid=true;"); err != nil {
			return nil, fmt.Errorf("GetValidAccounts: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(); err != nil {
			return nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if u, err := scanUser(rows, "rider"); err != nil {
					return nil, fmt.Errorf("GetValidAccounts: could not read row: %v", err)
				} else {
					users = append(users, u)
				}
			}
			return users, nil
		}
	}
}

func GetNonValidAccounts() ([]*model.User, error) {
	var users []*model.User
	if stmt, err := db.Prepare("SELECT partners.id FROM partners WHERE valid=false;"); err != nil {
		return nil, fmt.Errorf("GetNonValidAccounts: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		for rows.Next() {
			if u, err := scanUser(rows, "partner"); err != nil {
				return nil, fmt.Errorf("GetNonValidAccounts: could not read row: %v", err)
			} else {
				users = append(users, u)
			}
		}
		if stmt, err := db.Prepare("SELECT riders.id FROM riders WHERE valid=false;"); err != nil {
			return nil, fmt.Errorf("GetValidAccounts: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(); err != nil {
			return nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if u, err := scanUser(rows, "rider"); err != nil {
					return nil, fmt.Errorf("GetNonValidAccounts: could not read row: %v", err)
				} else {
					users = append(users, u)
				}
			}
			return users, nil
		}
	}
}

func GetUsers() ([]*model.User, error) {
	var users []*model.User
	if stmt, err := db.Prepare("SELECT clients.id FROM clients;"); err != nil {
		return nil, fmt.Errorf("GetUsers: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		for rows.Next() {
			if u, err := scanUser(rows, "client"); err != nil {
				return nil, fmt.Errorf("GetUsers: could not read row: %v", err)
			} else {
				users = append(users, u)
			}
		}
		if stmt, err := db.Prepare("SELECT partners.id FROM partners;"); err != nil {
			return nil, fmt.Errorf("GetUsers: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(); err != nil {
			return nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if u, err := scanUser(rows, "partner"); err != nil {
					return nil, fmt.Errorf("GetUsers: could not read row: %v", err)
				} else {
					users = append(users, u)
				}
			}
			if stmt, err := db.Prepare("SELECT riders.id FROM riders;"); err != nil {
				return nil, fmt.Errorf("GetUsers: cant db.Prepare() : %v", err)
			} else if rows, err := stmt.Query(); err != nil {
				return nil, err
			} else {
				defer rows.Close()
				for rows.Next() {
					if u, err := scanUser(rows, "rider"); err != nil {
						return nil, fmt.Errorf("GetUsers: could not read row: %v", err)
					} else {
						users = append(users, u)
					}
				}
				return users, nil
			}
		}
	}
}

func GetReceiptByPurchase(id string) (*model.Receipt, error) {
	if stmt, err := db.Prepare("SELECT id, receipt, receiptWatermarked, fk_purchase FROM receipts WHERE fk_purchase = ?;"); err != nil {
		return nil, err
	} else if receipt, err := scanReceipt(stmt.QueryRow(id)); err != nil {
		return nil, err
	} else {
		return receipt, nil
	}
}

func GetClient(id string) (*model.Client, error) {
	if stmt, err := db.Prepare("SELECT clients.id, clients.firstname, clients.lastname, address.street, address.zipcode, address.city, address.additionaladdress, clients.email, clients.phonenumber, clients.password, clients.deadline, clients.picture FROM clients INNER JOIN address ON clients.fk_address = address.id WHERE clients.id = ?;"); err != nil {
		return nil, fmt.Errorf("GetClient: can't db.Prepare(): %v", err)
	} else if p, err := scanClient(stmt.QueryRow(id)); err != nil {
		return nil, fmt.Errorf("GetClient: could not get client: %v", err)
	} else {
		return p, nil
	}
}

func GetAllClients() ([]*model.Client, error) {
	if stmt, err := db.Prepare("SELECT clients.id, clients.firstname, clients.lastname, address.street, address.zipcode, address.city, address.additionaladdress, clients.email, clients.phonenumber, clients.password, clients.deadline, clients.picture FROM clients INNER JOIN address ON clients.fk_address = address.id;"); err != nil {
		return nil, fmt.Errorf("GetAllClients: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var clients []*model.Client
		for rows.Next() {
			if d, err := scanClient(rows); err != nil {
				return nil, fmt.Errorf("GetAllClients: could not read row: %v", err)
			} else {
				clients = append(clients, d)
			}
		}
		return clients, nil
	}
	return nil, errors.New("GetAllClients : undefined behaviour")
}

func GetRider(id string) (*model.Rider, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders WHERE id = ?;"); err != nil {
		return nil, fmt.Errorf("GetRider: can't db.Prepare(): %v", err)
	} else if p, err := scanRider(stmt.QueryRow(id)); err != nil {
		return nil, fmt.Errorf("GetRider: could not get rider: %v", err)
	} else {
		return p, nil
	}
}

func GetAllValidRiders() ([]*model.Rider, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders WHERE valid=true"); err != nil {
		return nil, fmt.Errorf("GetAllRiders: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var riders []*model.Rider
		for rows.Next() {
			if d, err := scanRider(rows); err != nil {
				return nil, fmt.Errorf("GetAllValidRiders: could not read row: %v", err)
			} else {
				riders = append(riders, d)
			}
		}
		return riders, nil
	}
	return nil, errors.New("GetAllRiders : undefined behaviour")
}

func GetAllNonValidRiders() ([]*model.Rider, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders WHERE valid=false"); err != nil {
		return nil, fmt.Errorf("GetAllNonValidRiders: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var riders []*model.Rider
		for rows.Next() {
			if d, err := scanRider(rows); err != nil {
				return nil, fmt.Errorf("GetAllNonValidRiders: could not read row: %v", err)
			} else {
				riders = append(riders, d)
			}
		}
		return riders, nil
	}
	return nil, errors.New("GetAllRiders : undefined behaviour")
}

func GetAllRiders() ([]*model.Rider, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders"); err != nil {
		return nil, fmt.Errorf("GetAllRiders: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var riders []*model.Rider
		for rows.Next() {
			if d, err := scanRider(rows); err != nil {
				return nil, fmt.Errorf("GetAllRiders: could not read row: %v", err)
			} else {
				riders = append(riders, d)
			}
		}
		return riders, nil
	}
	return nil, errors.New("GetAllRiders : undefined behaviour")
}

func GetFreeRiders() ([]*model.Rider, error) {
	if stmt, err := db.Prepare("SELECT * FROM riders WHERE free=true"); err != nil {
		return nil, fmt.Errorf("GetFreeRiders: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var freeRiders []*model.Rider
		for rows.Next() {
			if d, err := scanRider(rows); err != nil {
				return nil, fmt.Errorf("GetFreeRiders: could not read row: %v", err)
			} else {
				freeRiders = append(freeRiders, d)
			}
		}
		return freeRiders, nil
	}
	return nil, errors.New("GetFreeRiders : undefined behaviour")
}

func GetPartner(id string) (*model.Partner, error) {
	if stmt, err := db.Prepare("SELECT partners.id, partners.name, address.street, address.zipcode, address.city, address.additionaladdress, partners.email, partners.phonenumber, partners.password, partners.logo, partners.valid, partners.siret FROM partners INNER JOIN address ON partners.fk_address = address.id where partners.id = ?;"); err != nil {
		return nil, fmt.Errorf("GetPartner: cant db.Prepare() : %v", err)
	} else if p, err := scanPartner(stmt.QueryRow(id)); err != nil {
		return nil, fmt.Errorf("GetPartner: could not get partner: %v", err)
	} else {
		return p, nil
	}
}

func GetAllValidPartners() ([]*model.Partner, error) {
	if stmt, err := db.Prepare("SELECT partners.id, partners.name, address.street, address.zipcode, address.city, address.additionaladdress, partners.email, partners.phonenumber, partners.password, partners.logo, partners.valid, partners.siret FROM partners INNER JOIN address ON partners.fk_address = address.id WHERE partners.valid=true;"); err != nil {
		return nil, fmt.Errorf("GetAllValidPartners: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var partners []*model.Partner
		for rows.Next() {
			if d, err := scanPartner(rows); err != nil {
				return nil, fmt.Errorf("GetAllValidPartners: could not read row: %v", err)
			} else {
				partners = append(partners, d)
			}
		}
		return partners, nil
	}
	return nil, errors.New("GetAllValidPartners : undefined behaviour")
}

func GetAllNonValidPartners() ([]*model.Partner, error) {
	if stmt, err := db.Prepare("SELECT partners.id, partners.name, address.street, address.zipcode, address.city, address.additionaladdress, partners.email, partners.phonenumber, partners.password, partners.logo, partners.valid, partners.siret FROM partners INNER JOIN address ON partners.fk_address = address.id WHERE partners.valid=false;"); err != nil {
		return nil, fmt.Errorf("GetAllNonValidPartners: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var partners []*model.Partner
		for rows.Next() {
			if d, err := scanPartner(rows); err != nil {
				return nil, fmt.Errorf("GetAllNonValidPartners: could not read row: %v", err)
			} else {
				partners = append(partners, d)
			}
		}
		return partners, nil
	}
	return nil, errors.New("GetAllNonValidPartners : undefined behaviour")
}

func GetAllPartners() ([]*model.Partner, error) {
	if stmt, err := db.Prepare("SELECT partners.id, partners.name, address.street, address.zipcode, address.city, address.additionaladdress, partners.email, partners.phonenumber, partners.password, partners.logo, partners.valid, partners.siret FROM partners INNER JOIN address ON partners.fk_address = address.id;"); err != nil {
		return nil, fmt.Errorf("GetAllPartners: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var partners []*model.Partner
		for rows.Next() {
			if d, err := scanPartner(rows); err != nil {
				return nil, fmt.Errorf("GetAllPartners: could not read row: %v", err)
			} else {
				partners = append(partners, d)
			}
		}
		return partners, nil
	}
	return nil, errors.New("GetAllPartners : undefined behaviour")
}

func GetPurchases(req string, arg ...interface{}) ([]*model.Purchase, error) {
	if stmt, err := db.Prepare(req); err != nil {
		return nil, fmt.Errorf("GetPurchases: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(arg...); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var purchases []*model.Purchase
		for rows.Next() {
			if d, err := scanPurchases(rows); err != nil {
				return nil, fmt.Errorf("GetPurchases: could not read row: %v", err)
			} else {
				purchases = append(purchases, d)
			}
		}
		return purchases, nil
	}
	return nil, errors.New("GetPurchases : undefined behaviour")
}

func GetPurchasePartnerForClient(client_id int, purcharse_id int) (*model.Partner, error) {
	var partner_id string
	row := db.QueryRow(`SELECT fk_partner FROM purchases WHERE id=? AND fk_client=?;`, purcharse_id, client_id)
	err := row.Scan(&partner_id)
	if err != nil {
		return nil, err
	}
	if partner, err := GetPartner(partner_id); err != nil {
		return nil, err
	} else {
		return partner, nil
	}
	return nil, errors.New("GetPurchasePartnerForClient: undefined behaviour")
}

func GetPurchasesOfDelivery(id int) ([]int, error) {
	var purchases []int
	if stmt, err := db.Prepare("SELECT DISTINCT purchases.id  FROM purchases WHERE purchases.fk_delivery = ?;"); err != nil {
		return nil, fmt.Errorf("GetPurchasesOfDelivery: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		for rows.Next() {
			if p, err := scanIDPurchase(rows); err != nil {
				return nil, fmt.Errorf("GetPurchasesOfDelivery: could not read row: %v", err)
			} else {
				purchases = append(purchases, p)
			}
		}
		return purchases, nil
	}
	return nil, errors.New("GetPurchasesOfDelivery : undefined behaviour")
}

func GetDeliveries(req string, arg ...interface{}) ([]*model.Delivery, error) {
	if stmt, err := db.Prepare(req); err != nil {
		return nil, fmt.Errorf("GetDeliveries: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(arg...); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var deliveries []*model.Delivery
		for rows.Next() {
			if d, err := scanDelivery(rows); err != nil {
				return nil, fmt.Errorf("GetDeliveries: could not read row: %v", err)
			} else {
				deliveries = append(deliveries, d)
			}
		}
		return deliveries, nil
	}
	return nil, errors.New("GetDeliveries : undefined behaviour")
}

func GetPartnerPurchasesStats(id string, start string, end string) ([]*model.StatGraph, error) {
	if stmt, err := db.Prepare("SELECT COUNT(id) as 'y', DATE(date) as 'x' FROM purchases WHERE fk_partner=? AND fk_status=6 AND date BETWEEN ? AND ? GROUP BY DATE(date) ORDER BY date ASC;"); err != nil {
		return nil, fmt.Errorf("GetPartnerPurchasesStats: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id, start, end); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var stats []*model.StatGraph
		for rows.Next() {
			fmt.Println("New row")
			if s, err := scanStatGraph(rows); err != nil {
				return nil, fmt.Errorf("GetPartnerPurchasesStats: could not read row: %v", err)
			} else {
				stats = append(stats, s)
			}
		}
		return stats, nil
	}
	return nil, errors.New("GetDeliveries : undefined behaviour")
}

func dbExec(req string, args ...interface{}) (int64, int64, error) {
	stmt, err := db.Prepare(req)
	if err != nil {
		return 0, 0, err
	}
	res, err := stmt.Exec(args...)
	if err != nil {
		return 0, 0, err
	}
	r, err := res.RowsAffected()
	if err != nil {
		return 0, 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, 0, err
	}
	return id, r, nil
}

func CreateAddress(args ...interface{}) (int64, error) {
	id, _, err := dbExec("INSERT INTO address (street, zipcode, city, additionaladdress) VALUES (?,?,?,?);", args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func CreateClient(args ...interface{}) (int64, error) {
	id, _, err := dbExec("INSERT INTO clients (firstname,lastname,fk_address,email,phonenumber,password,deadline) VALUES (?,?,?,?,?,?,?);", args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func CreateRider(args ...interface{}) (int64, error) {
	id, _, err := dbExec("INSERT INTO riders (firstname,lastname,email,phonenumber,rating,password,iban,longitude,latitude, siret) VALUES (?,?,?,?,?,?,?,0,0, ?);", args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func CreatePartner(n string, id_a int64, e string, p string, pw string, l string,  s string) (int64, error) {
	id, _, err := dbExec("INSERT INTO partners (name,fk_address,email,phonenumber,password,logo,siret) VALUES (?,?,?,?,?,?,?);", n, id_a, e, p, pw, l, s)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func CreateDelivery(purchases []int, idRider int, status int, tf string) (int64, error) {
	id, _, err := dbExec("INSERT INTO deliveries (fk_rider, fk_status, date) VALUES (?,?,?);", idRider, status, tf)
	if err != nil {
		return 0, err
	} else {
		for _, purchase := range purchases {
			if _, _, err := dbExec("UPDATE purchases SET fk_delivery=? WHERE id=?;", id, purchase); err != nil {
				return 0, err
			}
		}
	}
	return id, nil
}

func CreateReceipt(receipt string, receiptWatermarked string, idPurchase int) error {
	if _, _, err := dbExec("INSERT INTO receipts (receipt, receiptWatermarked, fk_purchase) VALUES (?, ?, ?);", receipt, receiptWatermarked, idPurchase); err != nil {
		return err
	}
	return nil
}

func CreateDeliveryLog(args ...interface{}) (int64, error) {
	id, _, err := dbExec("INSERT INTO deliveries_logs (fk_delivery, date, description) VALUES (?, ?, ?);", args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func CreateBillForDelivery(d *model.Delivery) error {
	t := time.Now()
	tf := t.Format("2006-01-02 15:04:05")
	newT, er := time.Parse("2006-01-02 15:04:05", tf)
	if er != nil {
		fmt.Println("Problem when calculating duration of delivery")
		return er
	}
	tStart, e := time.Parse("2006-01-02 15:04:05", d.Date)
	if e != nil {
		fmt.Println("Problem when calculating duration of delivery")
		return e
	}
	duration := newT.Sub(tStart).Seconds()
	pTab, err := GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE purchases.fk_delivery = ?;", d.ID)
	if err != nil {
		fmt.Println("Problem when getting purchases")
		return err
	} else {
		if _, _, err := dbExec("INSERT INTO bills (fk_rider, fk_client, fk_delivery, amount, date, time) VALUES (?,?,?,?,?,?);", d.IDRider, pTab[0].IDClient, d.ID, 5, tf, duration); err != nil {
			fmt.Println("Problem when creating bill")
			return err
		}
	}
	return nil
}

func GetAllCurrentRiderPayment(id int) (int, error) {
	var money int
	row := db.QueryRow(`SELECT SUM(amount) FROM bills WHERE MONTH(date) = MONTH(CURDATE()) AND fk_rider=?;`, id)
	err := row.Scan(&money)
	if err != nil {
		return 0, err
	} else {
		return money, nil
	}
}

func GetAllRiderPayment(id int, start string, end string) (int, error) {
	var money int
	row := db.QueryRow(`SELECT SUM(amount) FROM bills WHERE fk_rider=? AND date BETWEEN ? AND ?;`, id, start, end)
	err := row.Scan(&money)
	if err != nil {
		return 0, err
	} else {
		return money, nil
	}
}

func GetAverageCurrentRider(id int) (int, error) {
	var average int
	row := db.QueryRow(`SELECT ROUND(AVG(time)) FROM bills WHERE MONTH(date) = MONTH(CURDATE()) AND fk_rider=?;`, id)
	err := row.Scan(&average)
	if err != nil {
		return 0, err
	} else {
		return average, nil
	}
}

func GetAverageRider(id int, start string, end string) (int, error) {
	var average int
	row := db.QueryRow(`SELECT ROUND(AVG(time)) FROM bills WHERE fk_rider=? AND date BETWEEN ? AND ?;`, id, start, end)
	err := row.Scan(&average)
	if err != nil {
		return 0, err
	} else {
		return average, nil
	}
}

func GetHistoryCurrentRider(id int) ([]*model.History, error) {
	if stmt, err := db.Prepare("SELECT date, time, amount FROM bills WHERE fk_rider=? AND MONTH(date) = MONTH(CURDATE()) ORDER BY date ASC;"); err != nil {
		return nil, fmt.Errorf("GetAllClients: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var history []*model.History
		for rows.Next() {
			if d, err := scanHistory(rows); err != nil {
				return nil, fmt.Errorf("GetHistoryCurrentRider: could not read row: %v", err)
			} else {
				history = append(history, d)
			}
		}
		return history, nil
	}
	return nil, errors.New("GetHistoryCurrentRider : undefined behaviour")
}

func GetHistoryRider(id int, start string, end string) ([]*model.History, error) {
	if stmt, err := db.Prepare("SELECT date, time, amount FROM bills WHERE fk_rider=? AND date BETWEEN ? AND ? ORDER BY date ASC;"); err != nil {
		return nil, fmt.Errorf("GetAllClients: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id, start, end); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var history []*model.History
		for rows.Next() {
			if d, err := scanHistory(rows); err != nil {
				return nil, fmt.Errorf("GetHistoryRider: could not read row: %v", err)
			} else {
				history = append(history, d)
			}
		}
		return history, nil
	}
	return nil, errors.New("GetHistoryRider : undefined behaviour")
}

func GetCurrentClientsHabits(id int) ([]*model.ClientStat, error) {
	if stmt, err := db.Prepare("SELECT purchases.fk_client, COUNT(DISTINCT deliveries.id) FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE purchases.fk_partner = ? AND MONTH(deliveries.date) = MONTH(CURDATE()) GROUP BY fk_client;"); err != nil {
		return nil, fmt.Errorf("GetCurrentClientsHabits: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var clientStat []*model.ClientStat
		for rows.Next() {
			if d, err := scanClientStat(rows); err != nil {
				return nil, fmt.Errorf("GetCurrentClientsHabits: could not read row: %v", err)
			} else {
				clientStat = append(clientStat, d)
			}
		}
		return clientStat, nil
	}
	return nil, errors.New("GetCurrentClientsHabits : undefined behaviour")
}

func GetClientsHabits(id int, start string, end string) ([]*model.ClientStat, error) {
	if stmt, err := db.Prepare("SELECT purchases.fk_client, COUNT(DISTINCT deliveries.id) FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE purchases.fk_partner = ? AND deliveries.date BETWEEN ? AND ? GROUP BY fk_client;"); err != nil {
		return nil, fmt.Errorf("GetClientsHabits: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id, start, end); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var clientStat []*model.ClientStat
		for rows.Next() {
			if d, err := scanClientStat(rows); err != nil {
				return nil, fmt.Errorf("GetClientsHabits: could not read row: %v", err)
			} else {
				clientStat = append(clientStat, d)
			}
		}
		return clientStat, nil
	}
	return nil, errors.New("GetClientsHabits : undefined behaviour")
}

func GetCurrentClientsHabitsAdmin() ([]*model.ClientStat, error) {
	if stmt, err := db.Prepare("SELECT purchases.fk_client, COUNT(DISTINCT deliveries.id) FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE deliveries.date BETWEEN ? AND ? GROUP BY fk_client;"); err != nil {
		return nil, fmt.Errorf("GetCurrentClientsHabitsAdmin: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var clientStat []*model.ClientStat
		for rows.Next() {
			if d, err := scanClientStat(rows); err != nil {
				return nil, fmt.Errorf("GetCurrentClientsHabitsAdmin: could not read row: %v", err)
			} else {
				clientStat = append(clientStat, d)
			}
		}
		return clientStat, nil
	}
	return nil, errors.New("GetCurrentClientsHabitsAdmin : undefined behaviour")
}

func GetClientsHabitsAdmin(start string, end string) ([]*model.ClientStat, error) {
	if stmt, err := db.Prepare("SELECT purchases.fk_client, COUNT(DISTINCT deliveries.id) FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE deliveries.date BETWEEN ? AND ? GROUP BY fk_client;"); err != nil {
		return nil, fmt.Errorf("GetClientsHabitsAdmin: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(start, end); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		var clientStat []*model.ClientStat
		for rows.Next() {
			if d, err := scanClientStat(rows); err != nil {
				return nil, fmt.Errorf("GetClientsHabitsAdmin: could not read row: %v", err)
			} else {
				clientStat = append(clientStat, d)
			}
		}
		return clientStat, nil
	}
	return nil, errors.New("GetClientsHabitsAdmin : undefined behaviour")
}

func GetCurrentRiderStatAdmin() (int, int, float32, error) {
	var total, time int
	var average float32
	row := db.QueryRow(`SELECT COUNT(*) FROM bills WHERE MONTH(date) = MONTH(CURDATE());`)
	err := row.Scan(&total)
	if err != nil {
		return 0, 0, 0, err
	} else {
		row := db.QueryRow(`SELECT ROUND(AVG(time)) FROM bills WHERE MONTH(date) = MONTH(CURDATE());`)
		err := row.Scan(&time)
		if err != nil {
			return 0, 0, 0, err
		} else {
			row := db.QueryRow(`SELECT AVG(Count) FROM (SELECT COUNT(id) AS Count FROM bills WHERE MONTH(date) = MONTH(CURDATE()) GROUP BY fk_rider) as Count;`)
			err := row.Scan(&average)
			if err != nil {
				fmt.Println("COUCOU2")
				return 0, 0, 0, err
			} else {
				return total, time, average, nil
			}
		}
	}
}

func GetRiderStatAdmin(start string, end string) (int, int, float32, error) {
	var total, time int
	var average float32
	row := db.QueryRow(`SELECT COUNT(*) FROM bills WHERE date BETWEEN ? AND ?;`, start, end)
	err := row.Scan(&total)
	if err != nil {
		return 0, 0, 0, err
	} else {
		row := db.QueryRow(`SELECT ROUND(AVG(time)) FROM bills WHERE date BETWEEN ? AND ?;`, start, end)
		err := row.Scan(&time)
		if err != nil {
			return 0, 0, 0, err
		} else {
			row := db.QueryRow(`SELECT AVG(Count) FROM (SELECT COUNT(id) AS Count FROM bills WHERE date BETWEEN ? AND ? GROUP BY fk_rider) as Count;`, start, end)
			err := row.Scan(&average)
			if err != nil {
				return 0, 0, 0, err
			} else {
				return total, time, average, nil
			}
		}
	}
}

func GetCurrentClientDeliveryStat(id int) (int, []*model.ClientDeliveryStat, error) {
	var total int
	var clientDeliveryStat []*model.ClientDeliveryStat
	row := db.QueryRow(`SELECT COUNT(*) FROM bills WHERE fk_client=? AND MONTH(date) = MONTH(CURDATE());`, id)
	err := row.Scan(&total)
	if err != nil {
		return 0, nil, err
	} else {
		if stmt, err := db.Prepare("SELECT bills.date, GROUP_CONCAT(partners.name) FROM bills INNER JOIN deliveries ON bills.fk_delivery = deliveries.id INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN partners ON purchases.fk_partner = partners.id WHERE bills.fk_client = ? AND MONTH(bills.date) = MONTH(CURDATE()) GROUP BY bills.date ORDER BY bills.date ASC;"); err != nil {
			return 0, nil, fmt.Errorf("GetCurrentClientDeliveryStat: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(id); err != nil {
			return 0, nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if d, err := scanClientDeliveryStat(rows); err != nil {
					return 0, nil, fmt.Errorf("GetCurrentClientDeliveryStat: could not read row: %v", err)
				} else {
					clientDeliveryStat = append(clientDeliveryStat, d)
				}
			}
			return total, clientDeliveryStat, nil
		}
	}
}

func GetClientDeliveryStat(id int, start string, end string) (int, []*model.ClientDeliveryStat, error) {
	var total int
	var clientDeliveryStat []*model.ClientDeliveryStat
	row := db.QueryRow(`SELECT COUNT(*) FROM bills WHERE fk_client=? AND date BETWEEN ? AND ?;`, id, start, end)
	err := row.Scan(&total)
	if err != nil {
		return 0, nil, err
	} else {
		if stmt, err := db.Prepare("SELECT bills.date, GROUP_CONCAT(partners.name) FROM bills INNER JOIN deliveries ON bills.fk_delivery = deliveries.id INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN partners ON purchases.fk_partner = partners.id WHERE bills.fk_client = ? AND bills.date BETWEEN ? AND ? GROUP BY bills.date ORDER BY bills.date ASC;"); err != nil {
			return 0, nil, fmt.Errorf("GetClientDeliveryStat: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(id, start, end); err != nil {
			return 0, nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if d, err := scanClientDeliveryStat(rows); err != nil {
					return 0, nil, fmt.Errorf("GetClientDeliveryStat: could not read row: %v", err)
				} else {
					clientDeliveryStat = append(clientDeliveryStat, d)
				}
			}
			return total, clientDeliveryStat, nil
		}
	}
}

func CreatePurchase(args ...interface{}) (int64, error) {
	id, _, err := dbExec("INSERT INTO purchases (fk_delivery, fk_client, fk_partner, fk_status, date) VALUES (?,?,?,?,?);", args...)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func UpdateAddressClient(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE address INNER JOIN clients ON clients.fk_address = address.id SET street=?, zipcode=?, city=?, additionaladdress=? WHERE clients.id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdateClient: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdateAddressPartner(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE address INNER JOIN partners ON partners.fk_address = address.id SET street=?, zipcode=?, city=?, additionaladdress=? WHERE partners.id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePartner: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdatePasswordClient(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE clients SET password=? WHERE email=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePasswordClient : expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdateClient(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE clients SET firstname=?, lastname=?, email=?, phonenumber=?, password=?, deadline=? WHERE id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdateClient: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdateClientPicture(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE clients SET picture=? WHERE id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdateClient: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdatePasswordPartner(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE partners SET password=? WHERE email=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePasswordPartner : expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdatePartner(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE partners SET name=?, email=?, phonenumber=?, password=?, logo=?, siret=? WHERE id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePartner: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdatePasswordRider(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE riders SET password=? WHERE email=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePasswordRider : expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdateRider(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE riders SET firstname=?, lastname=?, email=?, phonenumber=?, rating=?, password=?, iban=?, longitude=?, latitude=?, free=?, siret=? WHERE id=?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdateRider: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdateDelivery(args ...interface{}) error {
	_, r, err := dbExec("UPDATE deliveries SET fk_rider=?, fk_status=?, date=? WHERE id=?;", args...)
	if err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdateDelivery: expected 1 row affected, got %d", r)
	}
	return nil
}

func UpdatePurchase(args ...interface{}) error {
	_, r, err := dbExec("UPDATE purchases SET fk_delivery=?, fk_client=?, fk_partner=?, fk_status=?, date=? WHERE id=?;", args...)
	if err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("UpdatePurchase: expected 1 row affected, got %d", r)
	}
	return nil
}

func AuthClient(email string, password string) (*model.Client, error) {
	stmt, err := db.Prepare("SELECT clients.id, clients.firstname, clients.lastname, address.street, address.zipcode, address.city, address.additionaladdress, clients.email, clients.phonenumber, clients.password, clients.deadline, clients.picture FROM clients INNER JOIN address ON clients.fk_address = address.id WHERE email = ?;")
	if err != nil {
		return nil, fmt.Errorf("AuthClient: can't db.Prepare() : %v", err)
	}
	c, err := scanClient(stmt.QueryRow(email))
	if err != nil {
		return nil, errors.New("AuthClient failed")
	}
	raw, err := argon2.Decode([]byte(c.Password))
	if err != nil {
		fmt.Printf("Error during decoding: %s\n", err.Error())
	}
	ok, err := raw.Verify([]byte(password))
	if err != nil {
		fmt.Printf("Error during verification: %s\n", err.Error())
	}
	if ok {
		return c, nil
	}
	return nil, errors.New("AuthClient failed")
}

func AuthPartner(email string, password string) (*model.Partner, error) {
	stmt, err := db.Prepare("SELECT partners.id, partners.name, address.street, address.zipcode, address.city, address.additionaladdress, partners.email, partners.phonenumber, partners.password, partners.logo, partners.valid, partners.siret FROM partners INNER JOIN address ON partners.fk_address = address.id WHERE email = ?;")
	if err != nil {
		return nil, fmt.Errorf("AuthPartner: can't db.Prepare() : %v", err)
	}
	c, err := scanPartner(stmt.QueryRow(email))
	if err != nil {
		return nil, errors.New("AuthPartner failed")
	}
	if c.Valid != true {
		return nil, errors.New("Account has not been validated by admin")
	}
	raw, err := argon2.Decode([]byte(c.Password))
	if err != nil {
		fmt.Printf("Error during decoding: %s\n", err.Error())
	}
	ok, err := raw.Verify([]byte(password))
	if err != nil {
		fmt.Printf("Error during verification: %s\n", err.Error())
	}
	if ok {
		return c, nil
	}
	return nil, errors.New("AuthPartner failed")
}

func AuthRider(email string, password string) (*model.Rider, error) {
	stmt, err := db.Prepare("SELECT * FROM riders WHERE email = ?;")
	if err != nil {
		return nil, fmt.Errorf("AuthRider: can't db.Prepare() : %v", err)
	}
	a, err := scanRider(stmt.QueryRow(email))
	if err != nil {
		return nil, fmt.Errorf("AuthRider failed : %v", err)
	}
	if a.Valid != true {
		fmt.Errorf("Account has not been validated by admin")
		return nil, errors.New("Account has not been validated by admin")
	}
	raw, err := argon2.Decode([]byte(a.Password))
	if err != nil {
		fmt.Printf("Error during decoding: %s\n", err.Error())
	}
	ok, err := raw.Verify([]byte(password))
	if err != nil {
		fmt.Printf("Error during verification: %s\n", err.Error())
	}
	if ok {
		return a, nil
	}
	return nil, errors.New("AuthRider failed")
}

func AuthApplication(idApplication int, password string) (*model.Account, error) {
	stmt, err := db.Prepare("SELECT * FROM accounts WHERE application_id = ?;")
	if err != nil {
		return nil, fmt.Errorf("AuthApplication: can't db.Prepare() : %v", err)
	}
	c, err := scanAccount(stmt.QueryRow(idApplication))
	if err != nil {
		return nil, errors.New("AuthApplicationfailed")
	}
	raw, err := argon2.Decode([]byte(c.Password))
	if err != nil {
		fmt.Printf("Error during decoding: %s\n", err.Error())
	}
	ok, err := raw.Verify([]byte(password))
	if err != nil {
		fmt.Printf("Error during verification: %s\n", err.Error())
	}
	if ok {
		return c, nil
	}
	return nil, errors.New("AuthApplication failed")
}

func AuthAdmin(email string, password string) (*model.Admin, error) {
	stmt, err := db.Prepare("SELECT admin.id, admin.email, admin.password FROM admin WHERE email = ?;")
	if err != nil {
		return nil, fmt.Errorf("AuthAdmin: can't db.Prepare() : %v", err)
	}
	a, err := scanAdmin(stmt.QueryRow(email))
	if err != nil {
		return nil, errors.New("AuthAdmin failed")
	}
	raw, err := argon2.Decode([]byte(a.Password))
	if err != nil {
		fmt.Printf("Error during decoding: %s\n", err.Error())
	}
	ok, err := raw.Verify([]byte(password))
	if err != nil {
		fmt.Printf("Error during verification: %s\n", err.Error())
	}
	if ok {
		return a, nil
	}
	return nil, errors.New("AuthAdmin failed")
}

func ValidPartner(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE partners SET valid=true WHERE id=?", args...); err != nil {
		return err
	} else if r != 1 {
		fmt.Println("Account does not exist or is already valid")
		return errors.New("Account does not exist or is already valid")
	}
	return nil
}

func ValidRider(args ...interface{}) error {
	if _, r, err := dbExec("UPDATE riders SET valid=true WHERE id=?", args...); err != nil {
		return err
	} else if r != 1 {
		fmt.Println("Account does not exist or is already valid")
		return errors.New("Account does not exist or is already valid")
	}
	return nil
}

func GetDeliveryAddresses(id string) ([]*model.Address, error) {
	var addresses []*model.Address
	if stmt, err := db.Prepare("SELECT DISTINCT address.street, address.zipcode, address.city, address.additionaladdress FROM address INNER JOIN clients ON clients.fk_address = address.id INNER JOIN purchases ON purchases.fk_client = clients.id  WHERE purchases.fk_delivery = ?;"); err != nil {
		return nil, fmt.Errorf("GetDeliveryAddresses: cant db.Prepare() : %v", err)
	} else if rows, err := stmt.Query(id); err != nil {
		return nil, err
	} else {
		defer rows.Close()
		for rows.Next() {
			if a, err := scanAddress(rows, "client"); err != nil {
				return nil, fmt.Errorf("GetDeliveryAddress: could not read row: %v", err)
			} else {
				addresses = append(addresses, a)
			}
		}
		if stmt, err := db.Prepare("SELECT DISTINCT address.street, address.zipcode, address.city, address.additionaladdress FROM address INNER JOIN partners ON partners.fk_address = address.id INNER JOIN purchases ON purchases.fk_partner = partners.id  WHERE purchases.fk_delivery = ?;"); err != nil {
			return nil, fmt.Errorf("GetDeliveryAddresses: cant db.Prepare() : %v", err)
		} else if rows, err := stmt.Query(id); err != nil {
			return nil, err
		} else {
			defer rows.Close()
			for rows.Next() {
				if a, err := scanAddress(rows, "partner"); err != nil {
					return nil, fmt.Errorf("GetDeliveryAddress: could not read row: %v", err)
				} else {
					addresses = append(addresses, a)
				}
			}
			return addresses, nil
		}
	}
}

func GetAllSpecifiedDeliveries(status string) ([]*model.Delivery, error) {
	if status == "all" {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries;")
	} else {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN statuses ON deliveries.fk_status = statuses.id WHERE statuses.description = ?;", status)
	}
}

func GetPurchaseDeliveries(status string, id string) ([]*model.Delivery, error) {
	if status == "all" {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE purchases.fk_delivery = ?;", id)
	} else {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN statuses ON deliveries.fk_status = statuses.id INNER JOIN purchases ON purchases.fk_delivery = deliveries.id WHERE purchases.fk_delivery = ? AND statuses.description = ?;", id, status)
	}
}

func GetClientDeliveries(status string, id string) ([]*model.Delivery, error) {
	if status == "all" {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN clients ON clients.id = purchases.fk_client WHERE clients.id = ?;", id)
	} else {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN clients ON clients.id = purchases.fk_client INNER JOIN statuses ON deliveries.fk_status = statuses.id WHERE clients.id = ? AND statuses.description = ?;", id, status)
	}
}

func GetPartnerDeliveries(status string, id string) ([]*model.Delivery, error) {
	if status == "all" {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN partners ON partners.id = purchases.fk_partner WHERE partners.id = ?;", id)
	} else {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN purchases ON purchases.fk_delivery = deliveries.id INNER JOIN partners ON partners.id = purchases.fk_partner INNER JOIN statuses ON deliveries.fk_status = statuses.id WHERE partners.id = ? AND statuses.description = ?;", id, status)
	}
}

func GetRiderDeliveries(status string, id string) ([]*model.Delivery, error) {
	if status == "all" {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries WHERE deliveries.fk_rider = ?;", id)
	} else {
		return GetDeliveries("SELECT DISTINCT deliveries.id, deliveries.fk_rider, deliveries.fk_status, deliveries.date FROM deliveries INNER JOIN statuses ON deliveries.fk_status = statuses.id WHERE deliveries.fk_rider = ? AND statuses.description = ?;", id, status)
	}
}

func GetAllSpecifiedPurchases(status string) ([]*model.Purchase, error) {
	if status == "all" {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id;")
	} else {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE statuses.description = ?;", status)
	}
}

func GetPartnerPurchases(status string, id string) ([]*model.Purchase, error) {
	if status == "all" {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE purchases.fk_partner = ?;", id)
	} else {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE purchases.fk_partner = ? AND statuses.description = ?;", id, status)
	}
}

func GetClientPurchases(status string, id string) ([]*model.Purchase, error) {
	if status == "all" {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, purchases.fk_status, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE purchases.fk_client = ?;", id)
	} else {
		return GetPurchases("SELECT DISTINCT purchases.id, purchases.fk_delivery, purchases.fk_client, purchases.fk_partner, statuses.id, purchases.date FROM purchases INNER JOIN statuses ON purchases.fk_status = statuses.id WHERE purchases.fk_client = ? AND statuses.description = ?;", id, status)
	}
}

func DeleteClient(args ...interface{}) error {
	if _, r, err := dbExec("DELETE clients, address FROM address INNER JOIN clients ON clients.fk_address = address.id WHERE clients.id = ?;", args...); err != nil {
		return err
	} else if r != 2 {
		return fmt.Errorf("DeleteClient: expected 2 row affected, got %d", r)
	}
	return nil
}

func DeleteReceiptByDelivery(args ...interface{}) error {
	if _, _, err := dbExec("DELETE FROM receipts WHERE fk_purchase = ?", args...); err != nil {
		return err
	}
	return nil
}

func DeletePartner(args ...interface{}) error {
	if _, r, err := dbExec("DELETE partners, address FROM address INNER JOIN partners ON partners.fk_address = address.id WHERE partners.id = ?;", args...); err != nil {
		return err
	} else if r != 2 {
		return fmt.Errorf("DeletePartner: expected 2 row affected, got %d", r)
	}
	return nil
}

func DeleteRider(args ...interface{}) error {
	if _, r, err := dbExec("DELETE FROM riders WHERE id = ?;", args...); err != nil {
		return err
	} else if r != 1 {
		return fmt.Errorf("DeleteRider: expected 1 row affected, got %d", r)
	}
	return nil
}
