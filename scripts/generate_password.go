package main

import (
	"fmt"
	"github.com/lhecker/argon2"
	"os"
)

func main() {
	if len(os.Args) >= 2 {
		cfg := argon2.DefaultConfig()
		raw, err := cfg.Hash([]byte(os.Args[1]), nil)
		if err != nil {
			fmt.Errorf("Cannot hash")
		} else {
			fmt.Println(os.Args[1])
			fmt.Println(string(raw.Encode()))
			fmt.Println("Remove terminal/shell history")
		}
	} else {
		fmt.Println("Give password as program argument")
	}
}
