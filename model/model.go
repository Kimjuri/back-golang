package model

import (
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type AuthPermissions struct {
	Role string
}

// Route represents an api route : its description, the http associated verb, a matching pattern and a associated function.
type Route struct {
	Name         string
	Method       string
	Pattern      string
	AuthRequired bool
	HandlerFunc  http.HandlerFunc
}

type User struct {
	Type string `json:"type"`
	ID   int    `json:"id"`
}

type Address struct {
	Type              string `json:"type"`
	Street            string `json:"street"`
	ZipCode           string `json:zipcode`
	City              string `json:"city"`
	AdditionalAddress string `json:"additionaladdress"`
}

// Partner represents a GroomShop partner store.
type Partner struct {
	Token             string `json:"token,omitempty"`
	ID                int    `json:"id,omitempty"`
	Name              string `json:"name,omitempty"`
	Street            string `json:"street,omitempty"`
	ZipCode           string `json:"zipcode,omitempty"`
	City              string `json:"city,omitempty"`
	AdditionalAddress string `json:"additionaladdress,omitempty"`
	Email             string `json:"email,omitempty"`
	PhoneNumber       string `json:"phonenumber,omitempty"`
	Password          string `json:"password,omitempty"`
	Logo              string `json:"logo,omitempty"`
	Valid             bool   `json:"valid"`
	Siret             string `json:"siret"`
}

// Client represents a customer of partner stores
type Client struct {
	Token             string `json:"token,omitempty"`
	ID                int    `json:"id"`
	FirstName         string `json:"firstname"`
	LastName          string `json:"lastname"`
	Street            string `json:"street"`
	ZipCode           string `json:"zipcode"`
	City              string `json:"city"`
	AdditionalAddress string `json:"additionaladdress"`
	Email             string `json:"email"`
	PhoneNumber       string `json:"phonenumber"`
	Password          string `json:"password,omitempty"`
	Deadline          string `json:"deadline"`
	Picture           string `json:"picture,omitempty"`
}

type Admin struct {
	Token    string `json:"token"`
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ForgotPassword struct {
	Email string `json:"email"`
}

type ForgotPasswordClaims struct {
	jwt.StandardClaims
	Email string `json:"email"`
}
type UpdatePassword struct {
	NewPassword string `json:"newpassword"`
}

// Account represents a special groomshop application account
type Account struct {
	Token         string `json:"token"`
	ID            int    `json:"id"`
	IDApplication int    `json:"idApplication"`
	Role          string `json:"role"`
	Password      string `json:"password"`
}

// Structure for user authentification
type Auth struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Structure for application authentication
type AuthApplication struct {
	IDApplication int    `json:"idApplication"`
	Password      string `json:"password"`
}

// JWT
type MyClaims struct {
	jwt.StandardClaims
	ID   int    `json:"id"`
	Role string `json:"role"`
}

type ClientClaims struct {
	jwt.StandardClaims
	FirstName         string `json:"firstname"`
	LastName          string `json:"lastname"`
	Street            string `json:"street"`
	ZipCode           string `json:"zipcode"`
	City              string `json:"city"`
	AdditionalAddress string `json:"additionaladdress"`
	Email             string `json:"email"`
	PhoneNumber       string `json:"phonenumber"`
	Password          string `json:"password"`
	Deadline          string `json:"deadline"`
}

type PartnerClaims struct {
	jwt.StandardClaims
	Name              string `json:"firstname"`
	Street            string `json:"street"`
	ZipCode           string `json:"zipcode"`
	City              string `json:"city"`
	AdditionalAddress string `json:"additionaladdress"`
	Email             string `json:"email"`
	PhoneNumber       string `json:"phonenumber"`
	Password          string `json:"password"`
	Siret			  string `json:"siret"`
}

type RiderClaims struct {
	jwt.StandardClaims
	FirstName   string `json:"firstname"`
	LastName    string `json:"lastname"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phonenumber"`
	Password    string `json:"password"`
	Iban        string `json:"iban"`
	Siret		string `json:"siret"`
}

type AuthContext struct {
	ID   int
	Role string
}

type ctxKey int

const (
	JwtKey ctxKey = iota
)

// The payload sent back on error
type Exception struct {
	Message string `json:"message"`
}

// Rider represents a bike deliverer
type Rider struct {
	Token       string  `json:"token,omitempty"`
	ID          int     `json:"id"`
	FirstName   string  `json:"firstname"`
	LastName    string  `json:"lastname"`
	Email       string  `json:"email"`
	PhoneNumber string  `json:"phonenumber"`
	Rating      int     `json:"rating"`
	Password    string  `json:"password,omitempty"`
	Iban        string  `json:"iban"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
	Free        bool    `json:"free"`
	Valid       bool    `json:"valid"`
	Siret       string  `json:"siret"`
}

type History struct {
	Date     string `json:"date"`
	Duration int    `json:"duration"`
	Amount   int    `json:"amount"`
}

type ClientStat struct {
	IDClient   int `json:"idClient"`
	NbDelivery int `json:"nbDelivery"`
}

type ClientDeliveryStat struct {
	Date  string `json:"date"`
	Store string `json:"store"`
}

// Delivery represents a delivery
type Delivery struct {
	ID          int    `json:"id"`
	IDPurchases []int  `json:"idPurchases"`
	IDRider     int    `json:"idRider"`
	Status      int    `json:"status"`
	Date        string `json:"date"`
}

// Represents a receipt in database
type Receipt struct {
	ID                 int    `json:"id"`
	Receipt            string `json:"riderReceipt"`
	ReceiptWatermarked string `json:"partnerReceipt"`
	IDDelivery         int    `json:"idDelivery"`
}

// Purchase represents the purchase of a client in a partner shop to be delivered
type Purchase struct {
	ID         int    `json:"id"`
	IDDelivery int    `json:"idDelivery"`
	IDClient   int    `json:"idClient"`
	IDPartner  int    `json:"idPartner"`
	Status     int64  `json:"status"`
	Date       string `json:"date"`
	Receipt    string `json:"receipt,omitempty"`
}

type Configuration struct {
	API_Address      string        `json:"api_address"`
	API_Port         int           `json:"api_port"`
	DB_Address       string        `json:"db_address"`
	DB_Port          int           `json:"db_port"`
	DB_Type          string        `json:"db_type"`
	DB_User          string        `json:"db_user"`
	DB_Pass          string        `json:"db_pass"`
	DB_Name          string        `json:"db_name"`
	JWT_Secret       string        `json:"jwt_secret"`
	JWT_Method       string        `json:"jwt_method"`
	JWT_Lifetime     time.Duration `json:"jwt_lifetime"`
	Version          string        `json:"version"`
	LastUpdate       string        `json:"last_update"`
	SMTP_host        string        `json:"smtp_host"`
	SMTP_port        string        `json:"smtp_port"`
	SMTP_user        string        `json:"smtp_user"`
	SMTP_password    string        `json:"smtp_password"`
	HOST             string        `json:"host"`
	EMAIL_validation bool          `json:"email_validation"`
	Images_PATH      string        `json:"images_path"`
	Images_URL       string        `json:"images_url"`
}

type Smtp struct {
	User     string
	Password string
	Host     string
	Port     string
}

type StatGraph struct {
	X string
	Y string
}
