FROM golang:1.10.0-alpine3.7 AS build
MAINTAINER Charles PAULET <charles.paulet@epitech.eu>

RUN apk add --no-cache git g++ make \
    && go get github.com/golang/dep/cmd/dep

COPY Gopkg.lock Gopkg.toml /go/src/back/

WORKDIR /go/src/back/

RUN dep ensure -vendor-only

COPY . /go/src/back/

RUN go build -ldflags "-X back/configuration.path=../configuration/confpre.json" -o /back

FROM alpine:3.7
COPY --from=build /back /back

RUN apk update
RUN apk add ca-certificates

# Local certificates still required ?
COPY --from=build /go/src/back/auth/server.rsa.crt /auth/server.crt
COPY --from=build /go/src/back/auth/server.rsa.key /auth/server.key
COPY --from=build /go/src/back/configuration/confpre.json /configuration/confpre.json
COPY --from=build /go/src/back/scripts/wait-for-predb.bash /start.sh
ENTRYPOINT ["/start.sh"]
