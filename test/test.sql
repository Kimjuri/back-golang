SOURCE ../init-db/00-structure.sql

USE groomshop;

INSERT INTO address(id, street, zipcode, city) VALUES (1, "16 rue d'Orléans", "44000", "Nantes");
INSERT INTO address VALUES (2, "9 rue Linné", "44100", "Nantes", "Appartement 36");
INSERT INTO address VALUES (3, "6 rue d'Alger", "44000", "Nantes", null);
INSERT INTO address VALUES (4, "17 Boulevard Vincent Gâche", "44200", "Nantes", "Appartement 76");
INSERT INTO address VALUES (5, "24 rue Dr Brindeau", "44000", "Nantes", null);
INSERT INTO address VALUES (6, "35 rue François Bruneau", "44000", "Nantes", null);
INSERT INTO address VALUES (7, "1 rue de Budapest", "44000", "Nantes", null);
INSERT INTO address VALUES (8, "6 rue de Feltre", "44000", "Nantes", null);
INSERT INTO address VALUES (9, "3 rue du Calvaire", "44000", "Nantes", null);
INSERT INTO address VALUES (10, "10 rue Boileau", "44000", "Nantes", null);
INSERT INTO address VALUES (11, "11 rue Paré", "44000", "Nantes", null);



INSERT INTO clients VALUES (1, "cfn1", "cln1", 1, "ce1@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);
INSERT INTO clients VALUES (2, "cfn2", "cln2", 2, "test_auth_client@client.fr", "0606060607", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);
INSERT INTO clients VALUES (3, "TestGetClient", "cln3", 3, "ce3@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);
INSERT INTO clients VALUES (4, "TestUpdateClient", "cln4", 4, "ce4@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);
INSERT INTO clients VALUES (5, "TestClientFetch", "cln5", 5, "ce5@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);
INSERT INTO clients VALUES (6, "TestAllPurchasesFetchAsAClient", "cln5", 6, "ce5@client.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I","04:13:54", null);

INSERT INTO riders VALUES (1, "rfn1", "rln1", "re1@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, true, true, '01234567890123');
INSERT INTO riders VALUES (2, "rfn1", "rln1", "test_auth_rider@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (3, "TestUpdateRider", "rln3", "re3@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (4, "TestAllPurchaseFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (5, "TestPlacedPurchaseFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (6, "TestPendingPurchaseFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (7, "TestResolvedPurchaseFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (8, "TestResolvedPurchasesForPartnerFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, true, '01234567890123');
INSERT INTO riders VALUES (9, "TestPendingPurchasesForClientFetch", "rln4", "re4@rider.fr", "0606060606", 0, "$argon2i$v=19$m=4096,t=3,p=1$3dEaoHLLKLFQG/7Sm+ygGA$jQRSQoSTyjfqnVbn7Ab+fex64NcBzkUiA4EX5xoWYLU", "FR7630006000011234567890189", 0, 0, false, false, '01234567890123');

INSERT INTO partners VALUES (1, "pn1", 7, "pe1@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/default.jpg", true, '01234567890123');
INSERT INTO partners VALUES (2, "pn1", 8, "test_auth_partner@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/default.jpg", true, '01234567890123');
INSERT INTO partners VALUES (3, "TestUpdatePartner", 9, "pe3@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/default.jpg", true, '01234567890123');
INSERT INTO partners VALUES (4, "pn1", 10, "test_auth_partner@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/default.jpg", true, '01234567890123');
INSERT INTO partners VALUES (5, "TestPurchaseCreation", 11, "pe5@partner.fr", "0606060606", "$argon2i$v=19$m=4096,t=3,p=1$9DZ0pT58oop03m6EkB473g$ZhPUuxGLlOfnOMPzYUI3jld2nEuMdoorzP0/9QHz2+w", "https://api.groomshop.fr/images/default.jpg", false, '01234567890123');

INSERT INTO statuses VALUES (1, "delivery_placed");
INSERT INTO statuses VALUES (2, "delivery_pending");
INSERT INTO statuses VALUES (3, "delivery_resolved");
INSERT INTO statuses VALUES (4, "purchase_placed");
INSERT INTO statuses VALUES (5, "purchase_pending");
INSERT INTO statuses VALUES (6, "purchase_resolved");

INSERT INTO purchases VALUES (1, 1, 1, 1, 4, "2019-09-21 16:20:47");
INSERT INTO purchases VALUES (2, 2, 1, 1, 5, "2019-10-21 12:20:47");
INSERT INTO purchases VALUES (3, 0, 1, 1, 6, "2019-01-10 13:20:47");
INSERT INTO purchases VALUES (4, 1, 1, 1, 4, "2019-09-21 17:20:47");
INSERT INTO purchases VALUES (5, 3, 1, 1, 4, "2019-09-22 15:20:47");

INSERT INTO accounts VALUES (1, 1, "application_role", "$argon2i$v=19$m=4096,t=3,p=1$7/+feUEmVPH+mhCfoWxz7Q$DNVhrFgKMSOlPM+aRMng4hPex9HM828PvxA79AKVK2I");

INSERT INTO deliveries VALUES (1, 1, 1, "2019-09-21 18:20:47");
INSERT INTO deliveries VALUES (2, 2, 2, "2019-10-21 17:15:47");
INSERT INTO deliveries VALUES (3, 1, 1, "2019-09-22 19:20:47");

INSERT INTO admin VALUES (1, "contact.groomshop@gmail.com", "$argon2i$v=19$m=4096,t=3,p=1$FtpbcataDFCTY44drrTjaQ$3dSRP53OkCuLPJg0GvwUAJZ3V00eSmEmkGg5pbQTljg");

INSERT INTO bills VALUES (1,1,1,1,5,"2019-09-21 18:45:15",650);
INSERT INTO bills VALUES (2,1,1,3,5,"2019-10-21 17:45:15",1020);
INSERT INTO bills VALUES (3,2,1,2,5,"2019-09-22 19:30:15",780);