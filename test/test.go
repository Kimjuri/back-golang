package test

import (
	"back/model"
	"fmt"
	"os"
	"os/exec"
	"testing"
)

func ReloadDB(t *testing.T) {
	pwd := "-p" + os.Getenv("DB_PWD")
	cmd := exec.Command("mysql", "-h", "db", "-u", "root", pwd, "-e", "source ../test/test.sql")
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(fmt.Sprint(err) + ": " + string(output))
		return
	} else {
		fmt.Println(string(output))
	}
}

func MyAssert(t *testing.T, test string, expect interface{}, got interface{}) {
	/* loose typing ? */
	if got != expect {
		t.Errorf("%s is incorrect. expect [%v] got [%v]", test, expect, got)
	}
}

/* Reject is !MyAssert */
func Reject(t *testing.T, test string, expect interface{}, got interface{}) {
	if got == expect {
		t.Errorf("%s is incorrect. wont [%v] got [%v]", test, expect, got)
	}
}

func CheckResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func ClientModel(t *testing.T, expect model.Client, got *model.Client) {
	MyAssert(t, "client id", expect.ID, got.ID)
	MyAssert(t, "client firstname", expect.FirstName, got.FirstName)
	MyAssert(t, "client lastname", expect.LastName, got.LastName)
	MyAssert(t, "client street", expect.Street, got.Street)
	MyAssert(t, "client zipcode", expect.ZipCode, got.ZipCode)
	MyAssert(t, "client city", expect.City, got.City)
	MyAssert(t, "client additionaladdress", expect.AdditionalAddress, got.AdditionalAddress)
	MyAssert(t, "client email", expect.Email, got.Email)
	MyAssert(t, "client phone number", expect.PhoneNumber, got.PhoneNumber)
	MyAssert(t, "client password", expect.Password, got.Password)
	MyAssert(t, "client picture", expect.Picture, got.Picture)
}

func RiderModel(t *testing.T, expect model.Rider, got *model.Rider) {
	MyAssert(t, "rider id", expect.ID, got.ID)
	MyAssert(t, "rider firstname", expect.FirstName, got.FirstName)
	MyAssert(t, "rider lastname", expect.LastName, got.LastName)
	MyAssert(t, "rider email", expect.Email, got.Email)
	MyAssert(t, "rider phone number", expect.PhoneNumber, got.PhoneNumber)
	MyAssert(t, "rider rating", expect.Rating, got.Rating)
	MyAssert(t, "rider password", expect.Password, got.Password)
}

func PartnerModel(t *testing.T, expect model.Partner, got *model.Partner) {
	MyAssert(t, "partner id", expect.ID, got.ID)
	MyAssert(t, "partner name", expect.Name, got.Name)
	MyAssert(t, "partner street", expect.Street, got.Street)
	MyAssert(t, "partner zipcode", expect.ZipCode, got.ZipCode)
	MyAssert(t, "partner city", expect.City, got.City)
	MyAssert(t, "partner additionaladdress", expect.AdditionalAddress, got.AdditionalAddress)
	MyAssert(t, "partner email", expect.Email, got.Email)
	MyAssert(t, "partner phone number", expect.PhoneNumber, got.PhoneNumber)
	MyAssert(t, "partner password", expect.Password, got.Password)
}

func DeliveryModel(t *testing.T, expect model.Delivery, got *model.Delivery) {
	MyAssert(t, "delivery id", expect.ID, got.ID)
	MyAssert(t, "delivery rider id", expect.IDRider, got.IDRider)
	MyAssert(t, "delivery status", expect.Status, got.Status)
}

func PurchaseModel(t *testing.T, expect model.Purchase, got *model.Purchase) {
	MyAssert(t, "purchase id", expect.ID, got.ID)
	MyAssert(t, "purchase delivery id", expect.IDDelivery, got.IDDelivery)
	MyAssert(t, "purchase client id", expect.IDClient, got.IDClient)
	MyAssert(t, "purchase partner id", expect.IDPartner, got.IDPartner)
	MyAssert(t, "purchase status", expect.Status, got.Status)
}

func AdminModel(t *testing.T, expect model.Admin, got *model.Admin) {
	MyAssert(t, "rider id", expect.ID, got.ID)
	MyAssert(t, "rider email", expect.Email, got.Email)
	MyAssert(t, "rider password", expect.Password, got.Password)
}
