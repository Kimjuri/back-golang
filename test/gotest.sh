#!/bin/sh
ROOT=$(git rev-parse --show-toplevel)
#cat $ROOT/test/test.sql | mysql -u root -p${DB_PWD}
cd "$ROOT"

GOCACHE=off go test ./api/... -v
GOCACHE=off go test ./db/... -v
