#!/bin/bash

SCHEME="https://"
HOST="localhost"
#HOST="api.groomshop.fr"
PORT="8080"
ROOT=$(git rev-parse --show-toplevel)
CERTIF="server.crt"
#CERTIF="cacert.pem"

hr()
{
  echo -e "\n"
  head -c 42 /dev/zero | tr '\0' '#'
  echo -e "\n$1"
  head -c 42 /dev/zero | tr '\0' '#'
  echo -e "\n"
}

scan()
{
  RET=$1
  OUT=$2
  hr "$3"
  HTTP_CODE=$(echo "$OUT" | tail -n1)
  echo "HTTP status: $HTTP_CODE"
  if [[ $HTTP_CODE -ne $4 ]];then
    echo "Expected $4"
    exit 1
  else
    REP=$(echo "$OUT" | head -n-1)
    echo -e "Response :\n$REP"
  fi
}

hr "Init database"
cat $ROOT/init-db/init.sql | mysql -u root -p${DB_PWD}

getToken()
{
  curl -H "Content-Type: application/json" -d '
  {
    "email": "'$2'",
    "password": "'$3'"
  }
  ' "$SCHEME$HOST:$PORT/auth/$1" -sk | grep  -Po '"token":.*?[^\\]",' | perl -pe 's/"token"://; s/^"//; s/",$//'
}

CT=$(getToken "client" "ce1@client.fr" "cp1")
PT=$(getToken "partner" "pe1@partner.fr" "pp1")
RT=$(getToken "rider" "re1@rider.fr" "rp1")
RT2=$(getToken "rider" "re2@rider.fr" "rp2")

#
# TLS
#

# TEST 1
#T=$(curl -fsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/riders/2" --ssl-reqd --tlsv1.2 --cacert "$CERTIF")
#scan $? "$T" "Get deliveries for rider 2 with certif (--ssl-reqd --tlsv1.2 --cacert $CERTIF)" 200

#
# MISC
#

# TEST 0
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: bad/one" -X POST -d '
{
  "firstname": "jean",
  "lastname": "dupont",
  "address": "24 rue de la brebie",
  "email": "jean@dupont.com",
  "phonenumber": "0606060606",
  "password": "p455w0rD"
}
' "$SCHEME$HOST:$PORT/clients" -H "Authorization: Bearer $CT")
scan $? "$T" "content type" 415

#
# DELIVERIES
#

# TEST 0
T=$(curl -kfsw '\n%{http_code}' -H "Content-type: application/json" -d '
{
  "idClient": 1,
  "idPartner": 2,
  "address": "NantesSansRider",
  "deadline": "04:13:54"
}
' "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $PT")
scan $? "$T" "Create delivery without rider" 201
# TEST 1
T=$(curl -kfsw '\n%{http_code}' -H "Content-type: application/json" -d '
{
  "idClient": 1,
  "idRider": 2,
  "idPartner": 2,
  "address": "Nantes",
  "deadline": "04:13:54"
}
' "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $PT")
scan $? "$T" "Create delivery" 201
# TEST 2
T=$(curl -kfsw '\n%{http_code}' -H "Content-type: application/json" -d '
{
  "idClient": 1,
  "idRider": 2,
  "addunknow": "Nantes",
  "deadline": "2015-07-04 04:13:54"
}
' "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $PT")
scan $? "$T" "Create invalid delivery" 400
# TEST 3
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/placed" -H "Authorization: Bearer $PT")
scan $? "$T" "Get placed deliveries" 200
# TEST 4
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/pending" -H "Authorization: Bearer $PT")
scan $? "$T" "Get pending deliveries" 200
# TEST 5
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/resolved" -H "Authorization: Bearer $PT")
scan $? "$T" "Get resolved deliveries" 200
# TEST 6
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/placed?p=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get placed deliveries for partner 1" 200
# TEST 7
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/placed?c=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get placed deliveries for client 1" 200
# TEST 8
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/placed?r=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get placed deliveries for rider 1" 200
# TEST 9
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/pending?p=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get pending deliveries for partner 1" 200
# TEST 10
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/pending?c=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get pending deliveries for client 1" 200
# TEST 11
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/pending?r=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get pending deliveries for rider 1" 200
# TEST 12
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/resolved?p=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get resolved deliveries for partner 1" 200
# TEST 13
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/resolved?c=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get resolved deliveries for client 1" 200
# TEST 14
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries/resolved?r=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get resolved deliveries for rider 1" 200
# TEST 15
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $PT")
scan $? "$T" "Get all deliveries" 200
# TEST 16
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries?r=2" -H "Authorization: Bearer $PT")
scan $? "$T" "Get all deliveries for rider 2" 200
# TEST 17
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries?p=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get all deliveries for partner 1" 200
# TEST 18
T=$(curl -kfsw '\n%{http_code}' -X GET -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/deliveries?c=1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get all deliveries for client 1" 200
## TEST 19
#T=$(curl -kfsw '\n%{http_code}' -H "Content-type: application/json" -d '
#{
#  "idClient": 1,
#  "idRider": 2,
#  "idPartner": 2,
#  "address": "Nantes",
#  "deadline": "04:13:54"
#}
#' "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $RT")
#scan $? "$T" "Create delivery as rider 1" 401
## TEST 20
#T=$(curl -kfsw '\n%{http_code}' -H "Content-type: application/json" -d '
#{
#  "idClient": 1,
#  "idRider": 2,
#  "idPartner": 2,
#  "address": "Nantes",
#  "deadline": "04:13:54"
#}
#' "$SCHEME$HOST:$PORT/deliveries" -H "Authorization: Bearer $CT")
#scan $? "$T" "Create as client 1" 401

#
# AUTH
#

# TEST 1
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "ce1@client.fr",
  "password": "cp1"
}
' "$SCHEME$HOST:$PORT/auth/client")
scan $? "$T" "valid client auth" 200
# TEST 2
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "ce1@client.fr",
  "pass": "cp1"
}
' "$SCHEME$HOST:$PORT/auth/client")
scan $? "$T" "invalid client auth (wrong key)" 400
# TEST 3
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "ce1@client.fr",
  "pass": "cp1"
}
' "$SCHEME$HOST:$PORT/auth/client")
scan $? "$T" "invalid client auth (wrong creds)" 400
# TEST 4
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "pe1@partner.fr",
  "password": "pp1"
}
' "$SCHEME$HOST:$PORT/auth/partner")
scan $? "$T" "valid partner auth" 200
# TEST 5
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "pe1@partner.fr",
  "pass": "pp1"
}
' "$SCHEME$HOST:$PORT/auth/partner")
scan $? "$T" "invalid partner auth (wrong key)" 400
# TEST 6
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "pe1@partner.fr",
  "pass": "pp1"
}
' "$SCHEME$HOST:$PORT/auth/partner")
scan $? "$T" "invalid partner auth (wrong creds)" 400
# TEST 7
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "re1@rider.fr",
  "password": "rp1"
}
' "$SCHEME$HOST:$PORT/auth/rider")
scan $? "$T" "valid rider auth" 200
# TEST 5
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "re1@rider.fr",
  "pass": "rp1"
}
' "$SCHEME$HOST:$PORT/auth/rider")
scan $? "$T" "invalid rider auth (wrong key)" 400
# TEST 6
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "email": "re1@rider.fr",
  "pass": "rp1"
}
' "$SCHEME$HOST:$PORT/auth/rider")
scan $? "$T" "invalid rider auth (wrong creds)" 400

#
# CLIENTS
#

# TEST 1
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "firstname": "jeeeeeeeeeeeeeeeee",
  "lastname": "dupont",
  "address": "24 rue de la brebie",
  "email": "jean@dupont.com",
  "phonenumber": "0606060606",
  "password": "p455w0rD",
}
' "$SCHEME$HOST:$PORT/clients")
scan $? "$T" "create valid client" 201

# TEST 2
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "firstname": "jean",
  "lastname": "dupont",
  "address": "24 rue de la brebie",
  "mail": "jean@dupont.com",
  "phonenumber": "0606060606",
  "password": "p455w0rD"
}
' "$SCHEME$HOST:$PORT/clients" -H "Authorization: Bearer $CT")
scan $? "$T" "create invalid client (unknow key)" 400
# TEST 3
CT=$(getToken "client" "ce2@client.fr" "cp2")
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/2" -H "Authorization: Bearer $CT")
scan $? "$T" "Get client informations" 200
CT=$(getToken "client" "ce1@client.fr" "cp1")
 TEST 4
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/21345678963422345678" -H "Authorization: Bearer $CT")
scan $? "$T" "Get unknow client informations" 404
# TEST 5
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "firstname": "jean",
  "lastname": "dupont",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com",
  "password": "lolilol"
}
' "$SCHEME$HOST:$PORT/clients/1" -H "Authorization: Bearer $CT")
scan $? "$T" "Update client informations" 202
# TEST 6
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "firstname": "jean",
  "lastname": "dupont",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com",
  "password": "lolilol"
}
' "$SCHEME$HOST:$PORT/clients/1" -H "Authorization: Bearer $CT")
scan $? "$T" "Re-Update client informations" 202
# TEST 7
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients" -H "Authorization: Bearer $CT")
scan $? "$T" "Undefined method on route" 405
# TEST 8
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/2" -H "Authorization: Bearer $CT")
scan $? "$T" "Delete client 2 with the creds of 1" 401
# TEST 9
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/2" -H "Authorization: Bearer $PT")
scan $? "$T" "Delete client as a partner" 401
# TEST 10
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/2" -H "Authorization: Bearer $RT")
scan $? "$T" "Delete client as a rider" 401
# TEST 11
#T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/clients/1" -H "Authorization: Bearer $CT")
#scan $? "$T" "Delete client 1" 200

#
# PARTNERS
#

# TEST 1
PT2=$(getToken "partner" "pe2@partner.fr" "pp2")
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $PT2")
scan $? "$T" "Get partner informations" 200
# TEST 2
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" --ssl-reqd --tlsv1.2 --cacert server.crt -H "Authorization: Bearer $PT2")
scan $? "$T" "Get partner informations with certif (--ssl-reqd --tlsv1.2 --cacert server.crt)" 200
# TEST 3
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/21345678963422345678" -H "Authorization: Bearer $PT")
scan $? "$T" "Get unknow partner informations" 404
# TEST 4
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "name": "hetm",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com"
}
' "$SCHEME$HOST:$PORT/partners/1" -H "Authorization: Bearer $PT")
scan $? "$T" "Update partner informations" 202
# TEST 5
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "name": "hetm",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com"
}
' "$SCHEME$HOST:$PORT/partners/1" -H "Authorization: Bearer $PT")
scan $? "$T" "Re-Update partner informations" 202
# TEST 6

T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "name": "hetm",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com"
}
' "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $PT")
scan $? "$T" "Update partner 2 with token of partner 1" 401
# TEST 7
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "name": "hetm",
  "address": "nouvelleaddresse",
  "email": "jean@dupont.com"
}
' "$SCHEME$HOST:$PORT/partners/1" -H "Authorization: Bearer $RT")
scan $? "$T" "Update partner 1 with token of rider 1" 403
# TEST 8
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/1" -H "Authorization: Bearer $CT")
scan $? "$T" "Get partner 1 infos with client 1 token" 401
# TEST 9
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $PT")
scan $? "$T" "Get partner 3 infos with partner 1 token" 401
# TEST 9
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $PT")
scan $? "$T" "Delete partner 2 with the creds of 1" 401
# TEST 10
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $PT")
scan $? "$T" "Delete partner 2 as partner 1" 401
# TEST 11
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/2" -H "Authorization: Bearer $RT")
scan $? "$T" "Delete partner 2 as a rider" 401
# TEST 12
T=$(curl -kfsw '\n%{http_code}' -X DELETE -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/partners/1" -H "Authorization: Bearer $PT")
scan $? "$T" "Delete partner 1" 200
#TEST 13
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '  
{                                                                                 
  "name": "bonobo",                                                           
  "address": "11 rue du chameau",                                               
  "email": "contact@bonobo.com",                                      
  "phonenumber": "0606060606",                                                  
  "password": "p455w0rD"                                                    
}                                                                                 
' "$SCHEME$HOST:$PORT/partners")
scan $? "$T" "create valid partner" 201

# TEST 14
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '  {                                                                                 
  "firstname": "bonobo",                                           
  "name": "plop"                                                    
  "address": " 11 rue du chameaue",                                               
  "mail": "contact@bonobo.com",                                                
  "phonenumber": "0606060606",                                                    
  "password": "p455w0rD"                                                          
}                                                                                 
' "$SCHEME$HOST:$PORT/partners")
scan $? "$T" "create invalid partner (unknow key)" 400


#
# RIDERS
#

# TEST 1
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/1" -H "Authorization: Bearer $RT")
scan $? "$T" "Get rider informations" 200
# TEST 2
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/1" --ssl-reqd --tlsv1.2 --cacert server.crt -H "Authorization: Bearer $RT")
scan $? "$T" "Get rider informations with certif (--ssl-reqd --tlsv1.2 --cacert server.crt)" 200
# TEST 3
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/21345678963422345678" -H "Authorization: Bearer $RT")
scan $? "$T" "Get unknown rider's informations" 404
# TEST 4
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X PATCH -d '
{
  "firstname": "rider1",
  "email": "nouveaumail",
  "phonenumber": "0123456779"
}
' "$SCHEME$HOST:$PORT/riders/1" -H "Authorization: Bearer $RT")
scan $? "$T" "Update rider informations" 202
# TEST 5

T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/1" -H "Authorization: Bearer $CT")
scan $? "$T" "Get rider 1 infos as a client" 401
# TEST 6
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/1" -H "Authorization: Bearer $PT")
scan $? "$T" "Get rider 1 infos as a partner" 401
# TEST 7
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" "$SCHEME$HOST:$PORT/riders/1" -H "Authorization: Bearer $RT2")
scan $? "$T" "Get rider 1 infos as rider 2" 401

#TEST 8
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "firstname": "rider",
  "lastname": "7",
  "email": "r7@grommshop.com",
  "phonenumber": "0606060606",
  "password": "p455w0rD"
}
' "$SCHEME$HOST:$PORT/riders")
scan $? "$T" "create valid rider" 201

# TEST 9
T=$(curl -kfsw '\n%{http_code}' -H "Content-Type: application/json" -X POST -d '
{
  "firstname": "rider",
  "lastname": "6",
  "address": "24 rue de la brebie",
  "mail": "r6@groomshop.fr",
  "phonenumber": "0606060606",
  "rating": 0,
  "password": "p455w0rD"
}
' "$SCHEME$HOST:$PORT/riders")
scan $? "$T" "create invalid rider (unknow key)" 400

