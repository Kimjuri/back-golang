#!/bin/sh

# Get the cert that the server is using
HOST="api.groomshop.fr"
openssl s_client -showcerts -servername server -connect "$HOST" > cacert.pem
